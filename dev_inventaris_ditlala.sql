-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 24, 2021 at 10:21 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_inventaris_ditlala`
--

-- --------------------------------------------------------

--
-- Table structure for table `cluster`
--

CREATE TABLE `cluster` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cluster_system_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cluster`
--

INSERT INTO `cluster` (`id`, `cluster_system_id`, `name`, `description`, `meta`, `created_at`, `updated_at`) VALUES
(3, 2, 'Power VM DWH', NULL, NULL, '2021-12-23 04:22:42', '2021-12-23 04:22:42'),
(4, 4, 'Power VM SIMLALA', NULL, NULL, '2021-12-23 04:23:04', '2021-12-23 04:23:04');

-- --------------------------------------------------------

--
-- Table structure for table `cluster_systems`
--

CREATE TABLE `cluster_systems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cluster_systems`
--

INSERT INTO `cluster_systems` (`id`, `name`, `description`, `meta`, `created_at`, `updated_at`) VALUES
(2, 'Nutanix', NULL, NULL, '2021-12-23 04:20:25', '2021-12-23 04:20:25'),
(3, 'Power VM', NULL, NULL, '2021-12-23 04:20:32', '2021-12-23 04:20:32'),
(4, 'VM Ware', NULL, NULL, '2021-12-23 04:20:59', '2021-12-23 04:20:59');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ip_addresses`
--

CREATE TABLE `ip_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `t_barangs_id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isMgmt` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ip_addresses`
--

INSERT INTO `ip_addresses` (`id`, `t_barangs_id`, `ip_address`, `isMgmt`, `created_at`, `updated_at`) VALUES
(10, 3, '10.252.242.161', 1, NULL, NULL),
(11, 3, '192.168.4.100', 0, NULL, NULL),
(12, 4, '10.252.242.162', 1, NULL, NULL),
(13, 4, '192.168.4.102', 0, NULL, NULL),
(14, 5, '192.168.50.192', 0, NULL, NULL),
(15, 6, '192.168.50.193', 0, NULL, NULL),
(16, 6, '10.252.242.150', 0, NULL, NULL),
(17, 7, '10.252.242.151', 0, NULL, NULL),
(18, 8, '10.252.242.152\r\n', 0, NULL, NULL),
(19, 9, '10.252.242.154', 0, NULL, NULL),
(20, 10, '10.252.242.155', 0, NULL, NULL),
(21, 9, '10.252.242.153', 0, NULL, NULL),
(22, 19, '10.252.242.177\r\n', 0, NULL, NULL),
(23, 19, '192.168.58.10', 0, NULL, NULL),
(24, 19, '192.168.58.14', 0, NULL, NULL),
(25, 20, '10.252.242.178', 0, NULL, NULL),
(26, 20, '192.168.58.11', 0, NULL, NULL),
(27, 20, '192.168.58.15', 0, NULL, NULL),
(28, 21, '10.252.242.179', 0, NULL, NULL),
(29, 21, '192.168.58.12', 0, NULL, NULL),
(30, 21, '192.168.58.16', 0, NULL, NULL),
(31, 17, '10.252.242.180', 0, NULL, NULL),
(32, 67, '10.252.242.190 / 24', 0, '2021-12-20 03:21:55', '2021-12-20 03:21:55'),
(33, 67, '10.252.242.191 / 24', 0, '2021-12-20 03:21:55', '2021-12-20 03:21:55'),
(34, 67, '10.252.242.192/ 24', 0, '2021-12-20 03:21:55', '2021-12-20 03:21:55'),
(35, 67, '10.252.242.193 / 24', 0, '2021-12-20 03:21:55', '2021-12-20 03:21:55'),
(36, 68, '192.168.50.196', 0, '2021-12-20 03:23:36', '2021-12-20 03:23:36'),
(37, 68, '192.168.50.197', 0, '2021-12-20 03:23:36', '2021-12-20 03:23:36'),
(38, 69, '192.168.58.122', 0, '2021-12-20 03:24:13', '2021-12-20 03:24:13'),
(39, 23, '192.168.72.9', 0, '2021-12-20 03:25:49', '2021-12-20 03:25:49'),
(40, 24, '192.168.72.8', 0, '2021-12-20 03:26:10', '2021-12-20 03:26:10'),
(41, 25, '192.168.72.7', 0, '2021-12-20 03:26:27', '2021-12-20 03:26:27'),
(42, 26, '192.168.72.6', 0, '2021-12-20 03:26:53', '2021-12-20 03:26:53'),
(43, 29, '192.168.72.4', 0, '2021-12-20 03:27:10', '2021-12-20 03:27:10'),
(44, 34, '192.168.72.71', 0, '2021-12-20 03:27:21', '2021-12-20 03:27:21'),
(45, 32, '192.168.72.5', 0, '2021-12-20 03:27:40', '2021-12-20 03:27:40'),
(46, 33, '192.168.72.3', 0, '2021-12-20 03:28:07', '2021-12-20 03:28:07'),
(47, 37, '192.168.72.11', 0, '2021-12-20 03:28:44', '2021-12-20 03:28:44'),
(48, 42, '192.168.72.48', 0, '2021-12-20 03:29:00', '2021-12-20 03:29:00'),
(49, 44, '192.168.72.47', 0, '2021-12-20 03:29:16', '2021-12-20 03:29:16'),
(50, 45, '192.168.72.46', 0, '2021-12-20 03:29:37', '2021-12-20 03:29:37'),
(51, 46, '192.168.72.82', 0, '2021-12-20 03:29:50', '2021-12-20 03:29:50'),
(52, 52, '192.168.72.49', 0, '2021-12-20 03:30:01', '2021-12-20 03:30:01'),
(53, 58, '192.168.72.219', 0, '2021-12-20 03:30:19', '2021-12-20 03:30:19'),
(54, 60, '192.168.72.218', 0, '2021-12-20 03:30:32', '2021-12-20 03:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_jenis_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_meta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`id`, `nama_jenis_barang`, `created_at`, `updated_at`, `_meta`) VALUES
(1, 'Server', '2021-12-13 19:36:06', '2021-12-13 19:36:06', 'text-danger'),
(2, 'Power System', '2021-12-13 19:36:10', '2021-12-13 19:36:10', 'text-success'),
(3, 'Network Switch', '2021-12-13 19:36:19', '2021-12-13 19:36:19', 'text-primary'),
(4, 'Storage', '2021-12-13 19:36:24', '2021-12-13 19:36:24', 'text-warning'),
(5, 'Load Balancer', '2021-12-13 19:36:37', '2021-12-13 19:36:37', 'text-info'),
(6, 'Tape Backup', '2021-12-13 19:36:37', '2021-12-13 19:36:37', 'text-secondary'),
(7, 'Software', NULL, NULL, 'dark');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_virtual_machines`
--

CREATE TABLE `jenis_virtual_machines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `_meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_virtual_machines`
--

INSERT INTO `jenis_virtual_machines` (`id`, `nama`, `_meta`, `created_at`, `updated_at`) VALUES
(1, 'Power VM', 'text-danger', NULL, NULL),
(2, 'VM Ware', 'text-success', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Hardware', NULL, NULL),
(8, 'Software', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lokasis`
--

CREATE TABLE `lokasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasis`
--

INSERT INTO `lokasis` (`id`, `name`, `created_at`, `updated_at`, `deskripsi`) VALUES
(1, 'DCT', '2021-12-12 23:23:59', '2021-12-12 23:23:59', 'Data Center'),
(2, 'JTN', '2021-12-12 23:24:07', '2021-12-12 23:24:07', 'Jatinegara');

-- --------------------------------------------------------

--
-- Table structure for table `merks`
--

CREATE TABLE `merks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_merk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_11_11_034937_create_jenis_barangs_table', 1),
(6, '2021_11_11_070112_create_kategori_barangs_table', 1),
(7, '2021_11_15_061620_create_lokasis_table', 1),
(8, '2021_12_09_041613_create_pengadaans_table', 1),
(9, '2021_12_09_042043_create_merks_table', 1),
(10, '2021_12_09_042103_create_tipes_table', 1),
(11, '2021_12_09_044806_create_sessions_table', 1),
(12, '2021_12_09_070643_add_deskripsi_lokasi', 1),
(13, '2021_12_09_082039_create_barangs_table', 1),
(14, '2021_12_09_100731_add_some_field_to_t_barangs', 1),
(15, '2021_12_09_105610_create_i_p_addresses_table', 1),
(16, '2021_12_11_082233_create_v_mware_table', 1),
(17, '2021_12_11_094629_create_nutanixes_table', 1),
(18, '2021_12_11_102737_create_power_v_m_s_table', 1),
(19, '2021_12_11_122248_create_power_v_m_lists_table', 1),
(20, '2021_12_13_091124_add_relation_kategori_id_to_barang', 2),
(21, '2021_12_16_093503_create_jenis_virtual_machines_table', 3),
(25, '2021_12_16_100507_create_virtual_machines_table', 4),
(26, '2021_12_16_124212_add_field_meta_to_jenis_barang', 5),
(27, '2021_12_17_105638_add_meta_field_virtualmachines', 6),
(28, '2021_12_20_063637_add_field_mac_address_field', 7),
(29, '2021_12_20_071413_add_is_hypervisor_field', 8),
(30, '2021_12_20_100224_add_barang_id_virtual_machine', 9),
(31, '2021_12_20_105048_add_some_field_virtual_mechines', 10),
(32, '2021_12_20_110341_add_some_field_t_barangs', 11),
(35, '2021_12_21_053101_create_clusters_table', 12),
(44, '2021_12_23_042654_create_cluster_systems_table', 13),
(45, '2021_12_23_043028_create_cluster_table', 13),
(46, '2021_12_23_053034_create_nodes_table', 13),
(47, '2021_12_23_053856_create_virtual_machines_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE `nodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cluster_id` bigint(20) UNSIGNED NOT NULL,
  `node_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `machine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `ip_address` text COLLATE utf8mb4_unicode_ci,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `cluster_id`, `node_name`, `machine_name`, `ip_address`, `meta`, `created_at`, `updated_at`) VALUES
(6, 4, 'DITLALA ESXI Host #1', 'dllhost01.dephub.go.id', '192.168.50.70', NULL, '2021-12-23 04:25:32', '2021-12-23 04:25:32'),
(7, 4, 'DITLALA ESXI Host #2', 'dllhost02.dephub.go.id', '192.168.50.71', NULL, '2021-12-23 04:25:32', '2021-12-23 04:25:32'),
(8, 4, 'DITLALA ESXI Host #3', 'dllhost03.dephub.go.id', '192.168.50.72', NULL, '2021-12-23 04:25:32', '2021-12-23 04:25:32'),
(9, 3, 'HOST #1', '123', '123', NULL, '2021-12-23 20:12:40', '2021-12-23 20:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `nutanixes`
--

CREATE TABLE `nutanixes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `host` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memory_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provisioned_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nutanixes`
--

INSERT INTO `nutanixes` (`id`, `host`, `name`, `ip_address`, `core`, `memory_capacity`, `provisioned_space`, `used_space`, `created_at`, `updated_at`) VALUES
(1, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'app-02', 'NULL', '4', '8 GiB', '100 GiB', '8.34 GiB', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(2, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'app-02', 'NULL', '4', '8 GiB', ' 100 GiB', '8.34 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(3, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'App-1', 'NULL', '2', '8 GiB', ' 100 GiB', '8.13 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(4, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'DB PPKA-Hibah-Simlala', '192.168.58.30 ', '8', '16 GiB', ' 500 GiB', '41.01 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(5, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Formgen PPKA-SIJUKA', '192.168.58.19 ', '8', '16 GiB', ' 500 GiB', '46.78 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(6, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP01', '192.168.58.20', '8', '16 GiB', ' 60 GiB', '29.87 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(7, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP02', '192.168.58.21 ', '8', '16 GiB', ' 60 GiB', '24.55 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(8, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP03', '192.168.58.22', '8', '16 GiB', '60 GiB', '24.3 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(9, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP04', '192.168.58.23', '8', '16 GiB', ' 60 GiB', '24.99 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(10, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP05', '192.168.58.24', '8', '16 GiB', ' 60 GiB', '25.31 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(11, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP06', '192.168.58.25', '8', '16 GiB', '60 GiB', '24.9 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(12, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP07', '192.168.58.26', '8', '16 GiB', ' 60 GiB', '25.07 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(13, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP08', '192.168.58.27', '8', '16 GiB', ' 60 GiB', '24.49 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(14, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP09', '192.168.58.28', '8', '16 GiB', ' 60 GiB', '24.14 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(15, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP10', '192.168.58.29', '8', '16 GiB', ' 60 GiB', '24.53 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(16, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP11', '192.168.58.70', '8', '16 GiB', ' 60 GiB', '24.19 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(17, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP12', '192.168.58.71', '8', '16 GiB', ' 60 GiB', '25.02 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(18, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP13', '192.168.58.72', '8', '16 GiB', ' 60 GiB', '25.63 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(19, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP14', '192.168.58.73', '8', '16 GiB', ' 60 GiB', '24.19 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(20, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP15', '192.168.58.74', '8', '16 GiB', ' 60 GiB', '24.44 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(21, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP16', '192.168.58.75', '8', '16 GiB', ' 60 GiB', '24.14 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(22, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP17', '192.168.58.76', '8', '16 GiB', ' 60 GiB', '24.01 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(23, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP18', '192.168.58.77', '8', '16 GiB', ' 60 GiB', '24.56 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(24, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAUTOMATION', '192.168.58.53', '2', '8 GiB', ' 60 GiB', '25.45 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(25, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB01', '192.168.58.40 ', '4', '16 GiB', ' 260 GiB', '174.1 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(26, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTDB02', '192.168.58.41 ', '4', '16 GiB', ' 260 GiB', '171.56 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(27, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB03', '192.168.58.42 ', '4', '16 GiB', ' 260 GiB', '172.17 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(28, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB04', '192.168.58.43 ', '4', '16 GiB', ' 260 GiB', '173.54 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(29, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTDB05', '192.168.58.44 ', '4', '16 GiB', ' 260 GiB', '173.44 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(30, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTDB06', '192.168.58.45 ', '4', '16 GiB', ' 260 GiB', '177.77 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(31, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTLBAPP', '192.168.58.58 ', '2', '8 GiB', ' 260 GiB', '48.61 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(32, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTLBDB', '192.168.58.57 ', '2', '8 GiB', ' 260 GiB', '137.02 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(33, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTMONITORING', 'NULL', '2', '8 GiB', '60 GiB', '4.91 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(34, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTNFS01', '192.168.58.50 ', '4', '16 GiB', '7.06 TiB', '4.82 TiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(35, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Nutanix VCenter', '192.168.58.17', '4', '16 GiB', ' 477 GiB', '118.67 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(36, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'PPKA-SIJUKA', '192.168.58.18 ', '8', '16 GiB', ' 1 TiB', '44.49 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(37, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Scheduler Server-02_replica', '192.168.50.179 ', '8', '16 GiB', ' 300 GiB', '10.87 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(38, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Simlala WebApps-01_replica', 'NULL', '4', '16 GiB', ' 300 GiB', '69.58 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(39, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Simlala WebApps-04_replica', 'NULL', '4', '16 GiB', ' 300 GiB', '86.63 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(40, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'SIMLALA-NFS-BACKUP', '192.168.58.103 ', '4', '8 GiB', '2.45 TiB', '1.54 TiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(41, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'SuperSet', '192.168.58.120', '8', '16 GiB', ' 60 GiB', '30.32 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34'),
(42, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Veeam', '192.168.58.100', '2', '16 GiB', '100 GiB', '41.42 GiB ', '2021-12-14 23:35:34', '2021-12-14 23:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengadaans`
--

CREATE TABLE `pengadaans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pengadaan` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengadaans`
--

INSERT INTO `pengadaans` (`id`, `nama_pengadaan`, `tgl_pengadaan`, `created_at`, `updated_at`) VALUES
(1, 'Pengadaan A', '2021-01-01', '2021-12-12 23:24:18', '2021-12-12 23:24:18'),
(2, 'Pengadaan B', '2021-01-01', '2021-12-12 23:24:26', '2021-12-12 23:24:26'),
(3, 'Pengadaan C', '2021-01-01', '2021-12-12 23:24:18', '2021-12-12 23:24:18'),
(4, 'Pengadaan D', '2021-01-01', '2021-12-12 23:24:26', '2021-12-12 23:24:26');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `power_vm`
--

CREATE TABLE `power_vm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kd_power_vm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vm_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `power_vm`
--

INSERT INTO `power_vm` (`id`, `kd_power_vm`, `vm_name`, `hostname`, `vm_ip`, `created_at`, `updated_at`) VALUES
(1, 'HOST #1', 'DITLALA PVM Host', 'dllpv01.dephub.go.id', '192.168.50.190', NULL, NULL),
(2, 'HOST #2', 'DITLALA PVM', 'dllpv02.dephub.go.id', '192.168.50.191', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `power_vm_list`
--

CREATE TABLE `power_vm_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `power_vm_id` bigint(20) UNSIGNED NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCpu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDisk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copmonents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `power_vm_list`
--

INSERT INTO `power_vm_list` (`id`, `power_vm_id`, `deskripsi`, `hostname`, `ip_address`, `vCpu`, `vRam`, `vDisk`, `OS`, `copmonents`, `service`, `created_at`, `updated_at`) VALUES
(5, 1, 'Deployment Manager', 'dllmgrbpm.dephub.go.id', '192.168.50.17', '4', '8GB', '100GB', 'SUSE 1', 'IBM WAS ND - Dmgr', 'Deployment Manager', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(6, 1, 'BPM Process Center', 'sllbpmpc.dephub.go.id', '192.168.50.186', '2', '8GB', '100 GB', 'SUSE 12', 'IBM BPM Process Center', 'SIMLALA BPM Process Center', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(7, 1, 'BPM Process Server Prod 1', 'bpmpsprod01.dephub.go.id', '192.168.50.187', '4', '16 GB', '200 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(8, 1, 'BPM Process Server Dev', 'bpmpsdev.dephub.go.id', '192.168.50.188', '2', '8 GB', '100 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(9, 1, 'DB2 PureScale 01', 'db2ps01.dephub.go.id', '192.168.50.192', '2', '32 GB', '300 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(10, 1, 'DB2 PureScale CF Primary', 'db2cfp.dephub.go.id', '192.168.50.73', '1', '8 GB', '100 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(11, 2, 'BPM Process Server Prod 2', 'bpmpsprod02.dephub.go.id', '192.168.50.189', '4', '16 GB', '200 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(12, 2, 'BPM Process Server UAT', 'bpmpsuat.dephub.go.id', '192.168.50.194', '2', '8 GB', '100 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(13, 2, 'DB2 PureScale 02', 'db2ps02.dephub.go.id', '192.168.50.193', '2', '32 GB', '300 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-14 23:51:10', '2021-12-14 23:51:10'),
(14, 2, 'DB2 PureScale CF Secondary', 'db2cfs.dephub.go.id', '192.168.50.74', '1', '8 GB', '100 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-14 23:51:10', '2021-12-14 23:51:10');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('eUIU057cBWwy5JfuUSFcgFPCEMMmKLmMR0XJPewH', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMUQxaVI4ZVd6dnBzUlVVNW1ucU9xNFFNbklkZU5xMWMySTdzMXFlTSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTc6Imh0dHA6Ly9pbnZlbnRhcmlzaW5mcmFkaXRsYWxhLmxvY2FsOjgwODgvbWFzdGVyL2Rhc2hib2FyZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1640339421);

-- --------------------------------------------------------

--
-- Table structure for table `tipes`
--

CREATE TABLE `tipes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_tipe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_barangs`
--

CREATE TABLE `t_barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_barang_id` bigint(20) UNSIGNED NOT NULL,
  `jenis_atau_tipe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengadaans_id` bigint(20) UNSIGNED NOT NULL,
  `garansi_awal` date DEFAULT NULL,
  `garansi_akhir` date DEFAULT NULL,
  `support_awal` date DEFAULT NULL,
  `support_akhir` date DEFAULT NULL,
  `lokasis_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` text COLLATE utf8mb4_unicode_ci,
  `kategori_barang_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `isHypervisor` tinyint(1) NOT NULL DEFAULT '0',
  `mac_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_barangs`
--

INSERT INTO `t_barangs` (`id`, `nama_barang`, `jenis_barang_id`, `jenis_atau_tipe`, `pengadaans_id`, `garansi_awal`, `garansi_akhir`, `support_awal`, `support_akhir`, `lokasis_id`, `created_at`, `updated_at`, `serial_number`, `position`, `kategori_barang_id`, `isHypervisor`, `mac_address`) VALUES
(3, 'Load Balancer Big IP F5 i5600 Series', 5, 'F5', 1, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-20 00:25:32', 'f5-pcua-pjhm', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":\"23\"}', 1, 0, '-'),
(4, 'Load Balancer Big IP F5 i5600 Series', 5, 'F5', 1, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'f5-ugjw-taqf', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":22}', 1, 0, '-'),
(5, 'IBM PowerLinux 8247 22L', 1, 'IBM', 1, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '687AD8A', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":18}', 1, 0, '-'),
(6, 'IBM PowerLinux 8247 22L', 1, 'IBM', 1, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '687AD7A', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":16}', 1, 0, '-'),
(7, 'Lenovo ThinkSystem SR530', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J300ACW6', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":33}', 1, 0, '-'),
(8, 'Lenovo ThinkSystem SR530', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J300ACW5', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":32}', 1, 0, '-'),
(9, 'Lenovo ThinkSystem SR530', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J300ACW4', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":31}', 1, 0, '-'),
(10, 'Lenovo ThinkSystem DS4200', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J34529E', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":25}', 1, 0, '-'),
(11, 'Lenovo ThinkSystem DS4200', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J34529F', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":24}', 1, 0, '-'),
(12, 'Lenovo B300 SAN Switch', 3, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '-', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":15}', 1, 0, '-'),
(14, 'Lenovo Rack Switch NE1032T', 3, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '---', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":36}', 1, 0, '-'),
(15, 'Lenovo ThinkSystem SR530', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J300ACW7', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":30}', 1, 0, '-'),
(16, 'IBM System Storage TS3100 Tape Library Model L2U', 5, 'IBM', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '78CF541', '{\"row\":\"1\",\"rack\":\"3\",\"unit\":28}', 1, 0, '-'),
(17, '715HD3 Switch : Lenovo ThinkSystem NE1032 Rack Swich', 3, 'Lenovo', 3, '2018-08-03', '2022-08-31', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'MM58722', '{\"row\":\"3\",\"rack\":\"5\",\"unit\":41}', 1, 0, '-'),
(18, 'Lenovo System X3550 m5', 1, 'LENOVO', 2, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', '687AADD', '{\"row\":\"1\",\"rack\":\"9\",\"unit\":20}', 1, 0, '-'),
(19, '7X84CTO6WW Nutanix Cluster : ThinkAgile HX7520', 1, 'Lenovo ThinkAgile', 3, '2018-08-03', '2022-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J3024T60', '{\"row\":\"3\",\"rack\":\"5\",\"unit\":2}', 1, 1, '-'),
(20, '7X84CTO6WW Nutanix Cluster : ThinkAgile HX7520', 1, 'Lenovo ThinkAgile', 3, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J3024T5Y', '{\"row\":\"3\",\"rack\":\"5\",\"unit\":6}', 1, 1, '-'),
(21, '7X84CTO6WW Nutanix Cluster : ThinkAgile HX7520', 1, 'Lenovo ThinkAgile', 3, '2018-08-03', '2021-08-02', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'J3024T5Z', '{\"row\":\"3\",\"rack\":\"5\",\"unit\":4}', 1, 1, '-'),
(23, 'Server IBM X 3650 M3', 1, 'IBM', 4, '2021-11-01', '2021-11-30', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(24, 'Server IBM X 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(25, 'Server IBM X 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(26, 'Server IBM X 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(27, 'SAN Switch 249824E Storwize V7000', 3, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(28, 'SAN Switch 249824E Storwize V7000', 3, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(29, 'Server X 3550M5 Lenovo', 1, 'Lenovo', 4, '2021-11-01', '2021-12-31', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"5A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(30, 'Server X 3550 M5 Lenovo', 1, 'Lenovo', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"5A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(31, 'Server X 3550 M5 Lenovo', 1, 'Lenovo', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"6A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(32, 'Server X 3850 X6 Lenovo', 1, 'LENOVO', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"5A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(33, 'Server X 3850 X6 Lenovo', 1, 'LENOVO', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"6A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(34, 'Server X 3850 X5 IBM', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"6A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(35, 'IBM System P560', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"8C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(36, 'IBM PowerPC 520', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"8B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(37, 'Lenovo System x3850 X6', 1, 'LENOVO', 1, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'SV53167134', '{\"row\":\"3\",\"rack\":\"8A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(38, 'Dell Switch S4128', 3, 'DELL', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"8C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(39, 'Dell Switch S4128', 3, 'DELL', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"8C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(40, 'Cisco 2950 Series', 3, 'CISCO', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(41, 'Cisco 2950 Series', 3, 'CISCO', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(42, 'IBM PowerPC 550', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(43, 'HMC IBM X3550 KVM', 2, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(44, 'IBM PowerPC 550', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(45, 'IBM PowerPC 550', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(46, 'IBM PowerPC 550', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"9A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(47, 'server ibm x 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(48, 'Switch UBNT', 3, 'UBNT', 4, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(49, 'Server IBM X 3650 M3', 1, 'IBM', 1, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(50, 'Server IBM X 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(51, 'Server IBM X 3650 M3', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"2\",\"rack\":\"4A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(52, 'IBM Power S822L', 1, 'IBM', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(53, 'IBM SAN SWITCH 248-5', 3, 'IBM', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(54, 'Hitachi VSP G200', 4, 'Hitachi', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(55, 'IBM SAN SWITCH 248-5', 3, 'IBM', 1, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10C\",\"unit\":\"NULL\"}', 1, 0, '-'),
(56, 'Hitachi VSP G200', 4, 'Hitachi', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10A\",\"unit\":\"NULL\"}', 1, 0, '-'),
(57, 'Brocade SAN 6505', 3, 'Brocade', 1, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(58, 'Hitachi Quanta DB51-2U', 1, 'Hitachi', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(59, 'Brocade SAN 6505', 3, 'Brocade', 1, '1970-01-01', '1970-01-01', NULL, NULL, 2, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(60, 'Hitachi Quanta DB51-2U', 1, 'Hitachi', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(61, 'Brocade SAN 6505', 3, 'Brocade', 1, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(62, 'Brocade SAN 6505', 3, 'Brocade', 4, '1970-01-01', '1970-01-01', NULL, NULL, 1, '2021-12-14 22:30:21', '2021-12-14 22:30:21', 'NULL', '{\"row\":\"3\",\"rack\":\"10B\",\"unit\":\"NULL\"}', 1, 0, '-'),
(67, 'IBM IIAS', 1, 'IBM', 1, NULL, NULL, NULL, NULL, 1, '2021-12-20 03:21:55', '2021-12-20 03:21:55', NULL, '{\"row\":\"2\",\"rack\":\"12\",\"unit\":null}', 1, 0, '-'),
(68, 'Lenovo ThinkSystem SR650', 1, 'Lenovo', 1, NULL, NULL, NULL, NULL, 1, '2021-12-20 03:23:36', '2021-12-20 03:23:36', NULL, '{\"row\":\"1\",\"rack\":\"10\",\"unit\":null}', 1, 0, '-'),
(69, 'Qnap ES-1640DCv2', 1, 'QNAP', 1, NULL, NULL, NULL, NULL, 1, '2021-12-20 03:24:13', '2021-12-20 03:24:13', NULL, '{\"row\":\"3\",\"rack\":\"6\",\"unit\":null}', 1, 0, '-');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_machines`
--

CREATE TABLE `virtual_machines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `node_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vCPU` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vRAM` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vDISK` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `OS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `serive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `power` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `virtual_machines`
--

INSERT INTO `virtual_machines` (`id`, `node_id`, `name`, `description`, `hostname`, `ip_address`, `vCPU`, `vRAM`, `vDISK`, `OS`, `component`, `serive`, `used_space`, `power`, `meta`, `created_at`, `updated_at`) VALUES
(3, 6, 'test', '123', '-', '[\"123\",\"234\"]', '123', '123', '123', '123', '123', '123', '100', 'on', NULL, '2021-12-23 20:14:08', '2021-12-23 20:14:08'),
(5, 9, 'test', '123', '-', '[\"123\",\"234\"]', '123', '123', '123', '123', '123', '123', '100', 'on', NULL, '2021-12-23 20:14:08', '2021-12-23 20:14:08'),
(6, 9, 'test', '123', '-', '[\"123\",\"234\"]', '123', '123', '123', '123', '123', '123', '100', 'on', NULL, '2021-12-23 20:14:08', '2021-12-23 20:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `vm_ware`
--

CREATE TABLE `vm_ware` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_os` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IP_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memory_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provisioned_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vm_ware`
--

INSERT INTO `vm_ware` (`id`, `hostname`, `guest_os`, `state`, `IP_address`, `memory_size`, `cpus`, `provisioned_space`, `used_space`, `created_at`, `updated_at`) VALUES
(1, 'app-02', 'Microsoft Windows Server 2016 or later (64-bit)', 'Powered Off', '192.168.58.102', '8 GB', '4', '108.25 GB', '8.35 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(2, 'App-1', 'Microsoft Windows Server 2016 or later (64-bit)', 'Powered Off', 'NULL', '8 GB', '2', '108.24 GB', '8.14 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(3, 'DB PPKA-Hibah-Simlala', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.30', '16 GB', '8', '500 GB', '41 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(4, 'Formgen PPKA-SIJUKA', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.19', '16 GB', '8', '500 GB', '46.78 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(5, 'IPNDCTAPP01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.20', '16 GB', '8', '60 GB', '29.88 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(6, 'IPNDCTAPP02', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.21', '16 GB', '8', '60 GB', '24.55 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(7, 'IPNDCTAPP03', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.22', '16 GB', '8', '60 GB', '24.3 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(8, 'IPNDCTAPP04', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.23', '16 GB', '8', '60 GB', '24.99 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(9, 'IPNDCTAPP05', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.24', '16 GB', '8', '60 GB', '25.31 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(10, 'IPNDCTAPP06', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.25', '16 GB', '8', '60 GB', '24.9 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(11, 'IPNDCTAPP07', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.26', '16 GB', '8', '60 GB', '25.07 GB', '2021-12-14 23:28:20', '2021-12-14 23:28:20'),
(12, 'IPNDCTAPP08', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.27', '16 GB', '8', '60 GB', '24.49 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(13, 'IPNDCTAPP09', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.28', '16 GB', '8', '60 GB', '24.14 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(14, 'IPNDCTAPP10', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.29', '16 GB', '8', '60 GB', '24.53 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(15, 'IPNDCTAPP11', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.70', '16 GB', '8', '60 GB', '24.19 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(16, 'IPNDCTAPP12', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.71', '16 GB', '8', '60 GB', '25.02 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(17, 'IPNDCTAPP13', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.72', '16 GB', '8', '60 GB', '25.63 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(18, 'IPNDCTAPP14', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.73', '16 GB', '8', '60 GB', '24.19 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(19, 'IPNDCTAPP15', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.74', '16 GB', '8', '60 GB', '24.44 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(20, 'IPNDCTAPP16', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.75', '16 GB', '8', '60 GB', '24.14 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(21, 'IPNDCTAPP17', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.76', '16 GB', '8', '60 GB', '24.01 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(22, 'IPNDCTAPP18', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.77', '16 GB', '8', '60 GB', '24.56 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(23, 'IPNDCTAUTOMATION', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.53', '8 GB', '2', '64.85 GB', '26.46 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(24, 'IPNDCTDB01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.40', '16 GB', '4', '260 GB', '174.1 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(25, 'IPNDCTDB02', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.41', '16 GB', '4', '260 GB', '171.56 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(26, 'IPNDCTDB03', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.42', '16 GB', '4', '260 GB', '172.17 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(27, 'IPNDCTDB04', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.43', '16 GB', '4', '260 GB', '204.96 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(28, 'IPNDCTDB05', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.44', '16 GB', '4', '260 GB', '173.44 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(29, 'IPNDCTDB06', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.45', '16 GB', '4', '260 GB', '177.77 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(30, 'IPNDCTLBAPP', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.58', '8 GB', '2', '260 GB', '48.61 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(31, 'IPNDCTLBDB', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.57', '8 GB', '2', '263.3 GB', '251.7 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(32, 'IPNDCTMONITORING', 'CentOS 8 (64-bit)', 'Powered Off', '192.168.58.54', '8 GB', '2', '68.21 GB', '4.91 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(33, 'IPNDCTNFS01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.50', '16 GB', '4', '7.06 TB', '5.78 TB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(34, 'NTNX-J3024T5Y-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.16', '32 GB', '12', '102.95 MB', '102.95 MB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(35, 'NTNX-J3024T5Z-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.14', '32 GB', '12', '103.01 MB', '103.01 MB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(36, 'NTNX-J3024T60-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.15', '32 GB', '12', '102.69 MB', '102.69 MB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(37, 'Nutanix VCenter', 'Other 3.x Linux (64-bit)', 'Powered On', '192.168.58.17', '16 GB', '4', '477 GB', '118.67 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(38, 'PPKA-SIJUKA', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.18', '16 GB', '8', '1 TB', '44.48 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(39, 'Scheduler Server-02_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered On', '192.168.50.179', '16 GB', '8', '300 GB', '10.87 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(40, 'Simlala WebApps-01_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered Off', 'NULL', '16 GB', '4', '316.23 GB', '69.59 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(41, 'Simlala WebApps-04_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered Off', '192.168.50.195', '16 GB', '4', '316.21 GB', '86.63 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(42, 'SIMLALA-NFS-BACKUP', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.103', '8 GB', '4', '2.45 TB', '1.58 TB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(43, 'SuperSet', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.120', '16 GB', '8', '60 GB', '30.32 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21'),
(44, 'Veeam', 'Microsoft Windows Server 2012 (64-bit)', 'Powered On', '192.168.58.100', '16 GB', '2', '100.02 GB', '100.02 GB', '2021-12-14 23:28:21', '2021-12-14 23:28:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cluster`
--
ALTER TABLE `cluster`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cluster_cluster_system_id_foreign` (`cluster_system_id`);

--
-- Indexes for table `cluster_systems`
--
ALTER TABLE `cluster_systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `ip_addresses`
--
ALTER TABLE `ip_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_addresses_t_barangs_id_foreign` (`t_barangs_id`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_virtual_machines`
--
ALTER TABLE `jenis_virtual_machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasis`
--
ALTER TABLE `lokasis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merks`
--
ALTER TABLE `merks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nodes_cluster_id_foreign` (`cluster_id`);

--
-- Indexes for table `nutanixes`
--
ALTER TABLE `nutanixes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengadaans`
--
ALTER TABLE `pengadaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `power_vm`
--
ALTER TABLE `power_vm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `power_vm_list`
--
ALTER TABLE `power_vm_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `power_vm_list_power_vm_id_foreign` (`power_vm_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `tipes`
--
ALTER TABLE `tipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_barangs`
--
ALTER TABLE `t_barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_barangs_jenis_barang_id_foreign` (`jenis_barang_id`),
  ADD KEY `t_barangs_pengadaans_id_foreign` (`pengadaans_id`),
  ADD KEY `t_barangs_lokasis_id_foreign` (`lokasis_id`),
  ADD KEY `t_barangs_kategori_barang_id_foreign` (`kategori_barang_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `virtual_machines`
--
ALTER TABLE `virtual_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `virtual_machines_node_id_foreign` (`node_id`);

--
-- Indexes for table `vm_ware`
--
ALTER TABLE `vm_ware`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cluster`
--
ALTER TABLE `cluster`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cluster_systems`
--
ALTER TABLE `cluster_systems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_addresses`
--
ALTER TABLE `ip_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jenis_virtual_machines`
--
ALTER TABLE `jenis_virtual_machines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lokasis`
--
ALTER TABLE `lokasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `merks`
--
ALTER TABLE `merks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nutanixes`
--
ALTER TABLE `nutanixes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pengadaans`
--
ALTER TABLE `pengadaans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `power_vm`
--
ALTER TABLE `power_vm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `power_vm_list`
--
ALTER TABLE `power_vm_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tipes`
--
ALTER TABLE `tipes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_barangs`
--
ALTER TABLE `t_barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_machines`
--
ALTER TABLE `virtual_machines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vm_ware`
--
ALTER TABLE `vm_ware`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cluster`
--
ALTER TABLE `cluster`
  ADD CONSTRAINT `cluster_cluster_system_id_foreign` FOREIGN KEY (`cluster_system_id`) REFERENCES `cluster_systems` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ip_addresses`
--
ALTER TABLE `ip_addresses`
  ADD CONSTRAINT `ip_addresses_t_barangs_id_foreign` FOREIGN KEY (`t_barangs_id`) REFERENCES `t_barangs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `nodes`
--
ALTER TABLE `nodes`
  ADD CONSTRAINT `nodes_cluster_id_foreign` FOREIGN KEY (`cluster_id`) REFERENCES `cluster` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `power_vm_list`
--
ALTER TABLE `power_vm_list`
  ADD CONSTRAINT `power_vm_list_power_vm_id_foreign` FOREIGN KEY (`power_vm_id`) REFERENCES `power_vm` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `t_barangs`
--
ALTER TABLE `t_barangs`
  ADD CONSTRAINT `t_barangs_jenis_barang_id_foreign` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_barangs_kategori_barang_id_foreign` FOREIGN KEY (`kategori_barang_id`) REFERENCES `kategori_barang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_barangs_lokasis_id_foreign` FOREIGN KEY (`lokasis_id`) REFERENCES `lokasis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_barangs_pengadaans_id_foreign` FOREIGN KEY (`pengadaans_id`) REFERENCES `pengadaans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `virtual_machines`
--
ALTER TABLE `virtual_machines`
  ADD CONSTRAINT `virtual_machines_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
