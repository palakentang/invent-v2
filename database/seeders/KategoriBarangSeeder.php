<?php

namespace Database\Seeders;

use App\Models\KategoriBarang;
use Illuminate\Database\Seeder;

class KategoriBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriBarang::create([
            'kd_kategori_barang' => 'KB-0001',
            'nama'               => 'Hardware'
        ]);
        KategoriBarang::create([
            'kd_kategori_barang' => 'KB-0002',
            'nama'               => 'Software'
        ]);
    }
}
