<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'superbubur',
            'email' => 'superadmin@ditlala.go.id',
            'password' => bcrypt('rahasia')
        ]);
        User::create([
            'name' => 'admin',
            'email' => 'admin@ditlala.go.id',
            'password' => bcrypt('rahasia')
        ]);
        User::create([
            'name' => 'guest',
            'email' => 'guest@ditlala.go.id',
            'password' => bcrypt('rahasia')
        ]);
        
        $faker = Faker::create('id_ID');
        for ($i=0; $i < 100; $i++) { 
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('rahasia')
            ]);
        }
    }
}
