<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // UsersSeeder::class()->run();
        $this->call(KategoriBarangSeeder::class);
        $this->call(JenisBarangSeeder::class);
    }
}
