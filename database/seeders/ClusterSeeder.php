<?php

namespace Database\Seeders;

use App\Models\Cluster;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clusters')->insert([
            [
                'name' => 'Simlala Cluster HOST#1',
                'IP' => '-',
                'host' => 'dllhost01.dephub.go.id'
            ],
            [
                'name' => 'Simlala Cluster HOST#2',
                'IP' => '-',
                'host' => 'dllhost02.dephub.go.id'
            ],
            [
                'name' => 'Simlala Cluster HOST#3',
                'IP' => '-',
                'host' => 'dllhost03.dephub.go.id'
            ],
            [
                'name' => 'NTX-CLUSTER',
                'host' => '192.168.58.10',
                'IP' => '-'
            ],
            [
                'name' => 'NTX-CLUSTER',
                'host' => '192.168.58.11',
                'IP' => '-'
            ],
            [
                'name' => 'NTX-CLUSTER',
                'host' => '192.168.58.12',
                'IP' => '-'
            ],
            [
                'name' => 'Simlala Cluster HOST#1',
                'IP' => '-',
                'host' => 'dllhost01.dephub.go.id'
            ],
        ]);

        // $nutanix = Cluster::create([
        //     [
        //         'name' => 'NTX-CLUSTER',
        //         'host' => '192.168.58.10',
        //         'IP' => '-'
        //     ],
        //     [
        //         'name' => 'NTX-CLUSTER',
        //         'host' => '192.168.58.11',
        //         'IP' => '-'
        //     ],
        //     [
        //         'name' => 'NTX-CLUSTER',
        //         'host' => '192.168.58.12',
        //         'IP' => '-'
        //     ],
        // ]);

        // $powerVM = Cluster::create([
        //     [
        //         'name' => 'Simlala Cluster HOST#1',
        //         'IP' => '-',
        //         'host' => 'dllhost01.dephub.go.id'
        //     ],
        // ]);

    }
}
