<?php

namespace Database\Seeders;

use App\Models\JenisBarang;
use Illuminate\Database\Seeder;

class JenisBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0001',
            'nama'            => 'Load Balancer'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0002',
            'nama'            => 'Server BPM'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0003',
            'nama'            => 'Server Primary DC DRC'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0004',
            'nama'            => 'Storage'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0005',
            'nama'            => 'SAN Switch'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0006',
            'nama'            => 'Network Switch'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0007',
            'nama'            => 'Server Backup'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0008',
            'nama'            => 'Tape Backup'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0009',
            'nama'            => 'Server'
        ]);
        JenisBarang::create([
            'kd_jenis_barang' => 'JB-0010',
            'nama'            => 'Server ETL'
        ]);
        
    }
}
