<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVirtualMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virtual_machines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('node_id');
            $table->foreign('node_id')
                ->references('id')
                ->on('nodes')
                ->onDelete('cascade');

            $table->string('name')->nullable()->default('-');
            $table->string('description')->nullable()->default('-');
            $table->string('hostname')->nullable()->default('-');
            $table->string('ip_address')->nullable()->default('-');
            $table->string('vCPU')->nullable()->default('-');
            $table->string('vRAM')->nullable()->default('-');
            $table->string('vDISK')->nullable()->default('-');
            $table->string('OS')->nullable()->default('-');
            $table->string('component')->nullable()->default('-');
            $table->string('serive')->nullable()->default('-');
            $table->string('used_space')->nullable()->default('-');
            $table->string('power')->nullable()->default('-');
            $table->text('meta')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_machines');
    }
}
