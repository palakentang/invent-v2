<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_barangs', function (Blueprint $table) {
            $table->id();
            $table->string('nama_barang')->nullable(false);

            // Jenis Barang
            $table->unsignedBigInteger('jenis_barang_id');
            $table->foreign('jenis_barang_id')
                    ->on('jenis_barang')
                    ->references('id')
                    ->onDelete('cascade');

            $table->string('jenis_atau_tipe')->nullable(false);

            // Pengadaan
            $table->unsignedBigInteger('pengadaans_id');
            $table->foreign('pengadaans_id')
                    ->on('pengadaans')
                    ->references('id')
                    ->onDelete('cascade');
            
            $table->date('garansi_awal')->nullable()->default(null);
            $table->date('garansi_akhir')->nullable()->default(null);

            $table->date('support_awal')->nullable()->default(null);
            $table->date('support_akhir')->nullable()->default(null);

            $table->unsignedBigInteger('lokasis_id');
            $table->foreign('lokasis_id')
                    ->on('lokasis')
                    ->references('id')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_barangs');
    }
}
