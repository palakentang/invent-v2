<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNutanixesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutanixes', function (Blueprint $table) {
            $table->id();
            $table->string('host')->nullable();
            $table->string('name')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('core')->nullable();
            $table->string('memory_capacity')->nullable();
            $table->string('provisioned_space')->nullable();
            $table->string('used_space')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutanixes');
    }
}
