<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldToTBarangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_barangs', function (Blueprint $table) {
            $table->string('serial_number')->nullable(true);
            $table->text('position')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_barangs', function (Blueprint $table) {
            $table->dropColumn('serial_number');
            $table->dropColumn('position');
        });
    }
}
