<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClusterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cluster_system_id');
            $table->foreign('cluster_system_id')
                ->references('id')
                ->on('cluster_systems')
                ->onDelete('cascade');
            $table->string('name')->nullable(true);
            $table->string('description')->nullable(true);
            $table->text('meta')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cluster');
    }
}
