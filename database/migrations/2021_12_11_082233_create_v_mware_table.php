<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVMwareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vm_ware', function (Blueprint $table) {
            $table->id();
            $table->string('hostname')->nullable();
            $table->string('guest_os')->nullable();
            $table->string('state')->nullable();
            $table->string('IP_address')->nullable();
            $table->string('memory_size')->nullable();
            $table->string('cpus')->nullable();
            $table->string('provisioned_space')->nullable();
            $table->string('used_space')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vm_ware');
    }
}
