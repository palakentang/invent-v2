<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerVMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_vm', function (Blueprint $table) {
            $table->id();
            $table->string('kd_power_vm')->nullable(false);
            $table->string('vm_name')->nullable(false);
            $table->string('hostname')->nullable(true);
            $table->string('vm_ip')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_vm');
    }
}
