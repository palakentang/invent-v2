<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerVMListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_vm_list', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('power_vm_id');
            $table->foreign('power_vm_id')
                    ->references('id')
                    ->on('power_vm')
                    ->onDelete('cascade');
            $table->string('deskripsi')->nullable();
            $table->string('hostname')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('vCpu')->nullable();
            $table->string('vRam')->nullable();
            $table->string('vDisk')->nullable();
            $table->string('OS')->nullable();
            $table->string('copmonents')->nullable();
            $table->string('service')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_vm_list');
    }
}
