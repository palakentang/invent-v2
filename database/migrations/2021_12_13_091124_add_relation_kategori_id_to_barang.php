<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationKategoriIdToBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_barangs', function (Blueprint $table) {
            $table->unsignedBigInteger('kategori_barang_id')->default(1);
            $table->foreign('kategori_barang_id')
                    ->references('id')
                    ->on('kategori_barang')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_barangs', function (Blueprint $table) {
            $table->dropForeign('t_barangs_kategori_barang_id_foreign');
            $table->dropColumn('kategori_barang_id');
        });
    }
}
