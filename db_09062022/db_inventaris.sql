-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table db_inventaris.cluster
CREATE TABLE IF NOT EXISTS `cluster` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cluster_system_id` bigint(20) unsigned NOT NULL,
  `nama_cluster` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cluster_cluster_system_id_foreign` (`cluster_system_id`),
  CONSTRAINT `cluster_cluster_system_id_foreign` FOREIGN KEY (`cluster_system_id`) REFERENCES `cluster_systems` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.cluster: ~7 rows (approximately)
/*!40000 ALTER TABLE `cluster` DISABLE KEYS */;
INSERT INTO `cluster` (`id`, `cluster_system_id`, `nama_cluster`, `description`, `meta`, `created_at`, `updated_at`) VALUES
	(4, 3, 'SIMLALA Back-End', NULL, NULL, '2022-05-20 11:18:55', '2022-05-20 11:19:10'),
	(5, 6, 'SIMLALA Front-End', NULL, NULL, '2022-05-20 11:19:32', '2022-06-07 07:42:13'),
	(6, 2, 'Inaportnet', NULL, NULL, '2022-05-20 11:35:16', '2022-05-20 11:35:16'),
	(7, 3, 'Data Warehouse Ditlala', NULL, NULL, '2022-05-21 17:58:59', '2022-05-21 17:58:59'),
	(8, 7, 'Ditlala Network', NULL, NULL, '2022-05-31 18:24:20', '2022-05-31 18:24:20'),
	(9, 8, 'Ditlala System Support', NULL, NULL, '2022-05-31 18:25:03', '2022-05-31 18:25:03');
/*!40000 ALTER TABLE `cluster` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.cluster_systems
CREATE TABLE IF NOT EXISTS `cluster_systems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.cluster_systems: ~6 rows (approximately)
/*!40000 ALTER TABLE `cluster_systems` DISABLE KEYS */;
INSERT INTO `cluster_systems` (`id`, `name`, `description`, `meta`, `created_at`, `updated_at`) VALUES
	(2, 'VMware Over Nutanix', 'VMware on Top Nutanix Cluster', NULL, '2021-12-23 11:20:25', '2022-05-17 12:23:08'),
	(3, 'IBM PowerVM', 'Cluster IBM PowerVM', NULL, '2021-12-23 11:20:32', '2022-05-17 12:24:20'),
	(5, 'VMWare VSAN', 'VSAN VMware Cluster', NULL, '2022-05-17 12:22:22', '2022-05-17 12:22:22'),
	(6, 'Bare Metal', 'Single Server hardware', NULL, '2022-05-17 12:49:07', '2022-05-17 12:49:07'),
	(7, 'Network Support Devices', 'Perangkat Pendukung Network', NULL, '2022-05-31 18:22:55', '2022-05-31 18:22:55'),
	(8, 'System Support Devices', 'Perangkat Pendukung Sistem', NULL, '2022-05-31 18:23:52', '2022-05-31 18:23:52');
/*!40000 ALTER TABLE `cluster_systems` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.ip_addresses
CREATE TABLE IF NOT EXISTS `ip_addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `t_barangs_id` bigint(20) unsigned NOT NULL,
  `ip_address` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isMgmt` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.ip_addresses: ~4 rows (approximately)
/*!40000 ALTER TABLE `ip_addresses` DISABLE KEYS */;
INSERT INTO `ip_addresses` (`id`, `t_barangs_id`, `ip_address`, `isMgmt`, `created_at`, `updated_at`) VALUES
	(57, 86, '192.168.72.218', 1, '2022-01-31 12:38:11', '2022-01-31 12:38:11'),
	(58, 86, ' 192.168.72.219', 0, '2022-01-31 12:38:11', '2022-01-31 12:38:11'),
	(59, 87, '10.252.242.161', 0, '2022-05-31 18:47:13', '2022-05-31 18:47:13'),
	(60, 87, '192.168.4.100', 0, '2022-05-31 18:47:13', '2022-05-31 18:47:13');
/*!40000 ALTER TABLE `ip_addresses` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.jenis_barang
CREATE TABLE IF NOT EXISTS `jenis_barang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jenis_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_meta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.jenis_barang: ~7 rows (approximately)
/*!40000 ALTER TABLE `jenis_barang` DISABLE KEYS */;
INSERT INTO `jenis_barang` (`id`, `nama_jenis_barang`, `created_at`, `updated_at`, `_meta`) VALUES
	(1, 'Server', '2021-12-14 02:36:06', '2021-12-14 02:36:06', 'text-danger'),
	(3, 'Network', '2021-12-14 02:36:19', '2022-05-12 07:32:54', 'text-primary'),
	(4, 'Storage', '2021-12-14 02:36:24', '2021-12-14 02:36:24', 'text-warning'),
	(5, 'Load Balancer', '2021-12-14 02:36:37', '2022-01-13 04:38:05', 'text-info'),
	(6, 'Tape Backup', '2021-12-14 02:36:37', '2021-12-14 02:36:37', 'text-secondary'),
	(10, 'SAN Switch', '2022-01-20 16:48:19', '2022-01-20 16:48:19', NULL),
	(11, 'Perangkat Lunak', '2022-05-17 12:20:00', '2022-05-17 12:20:00', NULL);
/*!40000 ALTER TABLE `jenis_barang` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.jenis_virtual_machines
CREATE TABLE IF NOT EXISTS `jenis_virtual_machines` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `_meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.jenis_virtual_machines: ~2 rows (approximately)
/*!40000 ALTER TABLE `jenis_virtual_machines` DISABLE KEYS */;
INSERT INTO `jenis_virtual_machines` (`id`, `nama`, `_meta`, `created_at`, `updated_at`) VALUES
	(1, 'Power VM', 'text-danger', NULL, NULL),
	(2, 'VM Ware', 'text-success', NULL, NULL);
/*!40000 ALTER TABLE `jenis_virtual_machines` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.lokasis
CREATE TABLE IF NOT EXISTS `lokasis` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_lokasi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_lokasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.lokasis: ~2 rows (approximately)
/*!40000 ALTER TABLE `lokasis` DISABLE KEYS */;
INSERT INTO `lokasis` (`id`, `nama_lokasi`, `detail_lokasi`, `created_at`, `updated_at`) VALUES
	(1, 'DCT', 'Data Center Medan Merdeka Timur', '2022-01-12 12:30:26', '2022-01-12 18:32:42'),
	(3, 'JTN', 'Data Center Jatinegara', '2022-01-12 12:31:48', '2022-01-12 12:31:48'),
	(4, 'Ditlala', 'Direktorat Lalu Lintas dan Angkutan Laut', '2022-01-14 19:58:38', '2022-01-14 19:58:38');
/*!40000 ALTER TABLE `lokasis` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.merks
CREATE TABLE IF NOT EXISTS `merks` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.merks: ~11 rows (approximately)
/*!40000 ALTER TABLE `merks` DISABLE KEYS */;
INSERT INTO `merks` (`id`, `nama_merk`, `created_at`, `updated_at`) VALUES
	(1, 'F5', '2022-01-07 02:49:54', '2022-01-07 02:49:57'),
	(2, 'IBM', '2022-01-07 02:51:30', '2022-01-07 02:51:32'),
	(3, 'Lenovo', '2022-01-07 02:51:51', '2022-01-07 02:51:53'),
	(4, 'Dell', '2022-01-07 02:54:37', '2022-01-07 02:54:38'),
	(5, 'Hitachi', '2022-01-07 02:54:40', '2022-01-07 02:54:39'),
	(6, 'Brocade', '2022-01-07 02:54:41', '2022-01-07 02:54:42'),
	(7, 'QNAP', '2022-01-07 02:54:44', '2022-01-07 02:54:43'),
	(9, 'HPE', '2022-01-13 05:11:52', '2022-01-13 05:11:52'),
	(10, 'DellEMC', '2022-01-15 18:35:53', '2022-01-15 18:35:53'),
	(11, 'VMware', '2022-06-08 19:16:58', '2022-06-08 19:16:58'),
	(12, 'Nutanix', '2022-06-08 19:29:47', '2022-06-08 19:29:47'),
	(13, 'Microsoft', '2022-06-08 20:10:57', '2022-06-08 20:10:57');
/*!40000 ALTER TABLE `merks` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.migrations: ~34 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2021_11_11_034937_create_jenis_barangs_table', 1),
	(6, '2021_11_11_070112_create_kategori_barangs_table', 1),
	(7, '2021_11_15_061620_create_lokasis_table', 1),
	(8, '2021_12_09_041613_create_pengadaans_table', 1),
	(9, '2021_12_09_042043_create_merks_table', 1),
	(10, '2021_12_09_042103_create_tipes_table', 1),
	(11, '2021_12_09_044806_create_sessions_table', 1),
	(12, '2021_12_09_070643_add_deskripsi_lokasi', 1),
	(13, '2021_12_09_082039_create_barangs_table', 1),
	(14, '2021_12_09_100731_add_some_field_to_t_barangs', 1),
	(15, '2021_12_09_105610_create_i_p_addresses_table', 1),
	(16, '2021_12_11_082233_create_v_mware_table', 1),
	(17, '2021_12_11_094629_create_nutanixes_table', 1),
	(18, '2021_12_11_102737_create_power_v_m_s_table', 1),
	(19, '2021_12_11_122248_create_power_v_m_lists_table', 1),
	(20, '2021_12_13_091124_add_relation_kategori_id_to_barang', 2),
	(21, '2021_12_16_093503_create_jenis_virtual_machines_table', 3),
	(25, '2021_12_16_100507_create_virtual_machines_table', 4),
	(26, '2021_12_16_124212_add_field_meta_to_jenis_barang', 5),
	(27, '2021_12_17_105638_add_meta_field_virtualmachines', 6),
	(28, '2021_12_20_063637_add_field_mac_address_field', 7),
	(29, '2021_12_20_071413_add_is_hypervisor_field', 8),
	(30, '2021_12_20_100224_add_barang_id_virtual_machine', 9),
	(31, '2021_12_20_105048_add_some_field_virtual_mechines', 10),
	(32, '2021_12_20_110341_add_some_field_t_barangs', 11),
	(35, '2021_12_21_053101_create_clusters_table', 12),
	(44, '2021_12_23_042654_create_cluster_systems_table', 13),
	(45, '2021_12_23_043028_create_cluster_table', 13),
	(46, '2021_12_23_053034_create_nodes_table', 13),
	(47, '2021_12_23_053856_create_virtual_machines_table', 13);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.nodes
CREATE TABLE IF NOT EXISTS `nodes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cluster_id` bigint(20) unsigned NOT NULL,
  `node_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `machine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `ip_address` text COLLATE utf8mb4_unicode_ci,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nodes_cluster_id_foreign` (`cluster_id`),
  CONSTRAINT `nodes_cluster_id_foreign` FOREIGN KEY (`cluster_id`) REFERENCES `cluster` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.nodes: ~1 rows (approximately)
/*!40000 ALTER TABLE `nodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodes` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.nutanixes
CREATE TABLE IF NOT EXISTS `nutanixes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memory_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provisioned_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.nutanixes: ~42 rows (approximately)
/*!40000 ALTER TABLE `nutanixes` DISABLE KEYS */;
INSERT INTO `nutanixes` (`id`, `host`, `name`, `ip_address`, `core`, `memory_capacity`, `provisioned_space`, `used_space`, `created_at`, `updated_at`) VALUES
	(1, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'app-02', 'NULL', '4', '8 GiB', '100 GiB', '8.34 GiB', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(2, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'app-02', 'NULL', '4', '8 GiB', ' 100 GiB', '8.34 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(3, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'App-1', 'NULL', '2', '8 GiB', ' 100 GiB', '8.13 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(4, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'DB PPKA-Hibah-Simlala', '192.168.58.30 ', '8', '16 GiB', ' 500 GiB', '41.01 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(5, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Formgen PPKA-SIJUKA', '192.168.58.19 ', '8', '16 GiB', ' 500 GiB', '46.78 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(6, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP01', '192.168.58.20', '8', '16 GiB', ' 60 GiB', '29.87 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(7, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP02', '192.168.58.21 ', '8', '16 GiB', ' 60 GiB', '24.55 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(8, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP03', '192.168.58.22', '8', '16 GiB', '60 GiB', '24.3 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(9, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP04', '192.168.58.23', '8', '16 GiB', ' 60 GiB', '24.99 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(10, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP05', '192.168.58.24', '8', '16 GiB', ' 60 GiB', '25.31 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(11, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP06', '192.168.58.25', '8', '16 GiB', '60 GiB', '24.9 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(12, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP07', '192.168.58.26', '8', '16 GiB', ' 60 GiB', '25.07 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(13, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP08', '192.168.58.27', '8', '16 GiB', ' 60 GiB', '24.49 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(14, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP09', '192.168.58.28', '8', '16 GiB', ' 60 GiB', '24.14 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(15, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP10', '192.168.58.29', '8', '16 GiB', ' 60 GiB', '24.53 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(16, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP11', '192.168.58.70', '8', '16 GiB', ' 60 GiB', '24.19 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(17, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP12', '192.168.58.71', '8', '16 GiB', ' 60 GiB', '25.02 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(18, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP13', '192.168.58.72', '8', '16 GiB', ' 60 GiB', '25.63 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(19, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTAPP14', '192.168.58.73', '8', '16 GiB', ' 60 GiB', '24.19 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(20, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP15', '192.168.58.74', '8', '16 GiB', ' 60 GiB', '24.44 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(21, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP16', '192.168.58.75', '8', '16 GiB', ' 60 GiB', '24.14 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(22, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAPP17', '192.168.58.76', '8', '16 GiB', ' 60 GiB', '24.01 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(23, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTAPP18', '192.168.58.77', '8', '16 GiB', ' 60 GiB', '24.56 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(24, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTAUTOMATION', '192.168.58.53', '2', '8 GiB', ' 60 GiB', '25.45 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(25, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB01', '192.168.58.40 ', '4', '16 GiB', ' 260 GiB', '174.1 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(26, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTDB02', '192.168.58.41 ', '4', '16 GiB', ' 260 GiB', '171.56 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(27, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB03', '192.168.58.42 ', '4', '16 GiB', ' 260 GiB', '172.17 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(28, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTDB04', '192.168.58.43 ', '4', '16 GiB', ' 260 GiB', '173.54 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(29, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTDB05', '192.168.58.44 ', '4', '16 GiB', ' 260 GiB', '173.44 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(30, 'HPV-LALA-01.HPVLALA01.hubla.dephub.go.id/ESXi', 'IPNDCTDB06', '192.168.58.45 ', '4', '16 GiB', ' 260 GiB', '177.77 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(31, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTLBAPP', '192.168.58.58 ', '2', '8 GiB', ' 260 GiB', '48.61 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(32, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTLBDB', '192.168.58.57 ', '2', '8 GiB', ' 260 GiB', '137.02 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(33, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'IPNDCTMONITORING', 'NULL', '2', '8 GiB', '60 GiB', '4.91 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(34, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'IPNDCTNFS01', '192.168.58.50 ', '4', '16 GiB', '7.06 TiB', '4.82 TiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(35, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Nutanix VCenter', '192.168.58.17', '4', '16 GiB', ' 477 GiB', '118.67 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(36, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'PPKA-SIJUKA', '192.168.58.18 ', '8', '16 GiB', ' 1 TiB', '44.49 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(37, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Scheduler Server-02_replica', '192.168.50.179 ', '8', '16 GiB', ' 300 GiB', '10.87 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(38, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Simlala WebApps-01_replica', 'NULL', '4', '16 GiB', ' 300 GiB', '69.58 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(39, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'Simlala WebApps-04_replica', 'NULL', '4', '16 GiB', ' 300 GiB', '86.63 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(40, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'SIMLALA-NFS-BACKUP', '192.168.58.103 ', '4', '8 GiB', '2.45 TiB', '1.54 TiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(41, 'HPV-LALA-03.HPVLALA03.hubla.dephub.go.id/ESXi', 'SuperSet', '192.168.58.120', '8', '16 GiB', ' 60 GiB', '30.32 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34'),
	(42, 'HPV-LALA-02.HPVLALA02.hubla.dephub.go.id/ESXi', 'Veeam', '192.168.58.100', '2', '16 GiB', '100 GiB', '41.42 GiB ', '2021-12-15 06:35:34', '2021-12-15 06:35:34');
/*!40000 ALTER TABLE `nutanixes` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.pengadaans
CREATE TABLE IF NOT EXISTS `pengadaans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_pengadaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pengadaan` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.pengadaans: ~5 rows (approximately)
/*!40000 ALTER TABLE `pengadaans` DISABLE KEYS */;
INSERT INTO `pengadaans` (`id`, `nama_pengadaan`, `tgl_pengadaan`, `created_at`, `updated_at`) VALUES
	(5, 'Pengadaan Server dan Storage', '2019-07-10', '2022-01-05 09:57:32', '2022-06-07 12:53:58'),
	(8, 'Pengadaan Load Balancer', '2021-08-01', '2022-05-31 18:44:16', '2022-05-31 18:44:16'),
	(10, 'Pengadaan Nutanix', '2020-07-31', '2022-06-07 12:52:22', '2022-06-08 17:17:27'),
	(11, 'Pengadaan Server dan Storage SIMLALA', '2018-08-03', '2022-06-08 12:12:18', '2022-06-08 12:16:32'),
	(12, 'Pengadaan IBM IIAS', '2018-08-01', '2022-06-08 18:08:42', '2022-06-08 18:08:42');
/*!40000 ALTER TABLE `pengadaans` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.personal_access_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.power_vm
CREATE TABLE IF NOT EXISTS `power_vm` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kd_power_vm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vm_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.power_vm: ~2 rows (approximately)
/*!40000 ALTER TABLE `power_vm` DISABLE KEYS */;
INSERT INTO `power_vm` (`id`, `kd_power_vm`, `vm_name`, `hostname`, `vm_ip`, `created_at`, `updated_at`) VALUES
	(1, 'HOST #1', 'DITLALA PVM Host', 'dllpv01.dephub.go.id', '192.168.50.190', NULL, NULL),
	(2, 'HOST #2', 'DITLALA PVM', 'dllpv02.dephub.go.id', '192.168.50.191', NULL, NULL);
/*!40000 ALTER TABLE `power_vm` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.power_vm_list
CREATE TABLE IF NOT EXISTS `power_vm_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `power_vm_id` bigint(20) unsigned NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vCpu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vRam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vDisk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copmonents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `power_vm_list_power_vm_id_foreign` (`power_vm_id`),
  CONSTRAINT `power_vm_list_power_vm_id_foreign` FOREIGN KEY (`power_vm_id`) REFERENCES `power_vm` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.power_vm_list: ~10 rows (approximately)
/*!40000 ALTER TABLE `power_vm_list` DISABLE KEYS */;
INSERT INTO `power_vm_list` (`id`, `power_vm_id`, `deskripsi`, `hostname`, `ip_address`, `vCpu`, `vRam`, `vDisk`, `OS`, `copmonents`, `service`, `created_at`, `updated_at`) VALUES
	(5, 1, 'Deployment Manager', 'dllmgrbpm.dephub.go.id', '192.168.50.17', '4', '8GB', '100GB', 'SUSE 1', 'IBM WAS ND - Dmgr', 'Deployment Manager', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(6, 1, 'BPM Process Center', 'sllbpmpc.dephub.go.id', '192.168.50.186', '2', '8GB', '100 GB', 'SUSE 12', 'IBM BPM Process Center', 'SIMLALA BPM Process Center', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(7, 1, 'BPM Process Server Prod 1', 'bpmpsprod01.dephub.go.id', '192.168.50.187', '4', '16 GB', '200 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(8, 1, 'BPM Process Server Dev', 'bpmpsdev.dephub.go.id', '192.168.50.188', '2', '8 GB', '100 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(9, 1, 'DB2 PureScale 01', 'db2ps01.dephub.go.id', '192.168.50.192', '2', '32 GB', '300 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(10, 1, 'DB2 PureScale CF Primary', 'db2cfp.dephub.go.id', '192.168.50.73', '1', '8 GB', '100 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(11, 2, 'BPM Process Server Prod 2', 'bpmpsprod02.dephub.go.id', '192.168.50.189', '4', '16 GB', '200 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(12, 2, 'BPM Process Server UAT', 'bpmpsuat.dephub.go.id', '192.168.50.194', '2', '8 GB', '100 GB', 'SUSE 12', 'IBM BPM Process Server', 'SIMLALA BPM Process Server', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(13, 2, 'DB2 PureScale 02', 'db2ps02.dephub.go.id', '192.168.50.193', '2', '32 GB', '300 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-15 06:51:10', '2021-12-15 06:51:10'),
	(14, 2, 'DB2 PureScale CF Secondary', 'db2cfs.dephub.go.id', '192.168.50.74', '1', '8 GB', '100 GB + 2TB Share', 'RHEL 7', 'IBM DB2 AWSE 11.1', 'SIMLALA Database', '2021-12-15 06:51:10', '2021-12-15 06:51:10');
/*!40000 ALTER TABLE `power_vm_list` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.sessions: ~1 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
	('eUIU057cBWwy5JfuUSFcgFPCEMMmKLmMR0XJPewH', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMUQxaVI4ZVd6dnBzUlVVNW1ucU9xNFFNbklkZU5xMWMySTdzMXFlTSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTc6Imh0dHA6Ly9pbnZlbnRhcmlzaW5mcmFkaXRsYWxhLmxvY2FsOjgwODgvbWFzdGVyL2Rhc2hib2FyZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1640339421);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.status_perangkat
CREATE TABLE IF NOT EXISTS `status_perangkat` (
  `id_status` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `desc_status` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table db_inventaris.status_perangkat: ~5 rows (approximately)
/*!40000 ALTER TABLE `status_perangkat` DISABLE KEYS */;
INSERT INTO `status_perangkat` (`id_status`, `desc_status`, `created_at`, `updated_at`) VALUES
	(1, 'Operasional', '2022-01-13', '2022-01-13'),
	(2, 'Standby', '2022-01-13', '2022-01-13'),
	(3, 'Retired', '2022-01-13', '2022-01-13'),
	(4, 'Rusak', '2022-01-13', '2022-01-13'),
	(5, 'Hilang', '2022-01-13', '2022-01-13');
/*!40000 ALTER TABLE `status_perangkat` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.t_barangs
CREATE TABLE IF NOT EXISTS `t_barangs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isHypervisor` tinyint(1) NOT NULL DEFAULT '0',
  `jenis_barang_id` bigint(20) unsigned NOT NULL,
  `merk_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tipe` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mac_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cluster_id` bigint(20) unsigned DEFAULT NULL,
  `status_id` smallint(5) unsigned NOT NULL,
  `lokasis_id` smallint(5) unsigned NOT NULL,
  `rack_position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengadaans_id` bigint(20) unsigned DEFAULT NULL,
  `rack_unit` smallint(5) unsigned DEFAULT '0',
  `garansi_awal` date DEFAULT NULL,
  `garansi_akhir` date DEFAULT NULL,
  `ip_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_barangs_jenis_barang_id_foreign` (`jenis_barang_id`),
  KEY `t_barangs_merk_id_foreign` (`merk_id`),
  KEY `t_barangs_lokasis_id_foreign` (`lokasis_id`),
  KEY `t_barangs_status_perangkat_id_foreign` (`status_id`),
  KEY `t_barangs_pengadaan_id_foreign` (`pengadaans_id`) USING BTREE,
  CONSTRAINT `t_barangs_jenis_barang_id_foreign` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_barangs_lokasis_id_foreign` FOREIGN KEY (`lokasis_id`) REFERENCES `lokasis` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_barangs_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_barangs_pengadaans_id_foreign` FOREIGN KEY (`pengadaans_id`) REFERENCES `pengadaans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_barangs_status_perangkat_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status_perangkat` (`id_status`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.t_barangs: ~27 rows (approximately)
/*!40000 ALTER TABLE `t_barangs` DISABLE KEYS */;
INSERT INTO `t_barangs` (`id`, `nama_barang`, `isHypervisor`, `jenis_barang_id`, `merk_id`, `tipe`, `serial_number`, `mac_address`, `cluster_id`, `status_id`, `lokasis_id`, `rack_position`, `pengadaans_id`, `rack_unit`, `garansi_awal`, `garansi_akhir`, `ip_address`, `created_at`, `updated_at`) VALUES
	(86, 'Server ex Inaportnet JTN', 1, 1, 5, 'Quanta DB51-2U', 'QTFCJ06340039', NULL, 6, 2, 1, 'Row 3 Rack 10B', 5, NULL, NULL, NULL, '192.168.72.218', '2022-01-31 12:38:11', '2022-06-07 18:21:26'),
	(87, 'Load Balancer (Primary)', 0, 5, 1, 'Big IP F5 i5600 Series', 'f5-pcua-pjhm', NULL, 8, 1, 1, 'Row 1 Rack 9', 8, 23, NULL, NULL, '10.252.242.161;192.168.4.100', '2022-05-31 18:47:13', '2022-06-07 13:48:55'),
	(88, 'Load Balancer (Secondary)', 0, 5, 1, 'Big IP F5 i5600 Series', 'f5-ugjw-taqf', NULL, 8, 1, 1, 'Row 1 Rack 9', 8, 22, NULL, NULL, '10.252.242.162;192.168.4.102', '2022-05-31 19:23:04', '2022-05-31 19:23:04'),
	(91, 'Server BPM 1', 0, 1, 2, 'PowerLinux 8247 22L', '687AD8A', NULL, 4, 1, 1, 'Row 1 Rack 9', 11, 18, '2017-08-02', '2021-08-02', '192.168.50.192', '2022-06-03 13:35:27', '2022-06-08 16:36:42'),
	(92, 'Server BPM 2', 0, 1, 2, 'PowerLinux 8247 22L', '687AD7A', NULL, 4, 1, 1, 'Row 1 Rack 9', 11, 16, '2017-08-03', '2021-08-02', '192.168.50.193', '2022-06-03 13:47:52', '2022-06-08 16:36:55'),
	(93, 'Server Primary DC DRC 1', 0, 1, 3, 'ThinkSystem SR530', 'J300ACW6', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 33, '2018-08-03', '2021-08-02', '10.252.242.150', '2022-06-07 08:09:54', '2022-06-08 16:37:11'),
	(94, 'Server Primary DC DRC 2', 0, 1, 3, 'ThinkSystem SR530', 'J300ACW5', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 32, '2018-08-03', '2021-08-02', '10.252.242.151', '2022-06-07 08:43:49', '2022-06-08 16:37:41'),
	(95, 'Server Primary DC DRC 3', 0, 1, 3, 'ThinkSystem SR530', 'J300ACW4', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 31, '2018-08-03', '2021-08-02', '10.252.242.152', '2022-06-07 09:36:19', '2022-06-08 16:37:32'),
	(96, 'Storage Lenovo 1', 0, 4, 3, 'ThinkSystem DS4200', 'J34529E', NULL, 9, 1, 1, 'Row 1 Rack 9', 11, 26, '2017-08-03', '2021-08-02', '10.252.242.154', '2022-06-08 12:10:48', '2022-06-08 16:38:13'),
	(97, 'Storage Lenovo 2', 0, 4, 3, 'ThinkSystem DS4200', 'J34529F', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 24, '2017-08-03', '2021-08-02', '10.252.242.155', '2022-06-08 12:28:44', '2022-06-08 12:28:44'),
	(98, 'SAN Switch SIMLALA 1', 0, 10, 3, 'B300 SAN Switch', NULL, NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 15, '2018-08-03', '2021-08-02', NULL, '2022-06-08 12:33:29', '2022-06-08 12:33:29'),
	(99, 'SAN Switch SIMLALA 2', 0, 10, 3, 'B300 SAN Switch', NULL, NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 14, '2018-08-03', '2021-08-02', NULL, '2022-06-08 12:35:30', '2022-06-08 12:35:30'),
	(100, 'Network Switch SIMLALA', 0, 3, 3, 'Rack Switch NE1032T', NULL, NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 36, '2018-08-03', '2021-08-02', NULL, '2022-06-08 12:41:20', '2022-06-08 12:41:20'),
	(101, 'Server Backup SIMLALA', 0, 1, 3, 'ThinkSystem SR530', 'J300ACW7', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 30, '2018-08-03', '2021-08-02', '10.252.242.153', '2022-06-08 12:51:15', '2022-06-08 13:10:35'),
	(102, 'Tape Backup SIMLALA', 0, 6, 2, 'System Storage TS3100 Tape Library Model L2U', '78CF541', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 28, '2018-08-03', '2021-08-02', NULL, '2022-06-08 12:58:29', '2022-06-08 12:58:29'),
	(103, 'HMC', 0, 1, 3, 'System X3550 m5', '687AADD', NULL, 5, 1, 1, 'Row 1 Rack 9', 11, 20, '2018-08-03', '2021-08-02', NULL, '2022-06-08 13:02:00', '2022-06-08 13:02:00'),
	(104, 'Server Inaportnet Node 1', 0, 1, 3, 'ThinkAgile HX7520', 'J3024T5Y', NULL, 6, 1, 1, 'Row 3 Rack 5', 10, 6, '2020-07-31', '2023-08-01', '10.252.242.177;192.168.58.10;192.168.58.14', '2022-06-08 17:25:50', '2022-06-08 17:25:50'),
	(105, 'Server Inaportnet Node 2', 0, 1, 3, 'ThinkAgile HX7520', 'J3024T5Z', NULL, 6, 1, 1, 'Row 3 Rack 10B', 10, 4, '2020-07-31', '2023-08-01', '10.252.242.178;192.168.58.11;192.168.58.15', '2022-06-08 17:27:53', '2022-06-08 17:27:53'),
	(106, 'Server Inaportnet Node 3', 0, 1, 3, 'ThinkAgile HX7520', 'J3024T60', NULL, 6, 1, 1, 'Row 3 Rack 10B', 10, 2, '2020-07-31', '2023-08-01', '10.252.242.179;192.168.58.12;192.168.58.16', '2022-06-08 17:51:26', '2022-06-08 17:51:26'),
	(107, 'Switch Inaportnet', 0, 3, 3, 'ThinkSystem NE1032 Rack Swich', 'MM58722', NULL, 6, 1, 1, 'Row 3 Rack 5', 10, 41, '2018-07-31', '2021-08-01', '10.252.242.180', '2022-06-08 18:42:16', '2022-06-08 18:42:16'),
	(108, 'IBM IIAS', 0, 1, 2, 'Appliances', NULL, NULL, 7, 1, 1, 'Row 2 Rack 12', 12, NULL, NULL, NULL, '10.252.242.190 /24;10.252.242.191 / 24;10.252.242.192/ 24;10.252.242.193 / 24', '2022-06-08 18:45:49', '2022-06-08 18:45:49'),
	(109, 'Server ETL', 0, 1, 3, 'ThinkSystem SR650', NULL, NULL, 7, 1, 1, 'Row 1 Rack 10', 12, NULL, NULL, NULL, '192.168.50.196', '2022-06-08 18:49:37', '2022-06-08 18:49:37'),
	(110, 'Server Cognos', 0, 1, 3, 'ThinkSystem SR650', NULL, NULL, 7, 1, 1, 'Row 1 Rack 10', 12, NULL, NULL, NULL, '192.168.50.197', '2022-06-08 18:54:58', '2022-06-08 18:54:58'),
	(111, 'Storage DWH', 0, 4, 7, 'ES-1640DCv2', NULL, NULL, 7, 1, 1, 'Row 3 Rack 6', 5, NULL, NULL, NULL, '192.168.58.121;192.168.58.122', '2022-06-08 19:04:05', '2022-06-08 19:04:05'),
	(112, 'vSphere 6 Standard 6 Socket License', 0, 11, 11, 'vSphere 6 Standard', NULL, NULL, 6, 1, 1, NULL, 10, NULL, NULL, NULL, NULL, '2022-06-08 19:21:30', '2022-06-08 19:21:30'),
	(113, 'vCenter Srv 6', 0, 11, 11, 'vCenter Srv 6 Fdn for vSph 6', NULL, NULL, 6, 1, 1, NULL, 10, NULL, NULL, NULL, NULL, '2022-06-08 19:28:33', '2022-06-08 19:28:33'),
	(114, 'Starter License', 0, 11, 12, 'Nutanix Starter License', NULL, NULL, 6, 1, 1, NULL, 10, NULL, NULL, NULL, NULL, '2022-06-08 19:33:35', '2022-06-08 19:33:35'),
	(115, 'Starter 3Yr Maintenance and Support', 0, 11, 12, 'Starter 3Yr Maintenance and Support', NULL, NULL, 6, 1, 1, NULL, 10, NULL, NULL, NULL, NULL, '2022-06-08 19:39:50', '2022-06-08 19:39:50'),
	(116, 'vSphere 6 Standard 6 Socket License', 0, 11, 11, 'vSphere 6 Standard 6 Socket License', NULL, NULL, 5, 1, 1, NULL, 11, NULL, NULL, NULL, NULL, '2022-06-08 20:04:51', '2022-06-08 20:04:51'),
	(117, 'vCenter Srv 6', 0, 11, 11, 'vCenter Srv 6 Fdn for vSph 6', NULL, NULL, 5, 1, 1, NULL, 11, NULL, NULL, NULL, NULL, '2022-06-08 20:08:44', '2022-06-08 20:08:44'),
	(118, 'Windows Server 2016', 0, 11, 13, 'Standard', NULL, NULL, 5, 1, 1, NULL, 11, NULL, NULL, NULL, NULL, '2022-06-08 20:14:12', '2022-06-08 20:14:12');
/*!40000 ALTER TABLE `t_barangs` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.virtual_machines
CREATE TABLE IF NOT EXISTS `virtual_machines` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vCPU` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vRAM` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `vDISK` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `OS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `component` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `serive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `power` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `meta` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `virtual_machines_node_id_foreign` (`node_id`),
  CONSTRAINT `virtual_machines_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.virtual_machines: ~0 rows (approximately)
/*!40000 ALTER TABLE `virtual_machines` DISABLE KEYS */;
/*!40000 ALTER TABLE `virtual_machines` ENABLE KEYS */;

-- Dumping structure for table db_inventaris.vm_ware
CREATE TABLE IF NOT EXISTS `vm_ware` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_os` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IP_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memory_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provisioned_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_space` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_inventaris.vm_ware: ~44 rows (approximately)
/*!40000 ALTER TABLE `vm_ware` DISABLE KEYS */;
INSERT INTO `vm_ware` (`id`, `hostname`, `guest_os`, `state`, `IP_address`, `memory_size`, `cpus`, `provisioned_space`, `used_space`, `created_at`, `updated_at`) VALUES
	(1, 'app-02', 'Microsoft Windows Server 2016 or later (64-bit)', 'Powered Off', '192.168.58.102', '8 GB', '4', '108.25 GB', '8.35 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(2, 'App-1', 'Microsoft Windows Server 2016 or later (64-bit)', 'Powered Off', 'NULL', '8 GB', '2', '108.24 GB', '8.14 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(3, 'DB PPKA-Hibah-Simlala', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.30', '16 GB', '8', '500 GB', '41 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(4, 'Formgen PPKA-SIJUKA', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.19', '16 GB', '8', '500 GB', '46.78 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(5, 'IPNDCTAPP01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.20', '16 GB', '8', '60 GB', '29.88 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(6, 'IPNDCTAPP02', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.21', '16 GB', '8', '60 GB', '24.55 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(7, 'IPNDCTAPP03', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.22', '16 GB', '8', '60 GB', '24.3 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(8, 'IPNDCTAPP04', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.23', '16 GB', '8', '60 GB', '24.99 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(9, 'IPNDCTAPP05', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.24', '16 GB', '8', '60 GB', '25.31 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(10, 'IPNDCTAPP06', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.25', '16 GB', '8', '60 GB', '24.9 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(11, 'IPNDCTAPP07', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.26', '16 GB', '8', '60 GB', '25.07 GB', '2021-12-15 06:28:20', '2021-12-15 06:28:20'),
	(12, 'IPNDCTAPP08', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.27', '16 GB', '8', '60 GB', '24.49 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(13, 'IPNDCTAPP09', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.28', '16 GB', '8', '60 GB', '24.14 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(14, 'IPNDCTAPP10', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.29', '16 GB', '8', '60 GB', '24.53 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(15, 'IPNDCTAPP11', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.70', '16 GB', '8', '60 GB', '24.19 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(16, 'IPNDCTAPP12', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.71', '16 GB', '8', '60 GB', '25.02 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(17, 'IPNDCTAPP13', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.72', '16 GB', '8', '60 GB', '25.63 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(18, 'IPNDCTAPP14', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.73', '16 GB', '8', '60 GB', '24.19 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(19, 'IPNDCTAPP15', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.74', '16 GB', '8', '60 GB', '24.44 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(20, 'IPNDCTAPP16', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.75', '16 GB', '8', '60 GB', '24.14 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(21, 'IPNDCTAPP17', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.76', '16 GB', '8', '60 GB', '24.01 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(22, 'IPNDCTAPP18', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.77', '16 GB', '8', '60 GB', '24.56 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(23, 'IPNDCTAUTOMATION', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.53', '8 GB', '2', '64.85 GB', '26.46 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(24, 'IPNDCTDB01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.40', '16 GB', '4', '260 GB', '174.1 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(25, 'IPNDCTDB02', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.41', '16 GB', '4', '260 GB', '171.56 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(26, 'IPNDCTDB03', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.42', '16 GB', '4', '260 GB', '172.17 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(27, 'IPNDCTDB04', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.43', '16 GB', '4', '260 GB', '204.96 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(28, 'IPNDCTDB05', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.44', '16 GB', '4', '260 GB', '173.44 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(29, 'IPNDCTDB06', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.45', '16 GB', '4', '260 GB', '177.77 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(30, 'IPNDCTLBAPP', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.58', '8 GB', '2', '260 GB', '48.61 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(31, 'IPNDCTLBDB', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.57', '8 GB', '2', '263.3 GB', '251.7 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(32, 'IPNDCTMONITORING', 'CentOS 8 (64-bit)', 'Powered Off', '192.168.58.54', '8 GB', '2', '68.21 GB', '4.91 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(33, 'IPNDCTNFS01', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.50', '16 GB', '4', '7.06 TB', '5.78 TB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(34, 'NTNX-J3024T5Y-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.16', '32 GB', '12', '102.95 MB', '102.95 MB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(35, 'NTNX-J3024T5Z-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.14', '32 GB', '12', '103.01 MB', '103.01 MB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(36, 'NTNX-J3024T60-A-CVM', 'CentOS 4/5 or later (64-bit)', 'Powered On', '192.168.58.15', '32 GB', '12', '102.69 MB', '102.69 MB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(37, 'Nutanix VCenter', 'Other 3.x Linux (64-bit)', 'Powered On', '192.168.58.17', '16 GB', '4', '477 GB', '118.67 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(38, 'PPKA-SIJUKA', 'Ubuntu Linux (64-bit)', 'Powered On', '192.168.58.18', '16 GB', '8', '1 TB', '44.48 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(39, 'Scheduler Server-02_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered On', '192.168.50.179', '16 GB', '8', '300 GB', '10.87 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(40, 'Simlala WebApps-01_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered Off', 'NULL', '16 GB', '4', '316.23 GB', '69.59 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(41, 'Simlala WebApps-04_replica', 'Red Hat Enterprise Linux 7 (64-bit)', 'Powered Off', '192.168.50.195', '16 GB', '4', '316.21 GB', '86.63 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(42, 'SIMLALA-NFS-BACKUP', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.103', '8 GB', '4', '2.45 TB', '1.58 TB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(43, 'SuperSet', 'CentOS 8 (64-bit)', 'Powered On', '192.168.58.120', '16 GB', '8', '60 GB', '30.32 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21'),
	(44, 'Veeam', 'Microsoft Windows Server 2012 (64-bit)', 'Powered On', '192.168.58.100', '16 GB', '2', '100.02 GB', '100.02 GB', '2021-12-15 06:28:21', '2021-12-15 06:28:21');
/*!40000 ALTER TABLE `vm_ware` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
