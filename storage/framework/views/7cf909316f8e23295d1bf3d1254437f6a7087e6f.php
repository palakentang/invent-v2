<?php echo $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'; ?>

* : wajib isi   
<div class="row">
    <div class="col-sm">
        <div class="form-group">
            <label for="cluster_id">Cluster</label>
            <select name="cluster_id" id="" class="form-control" required>
                <option value="">Pilih Cluster</option>
                <?php $__empty_1 = true; $__currentLoopData = \App\Models\Cluster::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->nama_cluster); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            <label for="hostname">Nama Host<sup>*</sup></label>
            <?php echo Form::text('hostname', old('hostname'), ['class' => 'form-control', 'required']); ?>

        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            <label for="name">VM Name<sup>*</sup></label>
            <?php echo Form::text('name', old('name'), ['class' => 'form-control', 'required']); ?>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-3">
        <div class="form-group">
            <label for="OS">OS</label>
            <select name="OS" id="" class="form-control" required>
                <option value="">Pilih OS</option>
                <?php $__empty_1 = true; $__currentLoopData = \App\Models\OS::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->nama_os); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </select>
        </div>
    </div>

    <div class="col-2">
        <div class="form-group">
            <label for="vCPU">vCPU</label>
            <?php echo Form::text('vCPU', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-2">
        <div class="form-group">
            <label for="vRAM">vRAM</label>
            <?php echo Form::text('vRAM', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-2">
        <div class="form-group">
            <label for="vDISK">vDISK</label>
            <?php echo Form::text('vDISK', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            <label for="used_space">Used Space</label>
            <?php echo Form::text('used_space', '', ['class' => 'form-control']); ?>

            titik (.) untuk pemisah desimal
        </div>
    </div>
</div>

<div class="form-group" id="ip_address_row">
    <label for="ip_address">IP Address</label>
    <textarea name="ip_address" id="ip_address_edit" cols="30" rows="1" style="resize:none;" class="form-control"></textarea>
    Isikan seluruh IP Address, dengan pemisah ; (titik koma), dan tambahkan ", mgmt" jika IP Mgmt, cth : 192.168.1.1, mgmt; 192.168.1.2; dst...
</div>
<div id="add-row-address">
    
</div>

<div class="row">
    <div class="col-6">
        <label for="Power">State</label>
        <div class="row">
            <div class="col-6">
                <div class="form-check">
                    <input name="power" value="on" type="checkbox" class="form-check-input" id="<?php echo e($data == null ? 'ON' : 'edit_ON'); ?>">
                    <label class="form-check-label" for="ON">Power ON</label>
                </div>
            </div>
            <div class="col-6">
                <div class="form-check">
                    <input name="power" value="off" type="checkbox" class="form-check-input" id="<?php echo e($data == null ? 'ON' : 'edit_OFF'); ?>">
                    <label class="form-check-label" for="OFf">Power OFF</label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inv\invent-v2\resources\views/copmonents/form/vm.blade.php ENDPATH**/ ?>