<script>
    var table;
    $(document).ready(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'auto',
            showDropdowns: true,

        }, function(start, end, label) {
            $('input[name=warranty_start]').val(start.format('YYYY-MM-DD'))
            $('input[name=warranty_end]').val(end.format('YYYY-MM-DD'))
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });

        $('input[name="support"]').daterangepicker({
            opens: 'auto',
            showDropdowns: true,

        }, function(start, end, label) {
            $('input[name=support_start]').val(start.format('YYYY-MM-DD'))
            $('input[name=support_end]').val(end.format('YYYY-MM-DD'))
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });

		$(function() {
		  $('input[name="GaransiMulai"]').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			autoApply: true,
            autoUpdateInput: true,
			minYear: 2010,
			maxYear: 2050,
			locale: { 
			format: 'DD-MM-YYYY'
			}
		  }, function(start, end, label) {
		  });
		});

		$(function() {
		  $('input[name="GaransiAkhir"]').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			autoApply: true,
            autoUpdateInput: true,
			minYear: 2010,
			maxYear: 2050,
			locale: { 
			format: 'DD-MM-YYYY'
			}			
		  }, function(start, end, label) {
		  });
		});

        $("#tambahIP").click(function (e) {
            e.preventDefault();
            $('#new').append($('#ipAddress').html())
        });

        $("#tambahIPEdit").click(function (e) {
            e.preventDefault();
            $('#new-ip-address').append(`<div id="ipAddress"><label for="ip_address">IP Adrress</label><?php echo Form::text('ip_address[]', old('ip_address[]'), ['class' => 'form-control']); ?></div>`)
        });

    } );

    function IPMgmt(params) {
        $('#modal-mgmt').modal();
        $('#ol-ip').remove();
        var html = '';
            html += '<ul id="ol-ip">'
                params.forEach(element => {
                    var checked =  element.isMgmt == 1 ? 'checked' : '';
                    html += '<li>'
                        html += '<div class="form-check">'
                            html += '<input type="checkbox" id="ip_address_checked" onclick="isMgmt(`'+element.ip_address+'`)" name="ip_address[]" '+checked+' class="form-check-input" value="'+element.ip_address+'" id="'+element.id+'">'
                            html += '<label class="form-check-label" for="'+element.id+'">'+element.ip_address+'</label>'
                        html +='</div>'
                    html +='</li>'
                });
            html += '</ul>'
        $('.set-it-mgmt').append(html);
    }

    function isMgmt(params) {
        $('#ip_address_checked').attr('disabled', true);
        $('.set-it-mgmt').hide();
        $('.spinner-grow').show();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('ajax.setMgmt')); ?>",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'IP_address': params,
                '_token': '<?php echo e(csrf_token()); ?>'
            },
            dataType: "json",
            success: function (response) {
                $('#ip_address_checked').removeAttr('disabled');
                if (response.success) {
                    $('.set-it-mgmt').show();
                    $('.spinner-grow').hide();
                    return alert(response.message)
                } return alert('Gagal')
            }
        });
    }

    function show(response) {
        console.log(response)
        $("#modal-show").modal()
        $('#merk_show').html(response.merks.nama_merk);
        $('#header_nama_barang').html(response.nama_barang);
        $('#jenis_barang_show').html(response.jenis_barang.nama_jenis_barang);
        $('#nama_barang_show').html(response.nama_barang);
        $('#serial_number_show').html(response.serial_number);
        $('#mac_address_show').html(response.mac_address);
        $('#nama_pengadaan_show').html(response.pengadaan.nama_pengadaan);

        response.i_p_addresses.forEach(element => {
            var html = `<li>`+element.ip_address+`</li>`;
            $('#ip_show').append(html)
        });

        $('td#daterange_show').html('asd');
        // $('#pengadaan_id_show').html(response.pengadaan.nama_pengadaan);

        $('#row_show').html(JSON.parse(response.position).row);
        $('#rack_show').html(JSON.parse(response.position).rack);
        $('#unit_show').html(JSON.parse(response.position).unit);
    }

    function edit(response) {
        console.log(response);
        $('#IPAddresses').val(response.ip_address);
        // $('#pengadaans_idEdit').val(response.pengadaans_id).change();
        // $('#clusterEdit').val(response.cluster_id)
        let aktion = "<?php echo e(route('master.barang.update', 'asd')); ?>";
        let action_nih = aktion.replace('asd', response.id);
        // alert(action_nih);
        $("#modal-edit").modal();
        $('input[name=id]').val('');
        $('input#nama_barang').val('');
        $('select[name=jenis_barang_id]').val('');
		$('select[name=merk_id]').val('');
        $('select[name=cluster_id]').val('');
        $('select[name=pengadaans_id]').val('');
		$('input#tipe').val('');
        $('input[name=serial_number]').val('');
		$('input[name=mac_address]').val('');
		$('select[name=status_id]').val('');
        $('select[name=lokasis_id]').val('');
		$('input[name=PosisiRack]').val('');
		$('input[name=UnitRack]').val('');

        $('select[name=pengadaans_id]').val(response.pengadaans_id);		
        $('input#nama_barang').val(response.nama_barang);
        $('select[name=jenis_barang_id]').val(response.jenis_barang_id);
        $('select[name=cluster_id]').val(response.cluster_id);
		$('select[name=merk_id]').val(response.merk_id);
        $('input#id').val(response.id);
		$('input#tipe').val(response.tipe);
        $('input[name=serial_number]').val(response.serial_number);
		$('input[name=mac_address]').val(response.mac_address);
		$('select[name=status_id]').val(response.status_id);
        $('select[name=lokasis_id]').val(response.lokasis_id);
		$('input[name=PosisiRack]').val(response.rack_position);
		$('input[name=UnitRack]').val(response.rack_unit);
        if(!!response.garansi_awal) {
            var dd = response.garansi_awal.substring(8,10);
            var mm = response.garansi_awal.substring(5,7);
            var yyyy = response.garansi_awal.substring(0,4);
            var cGaransiAwal = dd + '-' + mm + '-' + yyyy;
            console.log('cGaransiMulai :'+cGaransiAwal);
            $('input[name=GaransiMulai]').val(cGaransiAwal);
        } else {
            $('input[name=GaransiMulai]').val('');
        }

        if(!!response.garansi_akhir) {
            var dd1 = response.garansi_akhir.substring(8,10);
            var mm1 = response.garansi_akhir.substring(5,7);
            var yyyy1 = response.garansi_akhir.substring(0,4);
            var cGaransiAkhir = dd1 + '-' + mm1 + '-' + yyyy1;
            $('input[name=GaransiAkhir]').val(cGaransiAkhir);
        } else {
            $('input[name=GaransiAkhir]').val('');
        }    				
            // $('#GaransiMulai').val(cGaransiAwal);
            // $('input#IPAddresses').val(response.IPAddresses);
        $('select[name=pengadaans_id]').val(response.pengadaans_id);
        $('#header_nama_barang').html(response.nama_barang);
        // $('#edit').attr('action', "<?php echo e(route('master.barang.update', 'asd')); ?>".replace('asd', ''+val(response.id)+''));
        $('#edit').attr('action', action_nih );
        console.log('===>'+$('#edit').action);
        // $('#daterange_show').html((response.garansi_awal == null ? '' : response.garansi_awal)+' - '+(response.garansi_akhir == null ? '' : response.garansi_akhir));
        // // $('#pengadaan_id_show').html(response.pengadaan.nama_pengadaan)
        
    }
</script>
<?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/copmonents/script/barang.blade.php ENDPATH**/ ?>