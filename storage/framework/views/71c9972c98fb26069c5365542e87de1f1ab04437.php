<?php $__env->startSection('css-third'); ?>
<style>
</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>

                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6"><h5 class="m-0">List <?php echo e($pageTitle); ?></h5></div>
                                <?php if(\Request::segment(1) == 'master'): ?>
                                    <div class="col-6"><button class="btn btn-success float-right" onclick="tambah()">Tambah Cluster</button></div>
                                <?php endif; ?>
                            </div>
                            
                            
                        </div>
                        <div class="card-body">
                            <?php $__env->startComponent('layouts.partials.alert'); ?><?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            <?php $__env->startComponent('copmonents.tables.cluster', [
                                'thead' => ['Nama Cluster', 'Cluster System', 'Dibuat', 'Diubah'],
                                'data' => $data,
                                'route' => $route
                            ]); ?>

                            <?php if (isset($__componentOriginal810fff804f7569aaf7a2f74e0cf4ee5216f0f73e)): ?>
<?php $component = $__componentOriginal810fff804f7569aaf7a2f74e0cf4ee5216f0f73e; ?>
<?php unset($__componentOriginal810fff804f7569aaf7a2f74e0cf4ee5216f0f73e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <?php $__env->startComponent('copmonents.modal.global.show', [
                'field' => [
                    'nama_cluster',
                    'cluster_system_id',
                ],
            ]); ?>

            <?php if (isset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c)): ?>
<?php $component = $__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c; ?>
<?php unset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah Cluster',
                'route' => route($route['store']),
                'form' => 'copmonents.form.cluster',
                'select' => \App\Models\ClusterSystem::get(),
            ]); ?>

            <?php if (isset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759)): ?>
<?php $component = $__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759; ?>
<?php unset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.edit', [
                'pageTitle' => 'Cluster',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.cluster',
                'select' => null
            ]); ?>

            <?php if (isset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb)): ?>
<?php $component = $__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb; ?>
<?php unset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js-third'); ?>
    <script>

        function tambah() {
            $('#modal-tambah').modal()
            
        }

        function ubah(params) {
            $('#modal-edit').modal()
            $("form#update").attr('action', "<?php echo e(url('master/cluster/')); ?>/"+params.id+"");
            $('input[name=nama_cluster]').val(params.nama_cluster)
            $('select[name=cluster_system_id]').val(params.cluster_system_id)
        }

        function lihat(params) {
            $('#modal-show').modal();
            console.log(params.cluster);

            $('td#name').html(params.name)
            $('td#cluster_system_id').html(params.cluster_system.name)
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laravel\inventaris\resources\views/pages/virtual-machine/cluster/index.blade.php ENDPATH**/ ?>