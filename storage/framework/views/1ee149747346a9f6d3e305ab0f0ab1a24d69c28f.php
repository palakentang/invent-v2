<div class="modal fade" id="modal-tambah-ip" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Barang</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo e(route('master.barang.tambah.ipAddress')); ?>" method="post" id="store-ip-address">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" id="barang_id" name="barang_id" />
                    <div class="form-group">
                        <label>IP Address</label>
                        <input type="text" class="form-control" name="ip_address" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('store-ip-address').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inv\invent-v2\resources\views/copmonents/modal/barang/add-ip.blade.php ENDPATH**/ ?>