<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            <?php $__currentLoopData = $thead; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <th><?php echo e($item); ?></th>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr style="text-align: left">
            <td><?php echo e($item->nama_lokasi); ?> (<?php echo e($item->detail_lokasi); ?>)</td>
            <td><?php echo e(date('d F Y', strtotime($item->created_at))); ?></td>
            <td><?php echo e(date('d F Y', strtotime($item->updated_at))); ?></td>
            <td width="40" style="white-space:nowrap; align=right;">
                <div class="row">
                    <div class="col-6">                   
                        <form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post">
						<a title="edit" class="btn btn-success btn-sm" href="<?php echo e(route($route['edit'], $item->id)); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
						<button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                            <?php echo method_field('delete'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table><?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/tables/lokasi.blade.php ENDPATH**/ ?>