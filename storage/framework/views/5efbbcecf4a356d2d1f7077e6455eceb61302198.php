<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            <?php $__currentLoopData = $thead; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <th><?php echo e($item); ?></th>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if(\Request::segment(1) == 'master'): ?>

            <th>Aksi</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <td><?php echo e($item->name); ?></td>
            <td><?php echo e($item->description); ?></td>
            <td><?php echo e(date('d F Y', strtotime($item->created_at))); ?></td>
            <td><?php echo e(date('d F Y', strtotime($item->updated_at))); ?></td>
            <?php if(\Request::segment(1) == 'master'): ?>

            <td class="align-right">
                <div class="">
                    <div class="">
                        <a href="#" class="btn btn-success btn-sm" onclick="ubah(<?php echo e($item); ?>)"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post" id="hapus<?php echo e($item->id); ?>" style="display: none">
                            <?php echo method_field('DELETE'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                        <a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus<?php echo e($item->id); ?>').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
            </td>

            <?php endif; ?>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/tables/cluster-system.blade.php ENDPATH**/ ?>