<div class="modal fade" id="modal-tambah" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo e($pageTitle); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo e($route); ?>" method="post" id="store">
                    <?php echo csrf_field(); ?>
                    <?php $__env->startComponent($form, [
                        'data' => null,
                        'select' => $select
                    ]); ?>

                    <?php if (isset($__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96)): ?>
<?php $component = $__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96; ?>
<?php unset($__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('store').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inv\invent-v2\resources\views/copmonents/modal/global/add.blade.php ENDPATH**/ ?>