<?php $__env->startSection('css-third'); ?>
<script>
</script>
<style>
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>

                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
							    <div class="col-md-12 bg-light text-right">
                                    <?php if(\Request::segment(1) == 'master'): ?>
                                        <button onclick="tambah()" class="btn btn-success float-right"><i class="fas fa-plus">&nbsp;</i>Tambah Virtual Machine</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        
                
                            &nbsp;
                            <?php $__env->startComponent('layouts.partials.alert'); ?><?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            <?php $__env->startComponent('copmonents.tables.vm', [
                                'thead' => [
                                    'Aksi',
                                    'Cluster',
                                    'Host',
                                    'Nama VM',
                                    'State',
                                    'IP Address',
                                    'vMemory',
                                    'vCPU',
                                    'vDisk',
                                    'Description',
                                    'OS', 
                                    'IP Address',
                                    'Used Space',
                                    'ON / OFF',
                                    'Condition',
                                ],
                                'data' => $data,
                                'route' => $route
                            ]); ?>

                            <?php if (isset($__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3)): ?>
<?php $component = $__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3; ?>
<?php unset($__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <?php $__env->startComponent('copmonents.modal.global.show', [
                'field' => [
                    'name',
                    'cluster_id',
                    'description',
                    'hostname',
                    'ip_address_show',
                    'vCPU',
                    'vRAM',
                    'vDISK',
                    'OS',
                    'used_space',
                    'power',
                    'condition',
                ],
            ]); ?>

            <?php if (isset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c)): ?>
<?php $component = $__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c; ?>
<?php unset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah VM',
                'route' => route($route['store']),
                'form' => 'copmonents.form.vm',
                'select' => \App\Models\Node::get(),
            ]); ?>

            <?php if (isset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759)): ?>
<?php $component = $__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759; ?>
<?php unset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.edit', [
                'pageTitle' => 'VM',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.vm',
                'select' => null,
            ]); ?>

            <?php if (isset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb)): ?>
<?php $component = $__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb; ?>
<?php unset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js-third'); ?>
    <script>
        function tambah() {
            $('#modal-tambah').modal()
            
        }

        function ubah(params) {
            $('#modal-edit').modal()
            $("form#update").attr('action', "<?php echo e(url('master/virtual-machine/vm/')); ?>/"+params.id+"");
            console.log(params);
            $('input[name=name]').val(params.name)
            $('select[name=cluster_id]').val(params.cluster_id)
            $('input[name=name]').val(params.name)
            $('textarea[name=description]').val(params.description)
            $('input[name=hostname]').val(params.hostname)
            $('textarea[id=ip_address_edit]').val(params.ip_address)
            $('input[name=vCPU]').val(params.vCPU)
            $('input[name=vRAM]').val(params.vRAM)
            $('input[name=vDISK]').val(params.vDISK)
            $('select[name=OS]').val(params.OS)
            $('input[name=component]').val(params.component)
            $('input[name=serive]').val(params.serive)
            $('input[name=used_space]').val(params.used_space)
            console.log(params.power.toUpperCase());
            $("#edit_"+params.power.toUpperCase()).attr("checked", true);

        }

        function show(params, cluster) {
            $('#modal-show').modal();
            console.log(params.ip_address.split(';'));

            $('td#name').html(params.name)
            $('td#cluster_id').html(cluster.nama_cluster)
            $('td#description').html(params.description)
            $('td#hostname').html(params.hostname)
            
            var html = '<ul style="list-style: none;padding: 0;">'
                $.each(params.ip_address.split(';'), function (indexInArray, valueOfElement) { 
                    console.log(valueOfElement);
                    html +='<li>'+valueOfElement+'</li>'
                });
            html += '<ul>'
            $('td#ip_address_show').html(html)
            $('td#vCPU').html(params.vCPU)
            $('td#vRAM').html(params.vRAM)
            $('td#vDISK').html(params.vDISK)
            $('td#OS').html(params.OS.nama_os)
            $('td#component').html(params.component)
            $('td#serive').html(params.serive)
            $('td#used_space').html(params.used_space)
            var power = params.power == 'on' ? 'success' : 'danger';
            var htmlPower = '<span class="right badge badge-'+power+'">'+params.power.toUpperCase()+'</span>'
            $('td#power').html(htmlPower)
            var persentage = ((params.vDISK - params.used_space) / params.vDISK) * 100;
            switch (true) {
                case (persentage > 50):
                    var condition = 'success';
                    break;
                case (persentage < 50):
                    var condition = 'danger';
                    break;
            
                default:
                    var condition = 'warning';
                    break;
            }
            $('td#condition').html('<span class="right badge badge-'+condition+'">'+persentage.toFixed(2)+' % Kosong</span>')

        }

        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        function tambahIPAddress() {
            $('#add-row-address').append(
                '<div class="form-group">'+
                    '<label for="new_ip_address">IP Address</label>'+
                    '<?php echo Form::text("ip_address[]", old("ip_address"), ["class" => "form-control"]); ?>'+
                '</div>'
            );
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/pages/virtual-machine/vm/index.blade.php ENDPATH**/ ?>