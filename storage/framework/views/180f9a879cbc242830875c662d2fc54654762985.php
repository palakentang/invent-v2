<?php $__env->startSection('css-third'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('v1/dist/css/daterangepicker/daterangepicker.css')); ?>" />
<script>

</script>
<style>
    .fab-container {
        position: fixed;
        bottom: 5px;
        right: 13px;
        z-index: 999;
        cursor: pointer;
    }

    .fab-icon-holder {
    width: 50px;
    height: 50px;
    border-radius: 100%;
    background: #016fb9;
    box-shadow: 0 6px 20px rgba(0, 0, 0, 0.2);
    }

    .fab-icon-holder:hover {
    opacity: 0.8;
    }

    .fab-icon-holder i {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    font-size: 25px;
    color: #ffffff;
    }

    .fab {
    width: 60px;
    height: 60px;
    background: #28a745;
    }

    .fab-options {
    list-style-type: none;
    margin: 0;
    position: absolute;
    bottom: 70px;
    right: 0;
    opacity: 0;
    transition: all 0.3s ease;
    transform: scale(0);
    transform-origin: 85% bottom;
    }

    .fab:hover+.fab-options,
    .fab-options:hover {
    opacity: 1;
    transform: scale(1);
    }

	.modal-backdrop {
	   background-color: blue;
	}

	.modal-title {
		font-size: 16px;
	}

	.modal-header {
		font-size: 3em;
		padding-top: 0px;
		padding-bottom: 0px;
	   /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#b3dced+0,29b8e5+50,bce0ee+100;Blue+Pipe */
		background: #b3dced; /* Old browsers */
		background: -moz-linear-gradient(top, #b3dced 0%, #29b8e5 50%, #bce0ee 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee',GradientType=0 ); /* IE6-9 */ 

	}
	
	.modal-body {
		/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f6f8f9+0,e5ebee+50,d7dee3+51,f5f7f9+100;White+Gloss */
		background: #f6f8f9; /* Old browsers */
		background: -moz-linear-gradient(top,  #f6f8f9 0%, #e5ebee 50%, #d7dee3 51%, #f5f7f9 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f6f8f9', endColorstr='#f5f7f9',GradientType=0 ); /* IE6-9 */		
	   	}

</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>

                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <?php $__env->startComponent('layouts.partials.alert'); ?>
            <?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <div class="row">
                
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
							    <div class="col-md-12 bg-light text-right">
                                    
                                    <?php if(\Request::segment(1) == 'master'): ?>
                                        <button onclick="tambahBarang()" class="btn btn-success float-right"><i class="fas fa-plus">&nbsp;</i>Tambah Perangkat</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                    
					        &nbsp;
                            <?php $__env->startComponent('copmonents.tables.barang', [
                                'data' => $data,
                                'route' => $route
                            ]); ?>

                            <?php if (isset($__componentOriginal870d0ce9451055925b7040660dec0effe5745542)): ?>
<?php $component = $__componentOriginal870d0ce9451055925b7040660dec0effe5745542; ?>
<?php unset($__componentOriginal870d0ce9451055925b7040660dec0effe5745542); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

                            
                            <?php $__env->startComponent('copmonents.modal.barang.edit', [
                                'select' => $select,
                                'data' => null
                            ]); ?>

                            <?php if (isset($__componentOriginalebb12bd1f429efc0b3fbb7b24ec4c6d59f4badb2)): ?>
<?php $component = $__componentOriginalebb12bd1f429efc0b3fbb7b24ec4c6d59f4badb2; ?>
<?php unset($__componentOriginalebb12bd1f429efc0b3fbb7b24ec4c6d59f4badb2); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

                            
                            <?php $__env->startComponent('copmonents.modal.barang.add', [
                                'select' => $select
                            ]); ?>

                            <?php if (isset($__componentOriginal81a9c80bbbc28d71e955fbca7b9b11f2cd9adf6e)): ?>
<?php $component = $__componentOriginal81a9c80bbbc28d71e955fbca7b9b11f2cd9adf6e; ?>
<?php unset($__componentOriginal81a9c80bbbc28d71e955fbca7b9b11f2cd9adf6e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

                            
                            <?php $__env->startComponent('copmonents.modal.barang.show', [
                                'select' => $select,
                                'data' => null
                            ]); ?>

                            <?php if (isset($__componentOriginal1a56bd5bd07a9449d5014871ec9a51974f179726)): ?>
<?php $component = $__componentOriginal1a56bd5bd07a9449d5014871ec9a51974f179726; ?>
<?php unset($__componentOriginal1a56bd5bd07a9449d5014871ec9a51974f179726); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

                            
                            <?php $__env->startComponent('copmonents.modal.ip.mgmt'); ?>

                            <?php if (isset($__componentOriginald2f72f05e0ad10dc9a1b7f02f09d5f8f0f2ebf81)): ?>
<?php $component = $__componentOriginald2f72f05e0ad10dc9a1b7f02f09d5f8f0f2ebf81; ?>
<?php unset($__componentOriginald2f72f05e0ad10dc9a1b7f02f09d5f8f0f2ebf81); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            
                            
                            <?php $__env->startComponent('copmonents.modal.barang.add-ip'); ?>

                            <?php if (isset($__componentOriginal0c38fa6af95ef8dd56dc0edf5d7752785120e12c)): ?>
<?php $component = $__componentOriginal0c38fa6af95ef8dd56dc0edf5d7752785120e12c; ?>
<?php unset($__componentOriginal0c38fa6af95ef8dd56dc0edf5d7752785120e12c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

                    </div>
                </div>
                <!--<div class="fab-container">-->
                <!--    <div class="fab fab-icon-holder">-->
                <!--        <a href="#" onclick="tambahBarang()" ><i class="fas fa-plus"></i></a>-->
                <!--    </div>-->
                <!--</div>-->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js-third'); ?>
<script type="text/javascript" src="<?php echo e(asset('v1/dist/js/moment.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('v1/dist/js/daterangepicker.min.js')); ?>"></script>
<script>
    function tambahBarang() {
        $('#modal-tambah').modal()
        $("input[type=text]").val("");

    }
    
    function tambahIP(params) {
        console.log(params)
        $('#modal-tambah-ip').modal()
        $('input#barang_id').val(params)
      $("input[type=text]").val("");

    }
</script>
<?php $__env->startComponent('copmonents.script.barang'); ?>

<?php if (isset($__componentOriginal835fe954d0a0f59058a4c35c587e6e1fe4233af3)): ?>
<?php $component = $__componentOriginal835fe954d0a0f59058a4c35c587e6e1fe4233af3; ?>
<?php unset($__componentOriginal835fe954d0a0f59058a4c35c587e6e1fe4233af3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/pages/master/barang/index.blade.php ENDPATH**/ ?>