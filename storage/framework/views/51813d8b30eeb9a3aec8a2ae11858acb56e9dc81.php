
<?php $__env->startSection('css-third'); ?>
<style>
    .form-check-input {
        margin-top: -0.7rem; 
    }
</style>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>
                    
                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form <?php echo e($pageTitle); ?></h5>
                        </div>
                        <div class="card-body">
                            <?php $__env->startComponent('layouts.partials.alert'); ?>
                                
                            <?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            <form action="<?php echo e(route($route['update'], $data->id)); ?>" method="post">
                                <?php echo method_field('PUT'); ?>
                                <?php echo csrf_field(); ?>
                                <?php echo Form::hidden('id', $data->id, [null]); ?>

                                <div class="form-group">
                                    <label for="name">Lokasi</label>
                                    <?php echo Form::text('nama_os', $data->nama_os, ['class' => 'form-control', 'id' => 'nama']); ?>

                                </div>
                                <div class="form-group">
                                    <label for="name">Deskripsi</label>
                                    <textarea name="deskripsi" id="deskripsi" cols="2" rows="2" class="form-control"><?php echo e($data->deskripsi); ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-success float-right">Update <?php echo e($pageTitle); ?></button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List <?php echo e($pageTitle); ?></h5>
                        </div>
                        <div class="card-body">
                            <?php $__env->startComponent('copmonents.tables.os', [
                                'thead' => ['Nama OS', 'Deskripsi'],
                                'data' => $datas,
                                'route' => $route
                            ]); ?>
                                
                            <?php if (isset($__componentOriginal364f505a1f8e8626b9926975d313c68f4d89ef45)): ?>
<?php $component = $__componentOriginal364f505a1f8e8626b9926975d313c68f4d89ef45; ?>
<?php unset($__componentOriginal364f505a1f8e8626b9926975d313c68f4d89ef45); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>                      
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laravel\inv\invent-v2\resources\views/pages/master/os/edit.blade.php ENDPATH**/ ?>