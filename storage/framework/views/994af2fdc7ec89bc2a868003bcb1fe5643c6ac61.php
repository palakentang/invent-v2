<?php echo $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'; ?>

<div class="form-group">
    <label for="nama_cluster">Nama Cluster</label>
    <?php echo Form::text('nama_cluster', old('nama_cluster'), ['class' => 'form-control', 'required']); ?>

</div>
<div class="form-group">
    <label for="cluster_system_id">Cluster System</label>
    <select name="cluster_system_id" id="" class="form-control" required>
        <option value="">Pilih Cluster System</option>
        <?php $__empty_1 = true; $__currentLoopData = \App\Models\ClusterSystem::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>
    </select>
</div>
<?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/copmonents/form/cluster.blade.php ENDPATH**/ ?>