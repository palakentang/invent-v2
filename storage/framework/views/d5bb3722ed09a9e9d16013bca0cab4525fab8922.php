<div class="modal fade" id="modal-mgmt" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="set-it-mgmt"></div>
                <center>
                    <div style="display: none" class="spinner-grow" role="status">
                        <span class="sr-only">Mohon menunggu...</span>
                    </div>
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="history.go(0);" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/modal/ip/mgmt.blade.php ENDPATH**/ ?>