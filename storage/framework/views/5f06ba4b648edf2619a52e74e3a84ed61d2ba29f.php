
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="<?php echo e(route('dashboard')); ?>">Home</a></li>
        <?php for($i = 1; $i <= count(Request::segments()); $i++): ?>
        <li class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'breadcrumb-item', 'active' => Request::segment($i) != 'master']) ?>" class="breadcrumb-item">
        <?php echo Request::segment(2) == 'hypervisor' ? ucwords(str_replace("-"," ",Request::segment($i))) : '<a style="text-decoration: none;color: #6c757d;font-weight: 600;" href="'.Request::segment($i).'.index")>'.ucwords($pageTitle).'</a>'; ?>

        </li>
        <?php endfor; ?>
    </ol>
</div><!-- /.col -->
<?php /**PATH C:\Laravel\inventaris\resources\views/layouts/partials/breadcrumb.blade.php ENDPATH**/ ?>