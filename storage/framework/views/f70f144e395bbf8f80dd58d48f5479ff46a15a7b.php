<div class="modal fade" id="modal-edit" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Perangkat</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="edit">
			        <?php echo method_field('PUT'); ?>	
                    <?php echo csrf_field(); ?>
                    <?php $__env->startComponent('copmonents.form.barang-edit', [
                        'data' => null,
                        'select' => $select
                    ]); ?>
                        
                    <?php if (isset($__componentOriginal2a2b257ab6f35b073f3f825d28e3e86b973cbd48)): ?>
<?php $component = $__componentOriginal2a2b257ab6f35b073f3f825d28e3e86b973cbd48; ?>
<?php unset($__componentOriginal2a2b257ab6f35b073f3f825d28e3e86b973cbd48); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?> 
                </form>
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="button" onClick="event.preventDefault(); document.getElementById('edit').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/modal/barang/edit.blade.php ENDPATH**/ ?>