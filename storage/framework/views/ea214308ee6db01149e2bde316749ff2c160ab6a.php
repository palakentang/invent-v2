<div class="modal fade" id="modal-edit" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit <?php echo e($pageTitle); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo e($route); ?>" method="post" id="update">
                    <?php echo csrf_field(); ?>
                    <?php echo e(method_field('PUT')); ?>

                    <?php $__env->startComponent($form, [
                        'data' => $data,
                        'select' => $select
                    ]); ?>

                    <?php if (isset($__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96)): ?>
<?php $component = $__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96; ?>
<?php unset($__componentOriginalfb05a43927cff438da7fdbd15552ff3227bd4b96); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('update').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/modal/global/edit.blade.php ENDPATH**/ ?>