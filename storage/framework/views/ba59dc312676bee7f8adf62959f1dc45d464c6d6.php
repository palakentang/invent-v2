<div id="modal-show" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="header"></h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="width:100%" class="table table-striped">
                        <?php $__currentLoopData = $field; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th><?php echo e(str_replace('show', '', ucfirst(str_replace('_', ' ', $item)))); ?></th>
                                <td id="<?php echo e($item); ?>"></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/copmonents/modal/global/show.blade.php ENDPATH**/ ?>