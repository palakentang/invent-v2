<?php $__env->startSection('css-third'); ?>
<style>
</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>

                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6"><h5 class="m-0">List <?php echo e($pageTitle); ?></h5></div>
                                <?php if(\Request::segment(1) == 'master'): ?>
                                    <div class="col-6"><button class="btn btn-success float-right" onclick="tambah()">Tambah Cluster</button></div>
                                <?php endif; ?>
                            </div>
                            
                            
                        </div>
                        <div class="card-body">
                            <?php $__env->startComponent('layouts.partials.alert'); ?><?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            <?php $__env->startComponent('copmonents.tables.vm', [
                                'thead' => [
                                    'Nama VM', 
                                    'Description',
                                    'Node', 
                                    'IP Address',
                                    'Component', 
                                    'Services',
                                    'Used Space',
                                    'ON / OFF',
                                    'Condition',
                                ],
                                'data' => $data,
                                'route' => $route
                            ]); ?>

                            <?php if (isset($__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3)): ?>
<?php $component = $__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3; ?>
<?php unset($__componentOriginale2f74cc30d1c3e40463cce1afd3afb1ab5fb43e3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <?php $__env->startComponent('copmonents.modal.global.show', [
                'field' => [
                    'name',
                    'node_id',
                    'description',
                    'hostname',
                    'ip_address',
                    'vCPU',
                    'vRAM',
                    'vDISK',
                    'OS',
                    'component',
                    'serive',
                    'used_space',
                    'power',
                    'condition',
                ],
            ]); ?>

            <?php if (isset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c)): ?>
<?php $component = $__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c; ?>
<?php unset($__componentOriginal6132b800f182b4533a32bc2b4d4ecec34f2a6f4c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah VM',
                'route' => route($route['store']),
                'form' => 'copmonents.form.vm',
                'select' => \App\Models\Node::get(),
            ]); ?>

            <?php if (isset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759)): ?>
<?php $component = $__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759; ?>
<?php unset($__componentOriginalc3813d406791b7f3f83e68bd0560f70b89282759); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php $__env->startComponent('copmonents.modal.global.edit', [
                'pageTitle' => 'VM',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.vm',
                'select' => null,
            ]); ?>

            <?php if (isset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb)): ?>
<?php $component = $__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb; ?>
<?php unset($__componentOriginal8a2c85f571576d54073edad806992bdeb54e69cb); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js-third'); ?>
    <script>

        function tambah() {
            $('#modal-tambah').modal()
            
        }

        function ubah(params) {
            $('#modal-edit').modal()
            $("form#update").attr('action', "<?php echo e(url('master/virtual-machine/vm/')); ?>/"+params.id+"");
            $('input[name=name]').val(params.name)
            $('select[name=node_id]').val(params.node_id)
            $('input[name=name]').val(params.name)
            $('input[name=node_id]').val(params.node.node_name)
            $('textarea[name=description]').val(params.description)
            $('input[name=hostname]').val(params.hostname)
            $('input[name=ip_address]').val(params.ip_address)
            $('input[name=vCPU]').val(params.vCPU)
            $('input[name=vRAM]').val(params.vRAM)
            $('input[name=vDISK]').val(params.vDISK)
            $('input[name=OS]').val(params.OS)
            $('input[name=component]').val(params.component)
            $('input[name=serive]').val(params.serive)
            $('input[name=used_space]').val(params.used_space)
            console.log(params.power.toUpperCase());
            $("#edit_"+params.power.toUpperCase()).attr("checked", true);

        }

        function lihat(params) {
            $('#modal-show').modal();
            console.log(params.UsedSpaced);

            $('td#name').html(params.name)
            $('td#node_id').html(params.node.node_name)
            $('td#description').html(params.description)
            $('td#hostname').html(params.hostname)
            
            var html = '<ul style="list-style: none;padding: 0;">'
            $.each(JSON.parse(params.ip_address), function (indexInArray, valueOfElement) { 
                html +='<li>'+valueOfElement+'</li>'
            });
                html += '<ul>'
            $('td#ip_address').html(html)
            $('td#vCPU').html(params.vCPU)
            $('td#vRAM').html(params.vRAM)
            $('td#vDISK').html(params.vDISK)
            $('td#OS').html(params.OS)
            $('td#component').html(params.component)
            $('td#serive').html(params.serive)
            $('td#used_space').html(params.used_space)
            var power = params.power == 'on' ? 'success' : 'danger';
            var htmlPower = '<span class="right badge badge-'+power+'">'+params.power.toUpperCase()+'</span>'
            $('td#power').html(htmlPower)
            var persentage = ((params.vDISK - params.used_space) / params.vDISK) * 100;
            switch (true) {
                case (persentage > 50):
                    var condition = 'success';
                    break;
                case (persentage < 50):
                    var condition = 'danger';
                    break;
            
                default:
                    var condition = 'warning';
                    break;
            }
            $('td#condition').html('<span class="right badge badge-'+condition+'">'+persentage.toFixed(2)+' % Kosong</span>')

        }

        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        function tambahIPAddress() {
            $('#add-row-address').append(
                '<div class="form-group">'+
                    '<label for="new_ip_address">IP Address</label>'+
                    '<?php echo Form::text("ip_address[]", old("ip_address"), ["class" => "form-control"]); ?>'+
                '</div>'
            );
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laravel\inventaris\resources\views/pages/virtual-machine/vm/index.blade.php ENDPATH**/ ?>