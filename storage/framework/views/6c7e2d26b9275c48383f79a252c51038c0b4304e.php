<table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
    <thead>
    
    <tr style="text-align: center">
			<?php if(Request::segment(1) == 'master'): ?>            
            <th width="30">Aksi</th>
			<?php else: ?>
		    <th width="10">Aksi</th>
            <?php endif; ?>		
            <th width="8">No.</th>
            <th width="150">Cluster</th>
            <th width="90">Host</th>
            <th>Nama VM</th>
            <th>State</th>
            <th width="90">Kondisi Disk</th>
            <th width="80">vRAM</th>			
            <th width="80">vCPU</th>
            <th width="80">vDisk</th>
            <th width="80">Guest OS</th>			
			<th width="50">IP Address</th>
        </tr>    
    </thead>
    <tbody>
    <?php
       $no = 0;
    ?>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $no++;
        ?>    
            <td style="white-space:nowrap; align=right; vertical-align:top;">
                <?php if(Request::segment(1) == 'master'): ?>
					<a href="#" class="btn btn-success btn-sm" onclick="show(<?php echo e($item); ?>, <?php echo e($item->Cluster); ?>)"><i class="fa fa-info-circle" aria-hidden="true"></i></a>                            
					<a href="#" class="btn btn-warning btn-sm" onclick="ubah(<?php echo e($item); ?>)"><i class="fa fa-edit" aria-hidden="true"></i></a>
					<form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post" id="hapus<?php echo e($item->id); ?>" style="display: none">
						<?php echo method_field('DELETE'); ?>
						<?php echo csrf_field(); ?>
					</form>
					<a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus<?php echo e($item->id); ?>').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
				<?php else: ?>
					<a href="#" class="btn btn-success btn-sm" onclick="show(<?php echo e($item); ?>)"><i class="fa fa-info-circle" aria-hidden="true"></i></a>					
				<?php endif; ?>			
            </td>
            
            <td style="vertical-align:top;"><?php echo e($no); ?></td>
            <td style="vertical-align:top;"><?php echo e(isset($item->Cluster) ? $item->Cluster->nama_cluster : '-'); ?></td>
            <td style="vertical-align:top;"><?php echo e($item->hostname); ?></td>            
            <td style="vertical-align:top;"><?php echo e($item->name); ?></td>
            <td style="vertical-align:top;"><span class="<?php echo \Illuminate\Support\Arr::toCssClasses([
                'right badge badge-success' => $item->power == 'on',
                'right badge badge-danger' => $item->power == 'off',
            ]) ?>"><?php echo e(strtoupper($item->power)); ?></span></td>
            <td style="vertical-align:top;"><?php echo e($item->used_space); ?>&nbsp;&nbsp;
                <?php switch(true):
                    case ($item->PercentageUsedSpaced($item) > 50): ?>
                        <span class="right badge badge-success"><?php echo e($item->UsedSpaced()); ?> Kosong</span>
                        <?php break; ?>
                    <?php case ($item->PercentageUsedSpaced($item) < 50): ?>
                        <span class="right badge badge-danger"><?php echo e($item->UsedSpaced()); ?> Kosong</span>
                        <?php break; ?>
                    <?php default: ?>
                        <span class="right badge badge-warning"><?php echo e($item->UsedSpaced()); ?> Kosong</span>

                <?php endswitch; ?>
                
            </td>
            <td style="vertical-align:top;"><?php echo e($item->vRAM); ?></td>
            <td style="vertical-align:top;"><?php echo e($item->vCPU); ?></td>
            <td style="vertical-align:top;"><?php echo e($item->vDISK); ?></td>
            <td style="vertical-align:top;"><?php echo e(isset($item->OSS) ? $item->OSS->nama_os : '-'); ?></td>
            <td style="vertical-align:top;">
                <ul>
                    <?php if($item->ip_address===null): ?>  
					<?php echo e("-"); ?>

					<?php else: ?>
						<?=str_replace(';', '<br>', $item->ip_address)?>
					<?php endif; ?>
                </ul>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/copmonents/tables/vm.blade.php ENDPATH**/ ?>