<?php echo $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'; ?>

<div class="form-group">
    <label for="name">VM Name</label>
    <?php echo Form::text('name', old('name'), ['class' => 'form-control', 'required']); ?>

</div>
<div class="form-group">
    <label for="node_id">Node</label>
    <select name="node_id" id="" class="form-control" required>
        <option value="">Pilih Node</option>
        <?php $__empty_1 = true; $__currentLoopData = \App\Models\Node::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <option value="<?php echo e($item->id); ?>"><?php echo e($item->node_name); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <?php endif; ?>
    </select>
    Jika Node tidak ditemukan <a href="<?php echo e(route('master.virtual-machine.node.index')); ?>">Klik Disini</a>

</div>
<div class="form-group">
    <label for="description">Deskripsi</label>
    <textarea name="description" id="description" cols="2" rows="2" class="form-control"></textarea>
</div>

<div class="form-group" id="ip_address_row">
    <label for="ip_address">IP Address</label>
    <?php echo Form::text('ip_address[]', old('ip_address'), ['class' => 'form-control']); ?>

</div>
<div id="add-row-address">
    
</div>

<a href="#" onclick="tambahIPAddress()">Tambah IP Address</a>

<div class="row">
    <div class="col-3">
        <div class="form-group">
            <label for="vCPU">vCPU</label>
            <?php echo Form::text('vCPU', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="vRAM">vRAM</label>
            <?php echo Form::text('vRAM', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="vDISK">vDISK</label>
            <?php echo Form::text('vDISK', null, ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <label for="OS">OS</label>
            <?php echo Form::text('OS', null, ['class' => 'form-control']); ?>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="component">Komponent</label>
            <?php echo Form::text('component', '', ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="serive">Service</label>
            <?php echo Form::text('serive', '', ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="used_space">Used Space</label>
            <?php echo Form::text('used_space', '', ['class' => 'form-control']); ?>

        </div>
    </div>
    <div class="col-6">
        <label for="Power">Power</label>
        <div class="row">
            <div class="col-6">
                <div class="form-check">
                    <input name="power" value="on" type="checkbox" class="form-check-input" id="<?php echo e($data == null ? 'ON' : 'edit_ON'); ?>">
                    <label class="form-check-label" for="ON">Power ON</label>
                </div>
            </div>
            <div class="col-6">
                <div class="form-check">
                    <input name="power" value="off" type="checkbox" class="form-check-input" id="<?php echo e($data == null ? 'ON' : 'edit_OFF'); ?>">
                    <label class="form-check-label" for="OFf">Power OFF</label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/form/vm.blade.php ENDPATH**/ ?>