<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            <?php $__currentLoopData = $thead; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <th><?php echo e($item); ?></th>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if(\Request::segment(1) == 'master'): ?>

            <th>Aksi</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <td><?php echo e($item->node_name); ?></td>
            <td><?php echo e($item->cluster->name); ?></td>
            <td>
                <?php echo e($item->ip_address); ?>

            </td>
            <?php if(\Request::segment(1) == 'master'): ?>

            <td>
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-success btn-sm btn-block" href="#" onclick="ubah(<?php echo e($item); ?>)">Ubah</a>
                        <a class="btn btn-primary btn-sm btn-block" onclick="lihat(<?php echo e($item); ?>)" href="#">Lihat</a>
                        <form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post" id="hapus<?php echo e($item->id); ?>">
                            <?php echo method_field('delete'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                        <a style="margin-top: 8px;" class="btn btn-danger btn-sm btn-block" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus<?php echo e($item->id); ?>').submit()">Hapus</a>
                    </div>
                </div>
            </td>

            <?php endif; ?>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/tables/node.blade.php ENDPATH**/ ?>