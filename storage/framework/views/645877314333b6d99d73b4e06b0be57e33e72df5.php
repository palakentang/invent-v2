<table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
    <thead>
        <tr style="text-align: center">
			<?php if(Request::segment(1) == 'master'): ?>            
            <th width="30">Aksi</th>
			<?php else: ?>
		    <th width="10">Aksi</th>
            <?php endif; ?>		
            <th width="8">No.</th>
            <th width="150">Nama Barang</th>
            <th width="90">Jenis</th>
            <th>Merk / Tipe</th>
            <th>Cluster</th>			
            <th width="80">S/N</th>			
            <th width="80">Mac Address</th>
            <th width="80">IP Address</th>			
			<th width="50">Pengadaan</th>
	        <th width="90">Akhir Warranty</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no = 0;
        ?>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $no++;
        ?>
            <tr>
				<td style="white-space:nowrap; align=right; vertical-align:top;">
                <?php if(Request::segment(1) == 'master'): ?>
					<a href="#" class="btn btn-success btn-sm" onclick="show(<?php echo e($item); ?>)"><i class="fa fa-info-circle" aria-hidden="true"></i></a>                            
					<a href="#" class="btn btn-warning btn-sm" onclick="edit(<?php echo e($item); ?>)"><i class="fa fa-edit" aria-hidden="true"></i></a>
					<form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post" id="hapus<?php echo e($item->id); ?>" style="display: none">
						<?php echo method_field('DELETE'); ?>
						<?php echo csrf_field(); ?>
					</form>
					<a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus<?php echo e($item->id); ?>').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
				<?php else: ?>
					<a href="#" class="btn btn-success btn-sm" onclick="show(<?php echo e($item); ?>)"><i class="fa fa-info-circle" aria-hidden="true"></i></a>					
				<?php endif; ?>			
                </td>			
                <td style="vertical-align:top;"><?php echo e($no); ?></td>
                <td style="vertical-align:top;"><?php echo e($item->nama_barang); ?></td>
                <td style="vertical-align:top;"><?php echo e($item->JenisBarang->nama_jenis_barang); ?></td>
				<td style="vertical-align:top;"><?php echo e($item->MerkBarang->nama_merk); ?> <?php echo e($item->tipe); ?> </td>
				<td style="vertical-align:top;"> 
					<?php if(isset($item->ClusterBarang->nama_cluster)): ?>
					   <?php echo e($item->ClusterBarang->nama_cluster); ?>					
					<?php else: ?>
						<?php echo e("-"); ?>

					<?php endif; ?>
					</td>
				<td style="vertical-align:top;">
				    <?php if($item->serial_number===null): ?>  
					<?php echo e("-"); ?>

					<?php else: ?>
					<?php echo e($item->serial_number); ?>	
					<?php endif; ?>
				</td>
				<td style="vertical-align:top;">
					<?php if($item->mac_address===null): ?>  
					<?php echo e("-"); ?>

					<?php else: ?>
					<?php echo e($item->mac_address); ?>	
				<?php endif; ?>			
				</td>
				<td style="vertical-align:top;">
				
				 <?php if($item->ip_address===null): ?>  
					<?php echo e("-"); ?>

					<?php else: ?>
						<?=str_replace(';', '<br>', $item->ip_address)?>
					<?php endif; ?>
				</td>

				<td style="vertical-align:top;">
				    <?php if(isset($item->Pengadaan->tgl_pengadaan)): ?>
					   <?php echo e(date("Y", strtotime($item->Pengadaan->tgl_pengadaan))); ?>

				    <?php else: ?> 
					    - 
					<?php endif; ?> 			
				</td>
				<td style="vertical-align:top;">
			    <?php if($item->garansi_akhir===null): ?>  
					<?php echo e("-"); ?>

				<?php else: ?>
					<?php echo e(date("d F Y", strtotime($item->garansi_akhir))); ?>	
				<?php endif; ?>
				</td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/tables/barang.blade.php ENDPATH**/ ?>