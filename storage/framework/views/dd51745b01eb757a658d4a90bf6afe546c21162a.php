<div class="card">
    <div class="card-header">
        <h3 class="card-title">Kategori <?php echo e($title); ?> Report</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-100" style="padding: 20px">
        <div class="position-relative mb-4">
            <canvas id="<?php echo e($id); ?>" height="200"></canvas>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4 col-6">
                <div class="description-block border-right">
                    <h5 class="description-header"><?php echo e(strtoupper(str_replace('_', ' ', $key))); ?></h5>
                    <span class="description-text"><?php echo e($value); ?> Item</span>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.card-body -->
</div><?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/copmonents/diagram/bar-chart.blade.php ENDPATH**/ ?>