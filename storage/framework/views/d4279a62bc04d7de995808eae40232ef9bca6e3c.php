
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="<?php echo e(route('dashboard')); ?>">Home</a></li>
        <?php for($i = 1; $i <= count(Request::segments()); $i++): ?>
        <li class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'breadcrumb-item', 'active' => Request::segment($i) != 'master']) ?>" class="breadcrumb-item">
        
        <a style="text-decoration: none;color: #6c757d;font-weight: 600;" href="#")><?php echo e(ucfirst(Request::segment($i))); ?></a>
        </li>
        <?php endfor; ?>
    </ol>
</div><!-- /.col -->
<?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/layouts/partials/breadcrumb.blade.php ENDPATH**/ ?>