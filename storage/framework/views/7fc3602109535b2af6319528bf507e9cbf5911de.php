<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            <?php $__currentLoopData = $thead; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <th><?php echo e($item); ?></th>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php if(\Request::segment(1) == 'master'): ?>

            <th>Aksi</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <td><?php echo e($item->name); ?></td>
            <td>
                <?php echo e($item->description); ?>

                <ul style="list-style: none;padding: 0;">
                    <li><strong>vCPU</strong>: <?php echo e($item->vCPU); ?></li>
                    <li><strong>vRAM</strong>: <?php echo e($item->vRAM); ?></li>
                    <li><strong>vDISK</strong>: <?php echo e($item->vDISK); ?></li>
                    <li><strong>OS</strong>: <?php echo e($item->OS); ?></li>
                </ul>
            </td>
            <td><?php echo e($item->Node->node_name); ?></td>
            <td>
                <ul>
                <?php $__currentLoopData = json_decode($item->ip_address); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($value); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </td>
            <td><?php echo e($item->component); ?></td>
            <td><?php echo e($item->serive); ?></td>
            <td><?php echo e($item->used_space); ?></td>
            <td><span class="<?php echo \Illuminate\Support\Arr::toCssClasses([
                'right badge badge-success' => $item->power == 'on',
                'right badge badge-danger' => $item->power == 'off',
            ]) ?>"><?php echo e(strtoupper($item->power)); ?></span></td>
            <td>
                <?php switch(true):
                    case ($item->PercentageUsedSpaced($item) > 50): ?>
                        <span class="right badge badge-success"><?php echo e($item->UsedSpaced()); ?> Kosong</span>
                        <?php break; ?>
                    <?php case ($item->PercentageUsedSpaced($item) < 50): ?>
                        <span class="right badge badge-danger"><?php echo e($item->UsedSpaced()); ?> Kosong</span>
                        <?php break; ?>
                    <?php default: ?>
                        <span class="right badge badge-warning"><?php echo e($item->UsedSpaced()); ?> Kosong</span>

                <?php endswitch; ?>
                
            </td>

            <?php if(\Request::segment(1) == 'master'): ?>
            <td>
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-success btn-sm btn-block" href="#" onclick="ubah(<?php echo e($item); ?>)">Ubah</a>
                        <a class="btn btn-primary btn-sm btn-block" onclick="lihat(<?php echo e($item); ?>)" href="#">Lihat</a>
                        <form action="<?php echo e(route($route['delete'], $item->id)); ?>" method="post" id="hapus<?php echo e($item->id); ?>">
                            <?php echo method_field('delete'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                        <a style="margin-top: 8px;" class="btn btn-danger btn-sm btn-block" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus<?php echo e($item->id); ?>').submit()">Hapus</a>
                    </div>
                </div>
            </td>
            <?php endif; ?>
            
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/tables/vm.blade.php ENDPATH**/ ?>