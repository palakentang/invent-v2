
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo e(route('dashboard')); ?>">Home</a></li>
                        <li class="<?php echo \Illuminate\Support\Arr::toCssClasses([
                            'breadcrumb-item',
                        ]) ?>" class="breadcrumb-item">
                        <a style="text-decoration: none;color: #6c757d;font-weight: 600;" ><?php echo e(ucwords($pageTitle)); ?></a>
                        </li>
                    </ol>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo e($nTotalPerangkat); ?></h3>

                <p>Total Perangkat</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo e(route('barang.index')); ?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo e($nTotalPerangkatHabisGaransi); ?> </h3>

                <p>Perangkat Habis Garansi</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo e($nTotalVirtualMachine); ?></h3>

                <p>Total VM</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>65</h3>

                <p>VM Resource Maksimal</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->		
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Jenis Perangkat</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-100" style="padding: 20px">
                            <div class="position-relative mb-4">
                                <canvas id="sales-chart" height="200"></canvas>
                            </div>
                        </div>
                        <!--
                        <div class="card-footer">
                            <div class="row">
                                <?php $__currentLoopData = $merk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-sm-3 col-6">
                                    <div class="description-block border-right">
                                        <h5 class="description-header"><?php echo e($key); ?></h5>
                                        <span class="description-text"><?php echo e($value); ?> Item</span>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        -->
                        <!-- /.card-body -->
                    </div>
                    <?php $__env->startComponent('copmonents.diagram.bar-chart', [
                        'data' => $jenis_vm,
                        'title' => 'Virtual Machine',
                        'id' => 'virtual-machine-bar'
                    ]); ?>
                        
                    <?php if (isset($__componentOriginal1204ddefa807e7ab2370e2fb096d500cd3d6b7dc)): ?>
<?php $component = $__componentOriginal1204ddefa807e7ab2370e2fb096d500cd3d6b7dc; ?>
<?php unset($__componentOriginal1204ddefa807e7ab2370e2fb096d500cd3d6b7dc); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Komposisi Perangkat</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="chart-responsive">
                                        <canvas id="pieChart" height="175"></canvas>
                                    </div>
                                    <!-- ./chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-5">
                                    <ul class="chart-legend clearfix">
                                        <?php $__currentLoopData = $count['jenis']['quantity']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li style="margin: 10px 0;"><i class="far fa-circle <?php echo e($item->_meta); ?>">&nbsp;<?php echo e(ucfirst($item->nama_jenis_barang)); ?></i></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <?php $__env->startComponent('copmonents.diagram.pie-chart', [
                        'title' => 'Cluster System',
                        'data' => App\Models\JenisVirtualMachine::get(),
                        'id' => 'cluster-system-pie'
                    ]); ?>
                        
                    <?php if (isset($__componentOriginal9675c05edc1679fc3abe90b963cfc5c7b4194663)): ?>
<?php $component = $__componentOriginal9675c05edc1679fc3abe90b963cfc5c7b4194663; ?>
<?php unset($__componentOriginal9675c05edc1679fc3abe90b963cfc5c7b4194663); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js-third'); ?>
<script src="<?php echo e(asset('v1/plugins/chart.js/Chart.min.js')); ?>"></script>
<?php $__env->startComponent('copmonents.script.bar-chart', [
'label' => $count['jenis']['name'],
'value' => $count['jenis']['quantity']
]); ?>

<?php if (isset($__componentOriginal39a5d5c326c2eeb7afb03a0a93fb3608cd20d94c)): ?>
<?php $component = $__componentOriginal39a5d5c326c2eeb7afb03a0a93fb3608cd20d94c; ?>
<?php unset($__componentOriginal39a5d5c326c2eeb7afb03a0a93fb3608cd20d94c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('copmonents.script.pie-chart', [
'label' => $count['jenis']['name'],
'value' => $count['jenis']['quantity']
]); ?>

<?php if (isset($__componentOriginal00c83bc4044817003337a4b4650c0510cc2d7040)): ?>
<?php $component = $__componentOriginal00c83bc4044817003337a4b4650c0510cc2d7040; ?>
<?php unset($__componentOriginal00c83bc4044817003337a4b4650c0510cc2d7040); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('copmonents.script.dashboard.virtualmachine.bar-chart', [
'label' => $count['clusterSystem']['name'],
'value' => $count['clusterSystem']['quantity'],
'id' => 'cluster-system-bar'
]); ?>

<?php if (isset($__componentOriginalcaad43bce98b358fe56ab571b776301dada73825)): ?>
<?php $component = $__componentOriginalcaad43bce98b358fe56ab571b776301dada73825; ?>
<?php unset($__componentOriginalcaad43bce98b358fe56ab571b776301dada73825); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('copmonents.script.dashboard.virtualmachine.pie-chart', [
'label' => $count['clusterSystem']['name'],
'value' => $count['clusterSystem']['quantity'],
'id' => 'cluster-system-pie'
]); ?>

<?php if (isset($__componentOriginal9a44116b0d5aa999293594c825f8bd319eb40233)): ?>
<?php $component = $__componentOriginal9a44116b0d5aa999293594c825f8bd319eb40233; ?>
<?php unset($__componentOriginal9a44116b0d5aa999293594c825f8bd319eb40233); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<script>
    function show(response) {
        console.log(response.pengadaan)
        $("#modal-show").modal()

        $('#merk_atau_tipe_show').html(response.jenis_atau_tipe);
        $('#header_nama_barang').html(response.nama_barang);
        $('#jenis_barang_show').html(response.jenis_barang.nama_jenis_barang);
        $('#nama_barang_show').html(response.nama_barang);
        $('#serial_number_show').html(response.serial_number);
        $('#nama_pengadaan_show').html(response.pengadaan.nama_pengadaan);

        response.i_p_addresses.forEach(element => {
            var html = `<li>`+element.ip_address+`</li>`;
            $('#ip_show').append(html)
        });

        $('#daterange_show').html((response.garansi_awal == null ? '' : response.garansi_awal)+' - '+(response.garansi_akhir == null ? '' : response.garansi_akhir));
        // $('#pengadaan_id_show').html(response.pengadaan.nama_pengadaan);

        $('#row_show').html(JSON.parse(response.position).row);
        $('#rack_show').html(JSON.parse(response.position).rack);
        $('#unit_show').html(JSON.parse(response.position).unit);
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laravel\inv\invent-v2\resources\views/pages/dashboard.blade.php ENDPATH**/ ?>