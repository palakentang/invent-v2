<div class="modal fade" id="modal-edit" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit <?php echo e($pageTitle); ?></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="edit">
                    <?php echo method_field('PUT'); ?>
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label for="node_name">Nama Node</label>
                        <?php echo Form::text('node_name', old('node_name'), ['class' => 'form-control']); ?>

                    </div>
                    <div class="form-group">
                        <label for="cluster_id">Cluster</label>
                        <select name="cluster_id" id="cluster_id" class="form-control" required>
                            <option value="">Pilih Cluster</option>
                            <?php $__currentLoopData = App\Models\cluster::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        Jika Cluster tidak ditemukan <a href="<?php echo e(route('master.virtual-machine.cluster.index')); ?>">Klik Disini</a>
                    </div>
                    <div class="form-group">
                        <label for="machine_name">Nama Mesin</label>
                        <?php echo Form::text('machine_name', old('machine_name'), ['class' => 'form-control']); ?>

                    </div>

                    <div id="edit-row-address">
                        
                    </div>
                    <div id="add-row-addresses">
                        
                    </div>
                    

                    <a href="#" onclick="tambahIPAddress()">Tambah IP Address</a>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('edit').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/modal/virtualmachine/node/edit.blade.php ENDPATH**/ ?>