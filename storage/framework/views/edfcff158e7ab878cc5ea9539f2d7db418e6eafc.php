<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(url('/')); ?>" class="brand-link">
        <img src="<?php echo e(asset('v1/dist/img/logo_id.png')); ?>" alt="DITLALA Logo"
            style="opacity: .8; width: 100%">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?php echo e(asset('v1/dist/img/user2-160x160.jpg')); ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">DITLALA</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="<?php echo e(route('master.dashboard')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) ==
                        null
                        ]) ?>">
                        <i class="fa fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
				
                
                <li class="nav-item">
                    <a href="<?php echo e(route('master.barang.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) ==
                        'barang'
                        ]) ?>">
                        <i class="fas fa-server"></i>
                        <p>Perangkat</p>
                    </a>
                </li>
				
                
                <li class="nav-item">
					<a href="<?php echo e(route('master.virtual-machine.vm.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>Request::segment(1) == 'virtual-machine'
						]) ?>">
						<i class="fas fa-cubes"></i>
						<p>Virtual Machine</p>
					</a>
                </li>
				
                <li class="nav-item">
                    <a href="#" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) != 'dashboard' && Request::segment(1) != 'barang' && Request::segment(1) != 'tabel-pendukung']) ?>">
                        <i class="fas fa-table"></i>
                        <p>
                            Tabel Pendukung
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
					
                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.jenis.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'jenis'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Jenis Barang</p>
                            </a>
                        </li>

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.pengadaan.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'pengadaan'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Pengadaan</p>
                            </a>
                        </li>

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.lokasi.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'lokasi'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Lokasi</p>
                            </a>
                        </li>                        

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.merk.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'merk'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Merk</p>
                            </a>
                        </li>

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.statusbarang.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'statusbarang'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Status Perangkat</p>
                            </a>
                        </li>

                        

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.virtual-machine.cluster.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'cluster'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Cluster</p>
                            </a>
                        </li>

                        
                        <li class="nav-item">
                            <a href="<?php echo e(route('master.virtual-machine.cluster-system.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'cluster-system'
                                ]) ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Cluster System</p>
                            </a>
                        </li>				
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside><?php /**PATH C:\Laravel\inventaris\resources\views/layouts/partials/sidebar.blade.php ENDPATH**/ ?>