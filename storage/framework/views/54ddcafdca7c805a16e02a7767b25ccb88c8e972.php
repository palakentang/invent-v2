  * : wajib isi   
 
  <div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="nama_barang">Nama Perangkat <sup>*</sup></label>
				<?php echo Form::text('nama_barang', $data == null ? null : $data->nama_barang, ['class' => 'form-control', 'id' => 'nama_barang']); ?>

			</div>	   
			
		</div>
		<div class="col">
			<div class="form-group">
				<label for="jenis_barang_id">Jenis Perangkat<sup>*</sup></label>
				<?php echo Form::select('jenis_barang_id', $select['jenis'], $data == null ? null : $data->jenis_barang_id, ['class' => 'form-control']); ?>

			</div>
		</div>
		<div class="col">
			<div class="form-group">
				<label for="cluster_id">Cluster<sup>*</sup></label>
				<?php echo Form::select('cluster_id', $select['cluster'], $data == null ? null : $data->cluster_id, ['class' => 'form-control', 'id' => 'clusterEdit']); ?>				
			</div>
		</div>
		<div class="col">
			<div class="form-group">
				<label for="merk_id">Merk <sup>*</sup></label>
				<?php echo Form::select('merk_id', $select['merk'], $data == null ? null : $data->merk_id, ['class' => 'form-control']); ?>

			</div>
		</div>
   </div>	
   <div class="row">
      <div class="col">
		<div class="form-group">
			<label for="Tipe">Tipe</label>
			<?php echo Form::text('tipe', $data == null ? null : $data->tipe, ['class' => 'form-control', 'id' => 'tipe']); ?>

		</div>
    </div>
 
    <div class="col">
		<div class="form-group">
			<label for="serial_number">Serial Number</label>
			<?php echo Form::text('serial_number', $data == null ? null : $data->serial_number, ['class' => 'form-control', 'id' => 'serial_number']); ?>

		</div>
    </div>
    <div class="col">
		<div class="form-group">
			<label for="mac_address">Mac Address</label>
			<?php echo Form::text('mac_address', old('mac_address'), ['class' => 'form-control']); ?>

		</div>
    </div>
	<div class="col">
		<div class="form-group">
			<label for="status_id">Status Perangkat<sup>*</sup></label>
			<?php echo Form::select('status_id', $select['status'], $data == null ? null : $data->status_id, ['class' => 'form-control']); ?>

		</div>
	</div>
  </div> 
  <div class="row">
		<div class="col">
			<div class="form-group">
				<label for="lokasi_id">Lokasi Perangkat <sup>*</sup></label>
				<?php echo Form::select('lokasis_id', $select['lokasi'], $data == null ? null : $data->lokasis_id, ['class' => 'form-control']); ?>

			</div>
		</div>
		<div class="col">		
			<div class="form-group">
				<label for="rack">Posisi Rack</label>
				<?php echo Form::text('PosisiRack', old('PosisiRack'), ['class' => 'form-control']); ?>

			</div>
		</div>
		<div class="col">		
			<div class="form-group">
				<label for="unit">Unit Rack</label>
				<?php echo Form::text('UnitRack', old('UnitRack'), ['class' => 'form-control']); ?>

			</div>
		</div>
  </div>	
  <div class="row">
		<div class="col">
			<div class="form-group">
				<label for="pengadaans_id">Sumber Pengadaan</label>
				<?php echo Form::select('pengadaans_id', $select['pengadaan'], $data == null ? null : $data->pengadaans_id , ['class' => 'form-control', 'id' => 'pengadaans_idEdit']); ?>

			</div>
        </div>
		<div class="col-6">
			<div class="form-group">
				<label>Masa Garansi / Support:</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="far fa-calendar-alt"></i>
						</span>
					</div>
					<?php echo Form::text('GaransiMulai', $data == null ? null : $data->garansi_awal, ['class' => 'form-control' ]); ?>

					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="far fa-calendar-alt"></i>
						</span>
					</div>
					<?php echo Form::text('GaransiAkhir', $data == null ? null : $data->garansi_akhir, ['class' => 'form-control' ]); ?>

				</div>	
			</div>
		</div>	
   </div>	
	

  <div class="row">
	<div class="col">
		<div class="form-group">
		   <div id="ipAddress-Block" style="display: block;">
		   <label for="IPAddresses">IP Address</label>
		    <?php echo Form::textarea('IPAddresses', $data == null ? null : $data->ip_address, ['id' => 'IPAddresses', 'rows' => 2, 'style' => 'resize:none','class' => 'form-control']); ?>

			Isikan seluruh IP Address, dengan pemisah ; (titik koma), dan tambahkan ", mgmt" jika IP Mgmt, cth : 192.168.1.1, mgmt; 192.168.1.2; dst...
		   </div>
		</div>
	</div>	
  </div>	
<?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/form/barang-edit.blade.php ENDPATH**/ ?>