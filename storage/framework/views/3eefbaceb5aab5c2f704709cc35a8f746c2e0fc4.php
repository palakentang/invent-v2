<div class="modal fade" id="modal-show" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Perangkat</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="show">
			        <?php echo method_field('PUT'); ?>	
                    <?php echo csrf_field(); ?>
                    <?php $__env->startComponent('copmonents.form.barang-show', [
                        'data' => null,
                        'select' => $select
                    ]); ?>
                        
                    <?php if (isset($__componentOriginal9ef5333aa88822dbfc43f92a3672c88be13ce07d)): ?>
<?php $component = $__componentOriginal9ef5333aa88822dbfc43f92a3672c88be13ce07d; ?>
<?php unset($__componentOriginal9ef5333aa88822dbfc43f92a3672c88be13ce07d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
				

                </form>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inv\invent-v2\resources\views/copmonents/modal/barang/show.blade.php ENDPATH**/ ?>