<?php echo $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'; ?>

<div class="form-group">
    <label for="node_name">Nama Node</label>
    <?php echo Form::text('node_name', old('node_name'), ['class' => 'form-control']); ?>

</div>
<div class="form-group">
    <label for="cluster_id">Cluster</label>
    <select name="cluster_id" id="cluster_id" class="form-control" required>
        <option value="">Pilih Cluster</option>
        <?php $__currentLoopData = App\Models\cluster::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    Jika Cluster tidak ditemukan <a href="<?php echo e(route('master.virtual-machine.cluster.index')); ?>">Klik Disini</a>
</div>
<div class="form-group">
    <label for="machine_name">Nama Mesin</label>
    <?php echo Form::text('machine_name', old('machine_name'), ['class' => 'form-control']); ?>

</div>

<div class="form-group" id="ip_address_row">
    <label for="ip_address">IP Address</label>
    <?php echo Form::text('ip_address', old('ip_address'), ['class' => 'form-control']); ?>

</div><?php /**PATH C:\Laravel\inventaris\resources\views/copmonents/form/node.blade.php ENDPATH**/ ?>