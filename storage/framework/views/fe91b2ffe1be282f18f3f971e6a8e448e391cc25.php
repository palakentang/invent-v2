<?php $__env->startSection('css-third'); ?>
<style>
    .form-check-input {
        margin-top: -0.7rem; 
    }
</style>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contents'); ?>
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?php echo e($pageTitle); ?></h1>
                </div><!-- /.col -->
                <?php $__env->startComponent('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ]); ?>
                    
                <?php if (isset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88)): ?>
<?php $component = $__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88; ?>
<?php unset($__componentOriginal9c47395990edafc31dd0c0667147a1344fac2b88); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form <?php echo e($pageTitle); ?></h5>
                        </div>
                        <div class="card-body">
                            <?php $__env->startComponent('layouts.partials.alert'); ?>
                                
                            <?php if (isset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf)): ?>
<?php $component = $__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf; ?>
<?php unset($__componentOriginal24792f5051be036beeb53aeb12c358164352a9bf); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                            <form action="<?php echo e(route($route['store'])); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                <div class="form-group">
                                    <label for="nama_jenis_barang">Nama Jenis Barang</label>
                                    <?php echo Form::text('nama_jenis_barang', old('nama_jenis_barang'), ['class' => 'form-control', 'id' => 'nama_jenis_barang']); ?>

                                </div>
                                <button type="submit" class="btn btn-success float-right">Simpan <?php echo e($pageTitle); ?></button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List <?php echo e($pageTitle); ?></h5>
                        </div>
                        <div class="card-body">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr style=" text-align: center">
                                        <th>Jenis Barang</th>
                                        <th>Dibuat</th>
                                        <th>Diubah</th>
                                        <th width="40">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr style=" text-align: center">
                                        <td><?php echo e($item->nama_jenis_barang); ?></td>
                                        <td><?php echo e(date('d F Y', strtotime($item->created_at))); ?></td>
                                        <td><?php echo e(date('d F Y', strtotime($item->updated_at))); ?></td>
                                        <td style="white-space:nowrap; align=right;">
                                            <div class="row">
                                                
                                                <div class="col-6">
													<form action="<?php echo e(route('master.jenis.destroy', $item)); ?>" method="post">
													<a title="edit" class="btn btn-success btn-sm" href="<?php echo e(route('master.jenis.edit', $item->id)); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
													<button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
														<?php echo method_field('delete'); ?>
														<?php echo csrf_field(); ?>
													</form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laravel\inventaris\resources\views/pages/master/jenis/index.blade.php ENDPATH**/ ?>