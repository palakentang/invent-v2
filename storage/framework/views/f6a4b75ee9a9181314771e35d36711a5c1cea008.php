<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(url('/')); ?>" class="brand-link">
        <img src="<?php echo e(asset('v1/dist/img/logo_id.png')); ?>" alt="DITLALA Logo"
            style="opacity: .8; width: 100%">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?php echo e(asset('v1/dist/img/user2-160x160.jpg')); ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">DITLALA</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="<?php echo e(route('dashboard')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) ==
                        null
                        ]) ?>">
                        <i class="fas fa fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="<?php echo e(route('barang.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) ==
                        'barang-guest'
                        ]) ?>">
                        <i class="fas fa-server"></i>
                        <p>Daftar Perangkat</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo e(route('virtual-machine.index')); ?>" class="<?php echo \Illuminate\Support\Arr::toCssClasses([ 'nav-link' , 'active'=> Request::segment(1) == 
					    'virtual-machine'
                        ]) ?>">
                        <i class="fas fa-cubes"></i>
                        <p>Daftar Virtual Machine</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside><?php /**PATH /Users/masfahri/Sites/laravel/inventaris/resources/views/layouts/partials/sidebar-guest.blade.php ENDPATH**/ ?>