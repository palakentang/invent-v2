<div class="modal fade" id="modal-tambah" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Perangkat</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo e(route('master.barang.store')); ?>" method="post" id="store">
                    <?php echo csrf_field(); ?>
                    <?php $__env->startComponent('copmonents.form.barang', [
                        'data' => null,
                        'select' => $select
                    ]); ?>
                        
                    <?php if (isset($__componentOriginal452984e8b00db611123ef77e1ba697792aee9e3c)): ?>
<?php $component = $__componentOriginal452984e8b00db611123ef77e1ba697792aee9e3c; ?>
<?php unset($__componentOriginal452984e8b00db611123ef77e1ba697792aee9e3c); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
                </form>
            </div>
            <div class="modal-footer">
			    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="button" onClick="event.preventDefault(); document.getElementById('store').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\Laravel\inv\invent-v2\resources\views/copmonents/modal/barang/add.blade.php ENDPATH**/ ?>