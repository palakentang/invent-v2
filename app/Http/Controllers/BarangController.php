<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Lokasi;
use App\Models\Pengadaan;
use App\Models\JenisBarang;
use App\Models\ClusterBarang;
use App\Models\MerkBarang;
use App\Models\StatusBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BarangRequest;
use App\Imports\BarangImportFromSQL;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class BarangController extends Controller
{
    public function __construct() {
        $this->view = 'barang';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Daftar Perangkat';
        $this->model = new Barang();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Barang::with('Pengadaan', 'MerkBarang', 'ClusterBarang', 'JenisBarang','StatusBarang', 'Lokasi')->get();
        return view('pages.master.barang.index', [
            'pageTitle' => $this->pageTitle,
            'select' => [
                'lokasi' => Lokasi::all()->pluck('nama_lokasi', 'id')->prepend('Pilih Lokasi', ''),
                'cluster' => ClusterBarang::all()->pluck('nama_cluster', 'id')->prepend('Pilih Cluster', ''),
                'pengadaan' => Pengadaan::all()->pluck('nama_pengadaan', 'id')->prepend('Pilih Pengadaan', ''),
                'jenis' => JenisBarang::all()->pluck('nama_jenis_barang', 'id')->prepend('Pilih Jenis Perangkat', ''),
				'status' => StatusBarang::all()->pluck('desc_status', 'id_status')->prepend('Pilih Status Perangkat', ''),
				'merk' => MerkBarang::all()->pluck('nama_merk', 'id')->prepend('Pilih Merk Perangkat', '')
            ],
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'delete' => $this->delete
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BarangRequest $request, Barang $barang)
    {
        try {
            DB::beginTransaction();
            $barang = $barang::create([
                'nama_barang' => $request->nama_barang,
                'jenis_barang_id' => $request->jenis_barang_id,
                'merk_id' => $request->merk_id,
				'tipe' => $request->tipe,
                'serial_number' => $request->serial_number,
                'cluster_id' => $request->cluster_id,
                'mac_address' => $request->mac_address,
				'status_id' => $request->status_id,
				'lokasis_id' => $request->lokasis_id,
				'rack_position' => $request->PosisiRack,
				'rack_unit' => $request->UnitRack,
                'pengadaans_id' => $request->pengadaans_id,
                'ip_address' => $request->IPAddresses,
                'garansi_awal' => empty($request->GaransiMulai) ? null : date("Y-m-d", strtotime($request->GaransiMulai)),
                'garansi_akhir' => empty($request->GaransiAkhir) ? null : date("Y-m-d", strtotime($request->GaransiAkhir)),
            ]);
			
            /*
			if(isset($request->IPAddresses)) {
			   if(strpos($request->IPAddresses, ";")) {				   
				   $arrIPAddress = explode(";", $request->IPAddresses);
				   foreach($arrIPAddress as $cDetIPAddress)  {
					   if(strpos($cDetIPAddress, ",")) {
						   $arrDetIPAddress = explode(",", $cDetIPAddress); 
						   $cIPAddress = $arrDetIPAddress[0];
						   $ipAddress = new IPAddress();
						   $ipAddress->t_barangs_id = $barang->id;
						   $ipAddress->ip_address = $cIPAddress;
						   $ipAddress->isMgmt = 1;
						   $ipAddress->save();				   						   
					   } else {
						   $ipAddress = new IPAddress();
						   $ipAddress->t_barangs_id = $barang->id;
						   $ipAddress->ip_address = $cDetIPAddress;
						   $ipAddress->isMgmt = 0;
						   $ipAddress->save();				   				   					
					   }					
				   }
			   } else {
				   $ipAddress = new IPAddress();
				   $ipAddress->t_barangs_id = $barang->id;
				   $ipAddress->ip_address = $request->IPAddresses;
				   $ipAddress->isMgmt = 0;
				   $ipAddress->save();				   				   
			   }
			}	
            */        
            DB::commit();
            return $this->successStore($this->index, $this->pageTitle);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->failStore($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
		
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {

        $data = $data = Barang::with('Pengadaan', 'MerkBarang', 'ClusterBarang', 'JenisBarang',  'StatusBarang', 'Lokasi' )->get();
        return view('pages.master.barang.edit', [
            'pageTitle' => $this->pageTitle,
            'select' => [
                'lokasi' => Lokasi::all()->pluck('name', 'id')->prepend('Pilih Lokasi', ''),
                'pengadaan' => Pengadaan::all()->pluck('nama_pengadaan', 'id')->prepend('Pilih Pengadaan', ''),
                'jenis' => JenisBarang::all()->pluck('nama_jenis_barang', 'id')->prepend('Pilih Jenis Barang', ''),
                'cluster' => ClusterBarang::all()->pluck('nama_cluster', 'id')->prepend('Pilih Cluster', ''),
				'merk' => MerkBarang::all()->pluck('nama_merk', 'id')->prepend('Pilih Merk Perangkat', ''),
				'statusbarang' => StatusBarang::all()->pluck('desc_status', 'id_status')->prepend('Pilih Status Perangkat', '')
            ],
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'delete' => $this->delete
            ]
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        try {
            DB::beginTransaction();
            $barang->update([
                'nama_barang' => $request->nama_barang,
                'jenis_barang_id' => $request->jenis_barang_id,
                'merk_id' => $request->merk_id,
                'cluster_id' => $request->cluster_id,
				'tipe' => $request->tipe,
                'serial_number' => $request->serial_number,
                'mac_address' => $request->mac_address,
				'status_id' => $request->status_id,
				'lokasis_id' => $request->lokasis_id,
				'rack_position' => $request->PosisiRack,
				'rack_unit' => $request->UnitRack,
                'ip_address' => $request->IPAddresses,
                'pengadaans_id' => $request->pengadaans_id, 
                'garansi_awal' => empty($request->GaransiMulai) ? null : date("Y-m-d", strtotime($request->GaransiMulai)),
                'garansi_akhir' => empty($request->GaransiAkhir) ? null : date("Y-m-d", strtotime($request->GaransiAkhir)),				
            ]);

            // foreach ($request->IPAddress as $key => $value) {
                
            //     $ipAddress = new IPAddress();
            //     $dataIPAddress = $ipAddress::find($key);
            //     if($value == null) {
            //         $dataIPAddress->delete();
            //     }else{
            //         $dataIPAddress->ip_address = $value;
            //         $dataIPAddress->save();
            //     }
            // }
            /*
			$res = IPAddress::where(['t_barangs_id' => $request->barang_id])->delete();			
			if(isset($request->IPAddresses)) {
			   if(strpos($request->IPAddresses, ";")) {				   
				   $arrIPAddress = explode(";", $request->IPAddresses);
				   foreach($arrIPAddress as $cDetIPAddress)  {
					   if(strpos($cDetIPAddress, ",")) {
						   $arrDetIPAddress = explode(",", $cDetIPAddress); 
						   $cIPAddress = $arrDetIPAddress[0];
						   $ipAddress = new IPAddress();
						   $ipAddress->t_barangs_id = $barang->id;
						   $ipAddress->ip_address = $cIPAddress;
						   $ipAddress->isMgmt = 1;
						   $ipAddress->save();				   						   
					   } else {
						   $ipAddress = new IPAddress();
						   $ipAddress->t_barangs_id = $barang->id;
						   $ipAddress->ip_address = $cDetIPAddress;
						   $ipAddress->isMgmt = 0;
						   $ipAddress->save();				   				   					
					   }					
				   }
			   } else {
				   $ipAddress = new IPAddress();
				   $ipAddress->t_barangs_id = $barang->id;
				   $ipAddress->ip_address = $request->IPAddresses;
				   $ipAddress->isMgmt = 0;
				   $ipAddress->save();				   				   
			   }
			}
            */				
            DB::commit();
            return $this->successUpdate($this->index, $this->pageTitle);
        } catch (\Throwable $th) {
            DB::rollBack();
            \Log::info("message: ". $th->getMessage());
            return $this->failUpdate($this->pageTitle);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Barang $barang)
    {
        $barang->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
    
    /*
    public function tambahIPAddress(Request $request) {
        $data = IPAddress::where(['t_barangs_id' => $request->barang_id, 'ip_address' => $request->ip_address])->first();
        if($data == null)
        IPAddress::create([
            't_barangs_id' => $request->barang_id,
            'ip_address' => $request->ip_address
        ]);
        return $this->successStore($this->index, 'IP Address');

    }

    public function importMass(Request $request)
    {
        // dd($request->file('import'));
        // validasi
		
		// menangkap file excel
		$file = $request->file('import');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('barang',$nama_file);
 
		// import data
        try {
            Excel::import(new BarangImportFromSQL, public_path('/barang/'.$nama_file));
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
            $this->failStore($th->getMessage());
        }

    }
    */
}