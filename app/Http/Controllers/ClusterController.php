<?php

namespace App\Http\Controllers;

use App\Models\Cluster;
use Illuminate\Http\Request;

class ClusterController extends Controller
{

    public function __construct() {
        $this->view = 'cluster';
        $this->moduleName = 'virtual-machine.cluster';
        $this->index = 'master.'.$this->moduleName.'.index';
        $this->store = 'master.'.$this->moduleName.'.store';
        $this->edit = 'master.'.$this->moduleName.'.edit';
        $this->update = 'master.'.$this->moduleName.'.update';
        $this->destroy = 'master.'.$this->moduleName.'.destroy';
        $this->route = [
            'index' => $this->index,
            'store' => $this->store,
            'edit' => $this->edit,
            'update' => $this->update,
            'delete' => $this->destroy,
        ];
        $this->pageTitle = 'Cluster';
        $this->model = new Cluster();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->get();
        return view('pages.'.$this->moduleName.'.index', [
            'data' => $data,
            'route' => $this->route,
            'pageTitle' => $this->pageTitle
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cluster $cluster)
    {
        $cluster->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cluster $cluster)
    {
        $cluster->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
