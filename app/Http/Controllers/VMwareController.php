<?php

namespace App\Http\Controllers;

use App\Imports\VMWareImportFromSQL;
use App\Models\VMware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class VMwareController extends Controller
{

    public function __construct() {
        $this->view = 'vmware';
        $this->index = 'master.hypervisor.'.$this->view.'.index';
        $this->store = 'master.hypervisor.'.$this->view.'.store';
        $this->edit = 'master.hypervisor.'.$this->view.'.edit';
        $this->update = 'master.hypervisor.'.$this->view.'.update';
        $this->delete = 'master.hypervisor.'.$this->view.'.destroy';
        $this->pageTitle = ucfirst($this->view);
        $this->model = new VMware();

        // $this->isAdmin();
        
    }

    // private function isAdmin()
    // {
    //     $request = new Request();
    //     dd( request()->segment(1));
    //     if ($request->segment(1) != 'master') {
    //         dd('die');
    //     }
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        VMware::create([
            'hostname' => $request->hostname,
            'guest_os' => $request->guest_os,
            'state' => $request->state,
            'IP_address' => $request->ip_address,
            'memory_size' => $request->memory_size,
            'cpus' => $request->cpus,
            'provisioned_space' => $request->provisioned_space,
            'used_space' => $request->used_space,
        ]);
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VMware  $vMware
     * @return \Illuminate\Http\Response
     */
    public function show(VMware $vMware)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VMware  $vMware
     * @return \Illuminate\Http\Response
     */
    public function edit(VMware $vMware, $id)
    {
        $data = $vMware::find($id);
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VMware  $vMware
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VMware $vMware, $id)
    {
        $data = $vMware::find($id);
        $data->hostname = $request->hostname;
        $data->guest_os = $request->guest_os;
        $data->state = $request->state;
        $data->IP_address = $request->ip_address;
        $data->memory_size = $request->memory_size;
        $data->cpus = $request->cpus;
        $data->provisioned_space = $request->provisioned_space;
        $data->used_space = $request->used_space;
        $data->save();
        return $this->successUpdate($this->index, $this->pageTitle);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VMware  $vMware
     * @return \Illuminate\Http\Response
     */
    public function destroy(VMware $vMware, $id)
    {
        $data = $vMware::find($id);
        $data->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }

    public function importMass(Request $request)
    {
        // dd($request->file('import'));
        // validasi
		

		// menangkap file excel
		$file = $request->file('import');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('vmware',$nama_file);
		// import data
        try {
            Excel::import(new VMWareImportFromSQL, public_path('/vmware/'.$nama_file));
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
            $this->failStore($th->getMessage());
        }

    }
}
