<?php

namespace App\Http\Controllers;

use App\Models\JenisVirtualMachine;
use App\Models\VirtualMachine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VirtualMachineController extends Controller
{

    public function __construct() {
        $this->view = 'vm';
        $this->moduleName = 'virtual-machine.'.$this->view;
        $this->index = 'master.'.$this->moduleName.'.index';
        $this->store = 'master.'.$this->moduleName.'.store';
        $this->edit = 'master.'.$this->moduleName.'.edit';
        $this->update = 'master.'.$this->moduleName.'.update';
        $this->destroy = 'master.'.$this->moduleName.'.destroy';
        $this->route = [
            'index' => $this->index,
            'store' => $this->store,
            'edit' => $this->edit,
            'update' => $this->update,
            'delete' => $this->destroy,
        ];
        $this->pageTitle = 'Virtual Machine';
        $this->model = new VirtualMachine();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->get();
        return view('pages.'.$this->moduleName.'.index', [
            'data' => $data,
            'route' => $this->route,
            'pageTitle' => $this->pageTitle
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['ip_address'] = $request->ip_address;
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VirtualMachine  $virtualMachine
     * @return \Illuminate\Http\Response
     */
    public function show(VirtualMachine $virtualMachine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VirtualMachine  $virtualMachine
     * @return \Illuminate\Http\Response
     */
    public function edit(VirtualMachine $virtualMachine)
    {
        $data = $this->model::get();
        return view('pages.virtualmachine.edit', [
            'data' => $virtualMachine,
            'table' => $data,
            'select' => [
                'jenis' => JenisVirtualMachine::pluck('nama', 'id')->prepend('Pilih Virtual Machine', '')
            ],
            'pageTitle' => $this->pageTitle,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VirtualMachine  $virtualMachine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VirtualMachine $virtualMachine, $id)
    {
        // $meta = array_filter(array_combine($request->key, $request->value), function($value, $key) {
        //     return $key == "" || $value == "" ? false : $value;
        // }, ARRAY_FILTER_USE_BOTH );
        // try {
        //     $virtualMachine->update([
        //         'mac_address' => $request->mac_address,
        //         'used_spaced' => $request->used_spaced,
        //         "nama_vm" => $request->nama_vm,
        //         "deskripsi" => $request->deskripsi,
        //         "ip_address" => $request->ip_address,
        //         "jenis_virtual_machines_id" => $request->jenis_virtual_machines_id,
        //         "core" => $request->core,
        //         "kapasitas_memori" => $request->kapasitas_memori,
        //         "space_hdd" => $request->space_hdd,
        //         "status_power" => $request->status_power,
        //         "os" => $request->os,
        //         "cluster_id" => $request->cluster_id,
        //     ]);
        //     return $this->successUpdate($this->index, $this->pageTitle);
        // } catch (\Throwable $th) {
        //     Log::info('message: ', $th->getMessage());
        //     return $this->failUpdate($this->index, $this->pageTitle);
        // }
        $virtualMachine = $virtualMachine::find($id);
        $virtualMachine->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VirtualMachine  $virtualMachine
     * @return \Illuminate\Http\Response
     */
    public function destroy(VirtualMachine $virtualMachine, $id)
    {
        $virtualMachine = $virtualMachine::find($id);
        $virtualMachine->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
