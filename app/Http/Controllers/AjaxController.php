<?php

namespace App\Http\Controllers;

use App\Models\IPAddress;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function setIPMgmt(Request $request)
    {
        $barangIPAddress = new IPAddress();
        $barangIPAddress = $barangIPAddress::where('ip_address', $request->IP_address)->first();
        $barangIPAddress->isMgmt = $barangIPAddress->isMgmt == 0 ? 1 : 0;
        $barangIPAddress->save();
        $message = $barangIPAddress->isMgmt == 1 ? 'Manajemen' : 'Address';
        return array('success' => true, 'message' => 'IP '.$request->IP_address.' berhasil dijadikan IP '.$message);
    }
}
