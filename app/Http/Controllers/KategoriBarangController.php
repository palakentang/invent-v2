<?php

namespace App\Http\Controllers;

use App\Http\Requests\KategoriBarangRequest;
use App\Models\KategoriBarang;
use Illuminate\Http\Request;

class KategoriBarangController extends Controller
{
    public function __construct() {
        $this->view = 'kategori';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Jenis '.ucfirst($this->view);
        $this->model = new KategoriBarang();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->model->create($request->except('_token'));
            return $this->successStore($this->index, $this->pageTitle);
        } catch (\Throwable $th) {
            return $this->failStore($th->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KategoriBarang  $kategoriBarang
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriBarang $kategoriBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KategoriBarang  $kategoriBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriBarang $kategoriBarang, $id)
    {
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $kategoriBarang::find($id),
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KategoriBarang  $kategoriBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KategoriBarang $kategoriBarang, $id)
    {
        $kategoriBarang = $kategoriBarang::find($id);
        $kategoriBarang->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KategoriBarang  $kategoriBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriBarang $kategoriBarang, $id)
    {
        $kategoriBarang = $kategoriBarang::find($id);
        $kategoriBarang->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
