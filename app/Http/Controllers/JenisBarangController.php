<?php

namespace App\Http\Controllers;

use App\Http\Requests\JenisBarangRequest;
use App\Models\JenisBarang;
use Illuminate\Http\Request;

class JenisBarangController extends Controller
{
    public function __construct() {
        $this->index = 'master.jenis.index';
        $this->store = 'master.jenis.store';
        $this->update = 'master.jenis.update';
        $this->pageTitle = 'Jenis Barang';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenisBarang = new JenisBarang();
        $data = $jenisBarang::get();
        return view('pages.master.jenis.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JenisBarangRequest $request, JenisBarang $jenisBarang)
    {
        $jenisBarang::create($request->validated());
        return redirect()->route($this->index)->with('success', 'Berhasil Menambahkan '.$this->pageTitle);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function show(JenisBarang $jenisBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisBarang $jenisBarang, $id)
    {
        $jenisBarang = new JenisBarang();
        $data = $jenisBarang::findOrFail($id);
        return view('pages.master.jenis.edit', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => $this->update
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JenisBarang $jenisBarang, $id)
    {
        $jenisBarang::updateOrCreate([
            'id' => $id
        ], $request->except('_token'));
        return redirect()->route($this->index)->with('success', 'Berhasil Ubah '.$this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisBarang $jenisBarang, $id)
    {
        $data = $jenisBarang::findOrFail($id);
        $data->delete();
        return redirect()->route($this->index)->with('success', 'Berhasil Menghapus '.$this->pageTitle);
    }

}
