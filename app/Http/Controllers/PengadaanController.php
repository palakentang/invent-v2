<?php

namespace App\Http\Controllers;

use App\Http\Requests\PengadaanRequest;
use App\Models\Pengadaan;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;

class PengadaanController extends Controller
{

    public function __construct() {
        $this->view = 'pengadaan';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Jenis '.ucfirst($this->view);
        $this->model = new Pengadaan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengadaanRequest $request, Pengadaan $pengadaan)
    {
        $pengadaan->create($request->validated());
        return redirect()->route($this->index)->with('success', 'Berhasil Menambahkan '.$this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function show(Pengadaan $pengadaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengadaan $pengadaan)
    {
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $pengadaan,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function update(PengadaanRequest $request, Pengadaan $pengadaan)
    {
        $pengadaan->update($request->validated());
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengadaan $pengadaan)
    {
        $pengadaan->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
