<?php

namespace App\Http\Controllers;

use App\Http\Requests\LokasiRequest;
use App\Models\Lokasi;
use Illuminate\Http\Request;

class LokasiController extends Controller
{
    public function __construct() {
        $this->view = 'lokasi';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = ucfirst($this->view);
        $this->model = new Lokasi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LokasiRequest $request)
    {
		/***
        $lokasi::create($request->validated());
		*/
		$this->validate($request, [
			'name' => 'required',
			'deskripsi' => 'required'
		]);		
		$lokasi = new Lokasi();
		//On left field name in DB and on right field name in Form/view
		$lokasi->nama_lokasi = $request->input('name');
		$lokasi->detail_lokasi = $request->input('deskripsi');
		$lokasi->save();		
        return redirect()->route($this->index)->with('success', 'Berhasil Menambahkan Lokasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lokasi  $lokasi
     * @return \Illuminate\Http\Response
     */
    public function show(Lokasi $lokasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lokasi  $lokasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Lokasi $lokasi)
    {
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $lokasi,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lokasi  $lokasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lokasi $lokasi)
    {
		$lokasi = Lokasi::find($lokasi->id);
		$lokasi->nama_lokasi = $request->input('nama');
		$lokasi->detail_lokasi = $request->input('deskripsi');
        $lokasi->update();
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lokasi  $lokasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lokasi $lokasi)
    {
        $lokasi->delete();
        return redirect()->route($this->index)->with('success', 'Berhasil Menghapus Lokasi');
    }
}
