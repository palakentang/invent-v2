<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successStore($route, $module)
    {
        return redirect()->route($route)->with('success', 'Berhasil Menambahkan '.$module);
    }

    public function failStore($message)
    {
        return redirect()->back()->with('error', 'Gagal Menambahkan '.$message);
    }

    public function successUpdate($route, $module)
    {
        return redirect()->route($route)->with('success', 'Berhasil Ubah '.$module);
    }

    public function failUpdate($message)
    {
        return redirect()->back()->with('error', 'Gagal Menambahkan '.$message);
    }

    public function successDelete($route, $module)
    {
        return redirect()->route($route)->with('success', 'Berhasil Hapus '.$module);
    }

}
