<?php

namespace App\Http\Controllers;

use App\Models\JenisVirtualMachine;
use Illuminate\Http\Request;

class JenisVirtualMachineController extends Controller
{

    public function __construct() {
        $this->view = 'jenis virtual machine';
        $this->index = 'master.jenis-virtual-machine.index';
        $this->store = 'master.jenis-virtual-machine.store';
        $this->edit = 'master.jenis-virtual-machine.edit';
        $this->update = 'master.jenis-virtual-machine.update';
        $this->delete = 'master.jenis-virtual-machine.destroy';
        $this->pageTitle = ucfirst($this->view);
        $this->model = new JenisVirtualMachine();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.virtualmachine.jenis.index', [
            'data' => $data,
            'pageTitle' => $this->pageTitle,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'delete' => $this->delete
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisVirtualMachine  $jenisVirtualMachine
     * @return \Illuminate\Http\Response
     */
    public function show(JenisVirtualMachine $jenisVirtualMachine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisVirtualMachine  $jenisVirtualMachine
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisVirtualMachine $jenisVirtualMachine)
    {
        $data = $jenisVirtualMachine;
        return view('pages.virtualmachine.jenis.edit', [
            'data' => $data,
            'table' => $this->model::get(),
            'pageTitle' => $this->pageTitle,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisVirtualMachine  $jenisVirtualMachine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JenisVirtualMachine $jenisVirtualMachine)
    {
        $jenisVirtualMachine->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisVirtualMachine  $jenisVirtualMachine
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisVirtualMachine $jenisVirtualMachine)
    {
        $jenisVirtualMachine->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
