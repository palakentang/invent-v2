<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatusBarangRequest;
use App\Models\StatusBarang;
use Illuminate\Http\Request;

class StatusBarangController extends Controller
{
    public function __construct() {
        $this->view = 'statusbarang';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Status Perangkat';
        $this->model = new StatusBarang();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatusBarang $statusbarang, Request $request)
    {
        $statusbarang->create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusBarang  $statusbarang
     * @return \Illuminate\Http\Response
     */
    public function show(StatusBarang $statusbarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Status  $statusbarang
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusBarang $statusbarang)
    {
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $statusbarang,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatusBarang  $statusbarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusBarang $statusbarang)
    {
        $statusbarang->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatusBarang  $statusbarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusBarang $statusbarang )
    {
        $statusbarang->delete();
		return $this->successDelete($this->index, $this->pageTitle);
    }
}
