<?php

namespace App\Http\Controllers;

use App\Models\ClusterSystem;
use Illuminate\Http\Request;

class ClusterSystemController extends Controller
{
    public function __construct() {
        $this->view = 'clustersystem';
        $this->moduleName = 'virtual-machine.cluster-system';
        $this->index = 'master.'.$this->moduleName.'.index';
        $this->store = 'master.'.$this->moduleName.'.store';
        $this->edit = 'master.'.$this->moduleName.'.edit';
        $this->update = 'master.'.$this->moduleName.'.update';
        $this->destroy = 'master.'.$this->moduleName.'.destroy';
        $this->route = [
            'index' => $this->index,
            'store' => $this->store,
            'edit' => $this->edit,
            'update' => $this->update,
            'delete' => $this->destroy,
        ];
        $this->pageTitle = 'Cluster System';
        $this->model = new ClusterSystem();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.'.$this->moduleName.'.index', [
            'data' => $data,
            'route' => $this->route,
            'pageTitle' => $this->pageTitle,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClusterSystem  $clusterSystem
     * @return \Illuminate\Http\Response
     */
    public function show(ClusterSystem $clusterSystem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClusterSystem  $clusterSystem
     * @return \Illuminate\Http\Response
     */
    public function edit(ClusterSystem $clusterSystem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClusterSystem  $clusterSystem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClusterSystem $clusterSystem)
    {
        $clusterSystem->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClusterSystem  $clusterSystem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClusterSystem $clusterSystem)
    {
        $clusterSystem->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }
}
