<?php

namespace App\Http\Controllers;

use App\Models\PowerVM;
use Illuminate\Http\Request;

class PowerVMController extends Controller
{
    public function __construct() {
        $this->view = 'powervm';
        $this->index = 'master.hypervisor.'.$this->view.'.index';
        $this->store = 'master.hypervisor.'.$this->view.'.store';
        $this->edit = 'master.hypervisor.'.$this->view.'.edit';
        $this->update = 'master.hypervisor.'.$this->view.'.update';
        $this->delete = 'master.hypervisor.'.$this->view.'.destroy';
        $this->pageTitle = ucfirst($this->view);
        $this->model = new PowerVM();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        $kode = $this->model->KodePowerVM('kd_power_vm', 'HOST ');
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'kode' => $kode,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PowerVM  $powerVM
     * @return \Illuminate\Http\Response
     */
    public function show(PowerVM $powerVM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PowerVM  $powerVM
     * @return \Illuminate\Http\Response
     */
    public function edit(PowerVM $powerVM, $id)
    {
        $powerVM = $powerVM::find($id);
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $powerVM,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PowerVM  $powerVM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PowerVM $powerVM, $id)
    {
        $powerVM = $powerVM::find($id);
        $powerVM->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PowerVM  $powerVM
     * @return \Illuminate\Http\Response
     */
    public function destroy(PowerVM $powerVM)
    {
        //
    }
}
