<?php

namespace App\Http\Controllers;

use App\Models\OS;
use App\Models\IPAddress;
use Illuminate\Http\Request;

class OSController extends Controller
{
    public function __construct() {
        $this->view = 'os';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->destroy = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Daftar '.$this->view;

        $this->route = [
            'index' => $this->index,
            'store' => $this->store,
            'edit' => $this->edit,
            'update' => $this->update,
            'delete' => $this->destroy,
        ];

        $this->model = new OS();
    }

    public function index()
    {
        return view('pages.master.os.index', [
            'pageTitle' => $this->pageTitle,
            'route' => $this->route,
            'data' => OS::get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VirtualMachine  $virtualMachine
     * @return \Illuminate\Http\Response
     */
    public function edit(OS $os, $id)
    {
        $os = $os::find($id);
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $os,
            'datas' => OS::get(),
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->destroy,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lokasi  $lokasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OS $os)
    {
		$os = OS::find($request->id);
		$os->nama_os = $request->input('nama_os');
		$os->deskripsi = $request->input('deskripsi');
        $os->update();
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->model::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengadaan  $pengadaan
     * @return \Illuminate\Http\Response
     */
    public function show(OS $os)
    {
        //
    }

    public function destroy($id)
    {
        $os = OS::find($id);
        $os->delete();
        return redirect()->route($this->index)->with('success', 'Berhasil Menghapus OS');
    }
}
