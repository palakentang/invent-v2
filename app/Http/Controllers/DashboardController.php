<?php

namespace App\Http\Controllers;

use App\Constant\JenisVM;
use App\Models\Barang;
use App\Models\Cluster;
use App\Models\ClusterSystem;
use App\Models\JenisBarang;
use App\Models\JenisVirtualMachine;
use App\Models\Lokasi;
use App\Models\VirtualMachine;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct() {
        $this->view = 'barang';
        $this->index = 'master.'.$this->view.'.index';
        $this->store = 'master.'.$this->view.'.store';
        $this->edit = 'master.'.$this->view.'.edit';
        $this->update = 'master.'.$this->view.'.update';
        $this->delete = 'master.'.$this->view.'.destroy';
        $this->pageTitle = 'Master '.ucfirst($this->view);
        $this->model = new Barang();
    }

    public function index()
    {
        $pageTitle = 'Dashboard';
        $data = Barang::with('Pengadaan')->get();
		$nTotalPerangkat = Barang::count();
		$nTotalPerangkatHabisGaransi = Barang::where('garansi_akhir', '<', 'NOW()')->count();
		$nTotalVirtualMachine = VirtualMachine::count();
		
        $merk['IBM'] = Barang::where('nama_barang', 'like', '%IBM%')->count();
        $merk['LENOVO'] = Barang::where('nama_barang', 'like', '%Lenovo%')->count();
        $merk['NUTANIX'] = Barang::where('nama_barang', 'like', '%Nutanix%')->count();
        $merk['OTHER'] = $data->count() - $merk['IBM'] - $merk['LENOVO'] - $merk['NUTANIX'];

        $jenis_vm['power_vm'] = VirtualMachine::where('cluster_id', JenisVM::PowerVM())->count();
        $jenis_vm['vm_ware'] = VirtualMachine::where('cluster_id', JenisVM::VMWare())->count();
        $jenis_vm['nutanix'] = VirtualMachine::where('cluster_id', JenisVM::Nutanix())->count();

        $clusterSystems = ClusterSystem::with('childrenClusters')->get();
        foreach ($clusterSystems as $clusterSystem) {
            $dataVM[str_replace(' ', '_', $clusterSystem->name)] = $clusterSystem->childrenClusters->count();

            if ($clusterSystem->childrenClusters->count() > 0) {
                foreach ($clusterSystem->childrenClusters as $cluster) {
                    if ($cluster->childrenNodes->count() > 0) {
                        foreach ($cluster->childrenNodes as $node) {
                            $dataVM[str_replace(' ', '_', $clusterSystem->name)] = array(
                                str_replace(' ', '_', $cluster->name) => [
                                    $node->node_name => $node->VirtualMachines
                                ]
                            );
                        }
                    }

                    // $cluster[$cluster->name] = $cluster->childrenNodes->count();
                    if ($cluster->childrenNodes->count() > 0) {

                    }
                }
            }
        }
// dd($dataVM);
        return view('pages.dashboard', [
            'count' => [
                'lokasi' => Lokasi::get(),
                'jenis' => [
                    'name' => JenisBarang::pluck('nama_jenis_barang')->toArray(),
                    'quantity' => JenisBarang::get(),
                ],
                'clusterSystem' => [
                    'name' => ClusterSystem::pluck('name')->toArray(),
                    'quantity' => ClusterSystem::get()
                ],
                'virtualMachine' => [
                    'name' => VirtualMachine::pluck('name')->toArray(),
                    'quantity' => VirtualMachine::get()
                ]
            ],
            'pageTitle' => $pageTitle,
			'nTotalPerangkat' => $nTotalPerangkat,
			'nTotalVirtualMachine' => $nTotalVirtualMachine,
			'nTotalPerangkatHabisGaransi' => $nTotalPerangkatHabisGaransi,
            'data' => $data,
            'merk' => $merk,
            'jenis_vm' => $jenis_vm,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'delete' => $this->delete
            ]
        ]);
    }

}
