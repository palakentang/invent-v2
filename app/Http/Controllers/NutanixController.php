<?php

namespace App\Http\Controllers;

use App\Models\Nutanix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\NutanixImportFromSQL;

class NutanixController extends Controller
{
    public function __construct() {
        $this->view = 'nutanix';
        $this->index = 'master.hypervisor.'.$this->view.'.index';
        $this->store = 'master.hypervisor.'.$this->view.'.store';
        $this->edit = 'master.hypervisor.'.$this->view.'.edit';
        $this->update = 'master.hypervisor.'.$this->view.'.update';
        $this->delete = 'master.hypervisor.'.$this->view.'.destroy';
        $this->pageTitle = ucfirst($this->view);
        $this->model = new Nutanix();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model::get();
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nutanix = new Nutanix();
        $nutanix::create($request->except('_token'));
        return $this->successStore($this->index, $this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nutanix  $nutanix
     * @return \Illuminate\Http\Response
     */
    public function show(Nutanix $nutanix)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nutanix  $nutanix
     * @return \Illuminate\Http\Response
     */
    public function edit(Nutanix $nutanix)
    {
        return view('pages.master.'.$this->view.'.edit', [
            'pageTitle' => $this->pageTitle,
            'table' => $this->model::get(),
            'data' => $nutanix,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Nutanix  $nutanix
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nutanix $nutanix)
    {
        $nutanix->update($request->except('_token'));
        return $this->successUpdate($this->index, $this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Nutanix  $nutanix
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nutanix $nutanix)
    {
        $nutanix->delete();
        return $this->successDelete($this->index, $this->pageTitle);
    }

    public function importMass(Request $request)
    {
        // dd($request->file('import'));
        // validasi
		

		// menangkap file excel
		$file = $request->file('import');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('nutanix',$nama_file);
		// import data
        try {
            Excel::import(new NutanixImportFromSQL, public_path('/nutanix/'.$nama_file));
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
            $this->failStore($th->getMessage());
        }

    }
}
