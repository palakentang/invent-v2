<?php

namespace App\Http\Controllers;

use App\Http\Requests\MerkBarangRequest;
use App\Models\MerkBarang;
use Illuminate\Http\Request;

class MerkBarangController extends Controller
{
    public function __construct() {
        $this->index = 'master.merk.index';
        $this->store = 'master.merk.store';
        $this->update = 'master.merk.update';
        $this->pageTitle = 'Merk Barang';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merkBarang = new MerkBarang();
        $data = $merkBarang::get();
        return view('pages.master.merk.index', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MerkBarangRequest $request, MerkBarang $merkBarang)
    {
        $jenisBarang::create($request->validated());
        return redirect()->route($this->index)->with('success', 'Berhasil Menambahkan '.$this->pageTitle);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function show(MerkBarang $merkBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(MerkBarang $merkBarang, $id)
    {
        $merkBarang = new MerkBarang();
        $data = $merkBarang::findOrFail($id);
        return view('pages.master.merk.edit', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => $this->update
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MerkBarang $merkBarang, $id)
    {
        $jenisBarang::updateOrCreate([
            'id' => $id
        ], $request->except('_token'));
        return redirect()->route($this->index)->with('success', 'Berhasil Ubah '.$this->pageTitle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisBarang  $jenisBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(MerkBarang $merkBarang, $id)
    {
        $data = $merkBarang::findOrFail($id);
        $data->delete();
        return redirect()->route($this->index)->with('success', 'Berhasil Menghapus '.$this->pageTitle);
    }

}
