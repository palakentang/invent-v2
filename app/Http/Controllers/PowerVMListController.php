<?php

namespace App\Http\Controllers;

use App\Models\PowerVM;
use App\Models\PowerVMList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PowerVMListImportFromSQL;

class PowerVMListController extends Controller
{

    public function __construct() {
        $this->view = 'powervmlists';
        $this->index = 'master.hypervisor.powervm.lists.index';
        $this->store = 'master.hypervisor.powervm.lists.store';
        $this->edit = 'master.hypervisor.powervm.lists.edit';
        $this->update = 'master.hypervisor.powervm.lists.update';
        $this->delete = 'master.hypervisor.powervm.lists.destroy';
        $this->pageTitle = 'List PowerVM';
        $this->model = new PowerVMList();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = $this->model::where('power_vm_id', $id)->with('PowerVM')->get();
        $powerVM = PowerVM::find($id);
        return view('pages.master.'.$this->view.'.index', [
            'pageTitle' => $powerVM->kd_power_vm,
            'id_power_vm' => $powerVM->id,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request['power_vm_id'] = $id;
        PowerVMList::create($request->except('_token'));
        return redirect()->route($this->index, $id)->with('success', 'Berhasil Menambahkan '.$this->pageTitle);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PowerVMList  $powerVMList
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = PowerVM::get();
        return view('pages.master.powervmlists.lists', [
            'pageTitle' => $this->pageTitle,
            'data' => $data,
            'route' => [
                'store' => $this->store,
                'edit' => $this->edit,
                'update' => $this->update,
                'delete' => $this->delete,
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PowerVMList  $powerVMList
     * @return \Illuminate\Http\Response
     */
    public function edit(PowerVMList $powerVMList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PowerVMList  $powerVMList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PowerVMList $powerVMList)
    {
        $powerVMList = $powerVMList::find($request->segment(6));
        $powerVMList->update($request->except('_token'));
        return redirect()->route($this->index, $request->segment(4))->with('success', 'Berhasil Ubah '.$this->pageTitle);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PowerVMList  $powerVMList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PowerVMList $powerVMList, Request $request)
    {
        $data = $powerVMList::find($request->segment(6));
        $data->delete();
        return redirect()->route($this->index, $request->segment(4))->with('success', 'Berhasil Menambahkan '.$this->pageTitle);
    }

    public function importMass(Request $request)
    {
        // dd($request->file('import'));
        // validasi
		

		// menangkap file excel
		$file = $request->file('import');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('powervmlist',$nama_file);
		// import data
        try {
            Excel::import(new PowerVMListImportFromSQL, public_path('/powervmlist/'.$nama_file));
        } catch (\Throwable $th) {
            Log::info($th->getMessage());
            $this->failStore($th->getMessage());
        }

    }
}
