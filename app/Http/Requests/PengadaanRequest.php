<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PengadaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pengadaan' => 'required',
            'tgl_pengadaan' => 'required'
        ];
    }
    
    /**
     * Get the messages rules that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            'nama_pengadaan.required' => ':attribute Mohon Diisi',
            'tgl_pengadaan.required' => ':attribute Mohon Diisi',
        ];
    }
    
    /**
     * Set the :attribute name of rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'nama_pengadaan' => 'Nama Pengadaan',
            'tgl_pengadaan' => 'Tanggal Pengadaan',
        ];
    }
}
