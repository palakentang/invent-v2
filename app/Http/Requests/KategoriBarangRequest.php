<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KategoriBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kd_kategori_barang' => 'required|unique:kategori_barang,kd_kategori_barang',
            'nama'  => 'required|unique:kategori_barang,nama'
        ];
    }

    /**
     * Get the messages rules that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            'kd_kategori_barang.unique' => ':attribute Sudah Ada',
            'nama.required' => ':attribute Mohon Diisi',
            'nama.unique'   => ':attribute Sudah Ada'
        ];
    }
    
    /**
     * Set the :attribute name of rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'kd_kategori_barang' => 'Kode Kategori Barang',
            'nama' => 'Kategori Barang',
        ];
    }
}
