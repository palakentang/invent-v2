<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LokasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'desc_status' => 'required'
        ];
    }

    /**
     * Get the messages rules that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            'desc_status.required' => ':attribute Mohon Diisi',
        ];
    }
    
    /**
     * Set the :attribute name of rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'desc_status' => 'Status Perangkat',
        ];
    }
}
