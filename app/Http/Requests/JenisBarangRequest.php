<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JenisBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_jenis_barang' => 'required|unique:jenis_barang,nama_jenis_barang'
        ];
    }
    
    /**
     * Get the messages rules that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            'nama_jenis_barang.required' => ':attribute Mohon Diisi',
            'nama_jenis_barang.unique'   => ':attribute Sudah Ada',
        ];
    }
    
    /**
     * Set the :attribute name of rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'nama_jenis_barang' => 'Jenis Barang',
        ];
    }
}
