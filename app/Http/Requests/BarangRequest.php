<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_barang' => 'required',
            'jenis_barang_id' => 'required',
            'lokasis_id' => 'required',
        ];
    }

    /**
     * Get the messages rules that apply to the request.
     *
     * @return array
     */
    public function message()
    {
        return [
            'nama_barang.required' => ':attribute Mohon Diisi',
            'jenis_barang_id.required' => ':attribute Mohon Diisi',
            'pengadaans_id.required' => ':attribute Mohon Diisi',
            'lokasis_id.required' => ':attribute Mohon Diisi',
        ];
    }
    
    /**
     * Set the :attribute name of rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'nama_barang' => 'Nama Barang',
            'jenis_barang_id' => 'Jenis Barang',
            'lokasis_id' => 'Lokasi',
        ];
    }
}
