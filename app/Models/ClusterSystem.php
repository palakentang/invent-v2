<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ClusterSystem extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'cluster_systems';

    /**
     * Get all of the Clusters for the ClusterSystem
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Clusters(): HasMany
    {
        return $this->hasMany(Cluster::class);
    }

    /**
     * Get all of the ClustersChildren for the ClusterSystem
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenClusters(): HasMany
    {
        return $this->hasMany(Cluster::class)->withCount('Nodes');
    }

}
