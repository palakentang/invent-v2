<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Services\AutoIncrementServices;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JenisBarang extends Model
{
    use HasFactory;
    protected $table = 'jenis_barang';
    protected $primaryKey = 'id';
    protected $guarded = [];

    /**
     * Get all of the Barangs for the JenisBarang
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Barangs(): HasMany
    {
        return $this->hasMany(Barang::class);
    }
    
}
