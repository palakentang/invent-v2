<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Lokasi extends Model
{
    use HasFactory;
    protected $table = 'lokasis';
	protected $primaryKey = 'id';
    protected $guarded = [];

    public function KodeLokasiLast($field, $prefix)
    {
        $autoIncrementServices = new AutoIncrementServices();
        count($this::all()) == 0 ? $model = 0 : $model = $this->latest($field)->first()->$field;
        $kd_lokasi = $autoIncrementServices->handleIncrement(['data' => $model, 'prefix' => $prefix.'-', 'length' => 4]);
        return $kd_lokasi;
    }

    /**
     * Get all of the Baranag for the Lokasi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Barangs(): HasMany
    {
        return $this->hasMany(Barang::class, 'lokasis_id');
    }
}
