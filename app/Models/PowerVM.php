<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Services\AutoIncrementServices;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PowerVM extends Model
{
    use HasFactory;
    protected $table = 'power_vm';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function KodePowerVM($field, $prefix)
    {
        $autoIncrementServices = new AutoIncrementServices();
        count($this::all()) == 0 ? $model = 0 : $model = $this->latest($field)->first()->$field;
        $kode = $autoIncrementServices->handleIncrementOther(['data' => $model, 'prefix' => $prefix.'#', 'length' => 6]);
        return $kode;
    }

    public function namaVM(): string
    {
        return $this->vm_name.' '.$this->kd_power_vm.' / '.$this->vm_hostname.' / '.$this->vm_ip;
    }

    /**
     * Get all of the PowerVMList for the PowerVM
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Lists(): HasMany
    {
        return $this->hasMany(PowerVMList::class, 'power_vm_id');
    }
}
