<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Barang extends Model
{
    use HasFactory;
    protected $table = 't_barangs';
    protected $guarded = [];
    

    /**
     * Get the JenisBarang that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function JenisBarang(): BelongsTo
    {
        return $this->belongsTo(JenisBarang::class, 'jenis_barang_id');
    }

    /**
     * Get merk that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function MerkBarang(): BelongsTo
    {
        return $this->belongsTo(MerkBarang::class, 'merk_id');
    }

    /**
     * Get StatusBarang that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function StatusBarang(): BelongsTo
    {
        return $this->belongsTo(StatusBarang::class, 'status_id');
    }

     /**
     * Get the Cluster that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ClusterBarang(): BelongsTo
    {
        return $this->belongsTo(ClusterBarang::class, 'cluster_id');
    }
   
    /**
     * Get the Pengadaan that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Pengadaan(): BelongsTo
    {
        return $this->belongsTo(Pengadaan::class, 'pengadaans_id');
    }

    /**
     * Get the Pengadaan that owns the Barang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Lokasi(): BelongsTo
    {
        return $this->belongsTo(Lokasi::class, 'lokasis_id');
    }

}
