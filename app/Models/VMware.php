<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VMware extends Model
{
    use HasFactory;
    protected $table = 'vm_ware';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
