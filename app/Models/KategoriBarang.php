<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Services\AutoIncrementServices;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KategoriBarang extends Model
{
    use HasFactory;
    protected $table = 'kategori_barang';
    protected $primaryKey = 'id';
    protected $guarded = [];

    /**
     * Get all of the Barangs for the KategoriBarang
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Barangs(): HasMany
    {
        return $this->hasMany(Barang::class);
    }

}
