<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OS extends Model
{
    use HasFactory;
    protected $table = 'os';
    protected $guarded = [];
    protected $primaryKey = 'id';

    /**
     * Get all of the Baranag for the Lokasi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VirtualMachines(): HasMany
    {
        return $this->hasMany(VirtualMachine::class, 'OS');
    }
    

}
