<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusBarang extends Model
{
    use HasFactory;
    protected $table = 'status_perangkat';
	protected $primaryKey = 'id_status';
    protected $guarded = [];
}
