<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class IPAddress extends Model
{
    use HasFactory;
    protected $table = 'ip_addresses';
    protected $guarded = [];

    /**
     * Get the Barang that owns the IPAddress
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Barang(): BelongsTo
    {
        return $this->belongsTo(Barang::class, 't_barangs_id');
    }
}
