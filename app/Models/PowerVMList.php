<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PowerVMList extends Model
{
    use HasFactory;
    protected $table = 'power_vm_list';
    protected $guarded = [];

    /**
     * Get the PowerVM that owns the PowerVMList
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function PowerVM(): BelongsTo
    {
        return $this->belongsTo(PowerVM::class, 'power_vm_id');
    }
}
