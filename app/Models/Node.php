<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Node extends Model
{
    use HasFactory;
    protected $table = 'nodes';
    protected $guarded = [];

    /**
     * Get the Cluster that owns the Node
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Cluster(): BelongsTo
    {
        return $this->belongsTo(Cluster::class);
    }

    /**
     * Get all of the VirtualMachines for the Node
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VirtualMachines(): HasMany
    {
        return $this->hasMany(VirtualMachine::class);
    }
}
