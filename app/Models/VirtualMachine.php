<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VirtualMachine extends Model
{
    use HasFactory;
    protected $table = 'virtual_machines';
    protected $guarded = [];
    protected $primaryKey = 'id';

    /**
     * Get the Jenis that owns the VirtualMachine
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Jenis(): BelongsTo
    {
        return $this->belongsTo(JenisVirtualMachine::class, 'jenis_virtual_machines_id');
    }

    /**
     * Get the Cluster that owns the VirtualMachine
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Cluster(): BelongsTo
    {
        return $this->belongsTo(Cluster::class, 'cluster_id');
    }

    /**
     * Get the Cluster that owns the VirtualMachine
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function OSS(): BelongsTo
    {
        return $this->belongsTo(OS::class, 'OS');
    }

    /**
     * Get the Node that owns the VirtualMachine
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Node(): BelongsTo
    {
        return $this->belongsTo(Node::class, 'node_id');
    }

    public function UsedSpaced()
    {
        return number_format((($this->vDISK - $this->used_space) / $this->vDISK) * 100, 2, '.', '').'%';
    }

    public function PercentageUsedSpaced()
    {
        return (($this->vDISK - $this->used_space) / $this->vDISK) * 100;
    }

}
