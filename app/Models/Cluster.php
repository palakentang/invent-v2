<?php

namespace App\Models;

use App\Models\ClusterSystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cluster extends Model
{
    use HasFactory;
    protected $table = 'cluster';
    protected $guarded = [];
    protected $primaryKey = 'id';

    /**
     * Get the ClusterSystem that owns the Cluster
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ClusterSystem(): BelongsTo
    {
        return $this->belongsTo(ClusterSystem::class);
    }

    /**
     * Get all of the Nodes for the Cluster
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Nodes(): HasMany
    {
        return $this->hasMany(Node::class);
    }

    public function childrenNodes()
    {
        return $this->hasMany(Node::class)->with('VirtualMachines');
    }
}
