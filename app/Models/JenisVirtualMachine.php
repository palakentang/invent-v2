<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JenisVirtualMachine extends Model
{
    use HasFactory;
    protected $table = 'jenis_virtual_machines';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    /**
     * Get all of the VirtualMachine for the JenisVirtualMachine
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function VirtualMachine(): HasMany
    {
        return $this->hasMany(VirtualMachine::class, 'jenis_virtual_machines_id');
    }
}
