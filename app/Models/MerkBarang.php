<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Services\AutoIncrementServices;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MerkBarang extends Model
{
    use HasFactory;
    protected $table = 'merks';
    protected $primaryKey = 'id';
    protected $guarded = [];

    /**
     * Get all of the Baranag for the Lokasi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Barangs(): HasMany
    {
        return $this->hasMany(Barang::class, 'merk_id');
    }
    
}
