<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pengadaan extends Model
{
    use HasFactory;
    protected $table = 'pengadaans';
    protected $primaryKey = 'id';
    protected $guarded = [];
    // protected $dateFormat = 'd F Y';

    /**
     * Get all of the Barangs for the Pengadaan
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Barangs(): HasMany
    {
        return $this->hasMany(Barang::class);
    }

    
}
