<?php

namespace App\Imports;

use App\Models\PowerVMList;
use Maatwebsite\Excel\Concerns\ToModel;

class PowerVMListImportFromSQL implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PowerVMList([
            'power_vm_id' => $row[1],
            'deskripsi' => $row[2],
            'hostname' => $row[3],
            'ip_address' => $row[4],
            'vCpu' => $row[5],
            'vRam' => $row[6],
            'vDisk' => $row[7],
            'OS' => $row[8],
            'copmonents' => $row[9],
            'service'=> $row[10],

        ]);
    }
}
