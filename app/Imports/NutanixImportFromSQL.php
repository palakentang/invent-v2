<?php

namespace App\Imports;

use App\Models\Nutanix;
use Maatwebsite\Excel\Concerns\ToModel;

class NutanixImportFromSQL implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Nutanix([
            'host'	=> $row[1],
            'name'	=> $row[2],
            'ip_address'	=> $row[3],
            'core'	=> $row[4],
            'memory_capacity'	=> $row[5],
            'provisioned_space'	=> $row[6],
            'used_space'=> $row[7],
        ]);
    }
}
