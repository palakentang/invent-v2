<?php

namespace App\Imports;

use App\Models\Barang;
use Maatwebsite\Excel\Concerns\ToModel;

class BarangImportFromSQL implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Barang([
            'id' => $row[0],
            'nama_barang' => $row[1],
            'lokasis_id' => $row[2],
            'pengadaans_id' => $row[3],
            'jenis_atau_tipe' => $row[5],
            'garansi_awal' => date('Y-m-d', strtotime($row[9])),
            'garansi_akhir' => date('Y-m-d', strtotime($row[10])),
            'serial_number' => $row[6],
            'position' => json_encode(array('row' => json_decode($row[7])->row, 'rack' => json_decode($row[7])->rack, 'unit' => $row[8])),
            'jenis_barang_id' => $row[4]
        ]);
    }
}
