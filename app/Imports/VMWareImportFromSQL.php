<?php

namespace App\Imports;

use App\Models\VMware;
use Maatwebsite\Excel\Concerns\ToModel;

class VMWareImportFromSQL implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new VMware([
            'hostname' => $row[3],
            'guest_os' => $row[4],
            'state' => $row[5],
            'IP_address' => $row[6],
            'memory_size' => $row[7],
            'cpus' => $row[8],
            'provisioned_space' => $row[9],
            'used_space' => $row[10],
        ]);
    }
}
