

## How to Run Aplication From Localhost

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- Clone this repository on v2 Branch to Root Folder Web Server
- Import SQL to database
- Composer Install
- Create .env File on this outside Folder Web Application
    - Make sure the database name and setup on .env File
- Running on Command Prompt (make sure you have composer installed)
    - composer install 
    - php artisan key:generate
    - php artisan serve.

## How to Deploy Aplication to Server

- run on Command Prompt git pull origin v2
- Make sure no database new migration on seeder
- If there is additional migration or seeder, run the command
    - php artisan migrate
    - php artisan db:seed --class=SeederFileName
