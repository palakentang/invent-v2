<?php

use App\Http\Controllers\AjaxController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MerkController;
use App\Http\Controllers\StatusBarangController;
use App\Http\Controllers\TipeController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\ClusterController;
use App\Http\Controllers\ClusterSystemController;
use App\Http\Controllers\LokasiController;
use App\Http\Controllers\VMwareController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PengadaanController;
use App\Http\Controllers\JenisBarangController;
use App\Http\Controllers\MerkBarangController;
use App\Http\Controllers\JenisVirtualMachineController;
use App\Http\Controllers\KategoriBarangController;
use App\Http\Controllers\NodeController;
use App\Http\Controllers\NutanixController;
use App\Http\Controllers\OSController;
use App\Http\Controllers\PowerVMController;
use App\Http\Controllers\PowerVMListController;
use App\Http\Controllers\VirtualMachineController;
use App\Models\JenisVirtualMachine;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('');
// });

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/barang-guest', [BarangController::class, 'index'])->name('barang.index');
Route::resource('/virtual-machine', VirtualMachineController::class)->only(['index']);

Route::prefix('hypervisor')->name('hypervisor.')->group(function ()
{
    Route::resource('/vmware', VMwareController::class)->only(['index']);
    Route::resource('/nutanix', NutanixController::class)->only(['index']);
    Route::resource('/powervm/{power_vm_id}/lists', PowerVMListController::class, ['names' => 'powervm.lists'])->only(['index']);
    Route::get('powervm/lists', [PowerVMListController::class, 'show'])->name('powervmlists');
    Route::resource('/powervm', PowerVMController::class)->only(['index']);
    // Route::resource('/powervm/list', NutanixController::class);
});

Route::prefix('master')->name('master.')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/barang', BarangController::class);
    Route::post('/barang/tambah/ip-address', [BarangController::class, 'tambahIPAddress'])->name('barang.tambah.ipAddress');	
    Route::resource('/jenis', JenisBarangController::class);
    Route::resource('/pengadaan', PengadaanController::class);
    Route::resource('/lokasi', LokasiController::class);
    Route::resource('/merk', MerkController::class);
	Route::resource('/statusbarang', StatusBarangController::class);
    Route::resource('/tipe', TipeController::class);
    Route::resource('/kategori', KategoriBarangController::class);
    Route::resource('/cluster', ClusterController::class);
    Route::resource('/os', OSController::class);


    Route::prefix('hypervisor')->name('hypervisor.')->group(function ()
    {
        Route::resource('/vmware', VMwareController::class);
        Route::resource('/nutanix', NutanixController::class);
        Route::resource('/powervm/{power_vm_id}/lists', PowerVMListController::class, ['names' => 'powervm.lists']);
        Route::get('powervm/lists', [PowerVMListController::class, 'show'])->name('powervmlists');
        Route::resource('/powervm', PowerVMController::class);
        // Route::resource('/powervm/list', NutanixController::class);
    });
    
    Route::prefix('virtual-machine')->name('virtual-machine.')->group(function ()
    {
        Route::resource('/vm', VirtualMachineController::class);
        Route::resource('/node', NodeController::class);
        Route::resource('/cluster', ClusterController::class);
        Route::resource('/cluster-system', ClusterSystemController::class);
    });

    Route::prefix('import')->name('import.')->group(function ()
    {
        Route::post('/barang', [BarangController::class, 'importMass'])->name('barang');
        Route::post('/vmware', [VMwareController::class, 'importMass'])->name('vmware');
        Route::post('/nutanix', [NutanixController::class, 'importMass'])->name('nutanix');
        Route::post('/powervm', [PowerVMController::class, 'importMass'])->name('powervm');
        Route::post('/powervmlist', [PowerVMListController::class, 'importMass'])->name('powervmlist');
    });
});

Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('/ipAddress/setMgmt', [AjaxController::class, 'setIPMgmt'])->name('setMgmt');
    // Route::post('/barang/store', [BarangsController::class, 'updateJSON'])->name('barang.store');

});

Route::view('/powergrid', 'powergrid-demo');
Route::view('/users', 'index');
