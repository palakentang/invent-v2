@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        /* margin-top: -0.3rem; */
    }
</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div @class([
                    'col-lg-4',
                    'd-none' => Request::segment(1) != 'master'
                ])>
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form {{ $pageTitle }}</h5>
                            {{-- <form action="{{ route('master.import.nutanix') }}" method="post" id="import" class="float-right" enctype="multipart/form-data">
                                @csrf
                                {!! Form::file('import', [null]) !!}
                                {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                            </form> --}}
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')

                            @endcomponent
                            <form action="{{ route($route['update'], $data->id) }}" method="post">
                                @csrf
                                @component('copmonents.form.virtualmachine', [
                                    'data' => $data,
                                    'select' => $select
                                ])

                                @endcomponent
                                <button type="submit" class="btn btn-success float-right">Simpan {{ $pageTitle }}</button>
                            </form>
                            {{-- <div class="form-group fieldGroupCopy" style="display: none;">
                                <div class="row">
                                    <div class="col-9 ">
                                        <label for="attribute">Tambah Attribut</label>
                                        <div class="form-group">
                                            <label for="key">Key</label>
                                            <input type="text" name="key[]" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="key">value</label>
                                            <input type="text" name="value[]" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group float-right">
                                            <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                        </div>
                    </div>
                </div>
                <div @class([
                    'col-lg-8' => Request::segment(1) == 'master',
                    'col-lg-12' => Request::segment(1) != 'master'
                ])>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.virtualmachine', [
                                'thead' => ['Nama VM', 'Jenis VM', 'Deskripsi', 'IP Address', 'Kapasitas Memori', 'Power State'],
                                'data' => $table,
                                'route' => $route
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
@section('js-third')
    <script>
        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        $(document).ready(function(){
            // membatasi jumlah inputan
            var maxGroup = 10;

            //melakukan proses multiple input
            $(".addMore").click(function(){
                if($('body').find('.fieldGroup').length < maxGroup){
                    var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
@endsection
