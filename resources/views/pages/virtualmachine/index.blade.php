@extends('layouts.app')
@section('css-third')
<style>
</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6"><h5 class="m-0">List {{ $pageTitle }}</h5></div>
                                @if (\Request::segment(1) == 'master')
                                <div class="col-6"><button class="btn btn-success float-right" onclick="tambahVM()">Tambah VM</button></div>
                                @endif
                            </div>
                            
                            
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.virtualmachine', [
                                'thead' => ['Host', 'Nama VM', 'Cluster', 'Jenis VM', 'Deskripsi', 'IP Address', 'Kapasitas Memori', 'Power State'],
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            @component('copmonents.modal.global.show', [
                'field' => [
                    'nama_vm',
                    'deskripsi',
                    'ip_address',
                    'jenis_virtual_machine',
                    'Core',
                    'kapasitas_memori',
                    'space_hdd',
                    'status_power',
                    'os'
                ],
            ])

            @endcomponent
            @component('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah VM',
                'route' => route($route['store']),
                'form' => 'copmonents.form.virtualmachine',
                'select' => $select,
            ])

            @endcomponent
            @component('copmonents.modal.global.edit', [
                'pageTitle' => 'VM',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.virtualmachine',
                'select' => $select,
            ])

            @endcomponent
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
@section('js-third')
    <script>

        function tambahVM() {
            $('#modal-tambah').modal()
            
        }

        function ubah(params) {
            $('#modal-edit').modal()
            $("form#update").attr('action', "{{ url('master/virtual-machine/') }}/"+params.id+"");
            $('input#nama_vm').val(params.nama_vm)
            $('textarea#deskripsi').val(params.deskripsi)
            $('select[name=jenis_virtual_machines_id]').val(params.jenis_virtual_machines_id);
            $('select[name=barang_id]').val(params.barang_id);
            $( "input#"+params.status_power+"").prop('checked', true);
            $('input#ip_address').val(params.ip_address)
            $('input#core').val(params.core)
            $('input#kapasitas_memori').val(params.kapasitas_memori)
            $('input#space_hdd').val(params.space_hdd)
            $('input#os').val(params.os)
            $('input#mac_address').val(params.mac_address)
            $('input#used_spaced').val(params.used_spaced)
        }

function lihat(params) {
    $('#modal-show').modal();
    console.log(params.cluster);

    // var html = '';
    // $.each(JSON.parse(params.meta), function (indexInArray, valueOfElement) {
    //     html += '<div class="row">'
    //     html += '<div class="col-5"> <strong>'+indexInArray+'</strong> </div>'
    //     html += '<div class="col-2"> : </div>'
    //     html += '<div class="col-5"> '+valueOfElement+' </div>'
    //     html += '</div>'
    // });
    var html = '<ul style="list-style-type: none;padding-left: 0;">';
            html += '<li><strong>Mac Address</strong>: '+params.mac_address+'</li>'
            html += '<li><strong>Used Spaced</strong>: '+params.used_spaced+'</li>'
            html += '<li><strong>Cluster</strong>: '+params.cluster.name+'</li>'
        html += '<ul>';
    $('td#nama_vm').html(params.nama_vm);
    $('td#deskripsi').html(html);
    $('td#ip_address').html(params.ip_address);
    $('td#jenis_virtual_machine').html(params.jenis.nama);
    $('td#Core').html(params.core);
    $('td#kapasitas_memori').html(params.kapasitas_memori);
    $('td#space_hdd').html(params.space_hdd);
    $('td#status_power').html(params.status_power);
    $('td#os').html(params.os);
}

        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });
        $(document).ready(function(){
            // membatasi jumlah inputan
            var maxGroup = 10;

            //melakukan proses multiple input
            $(".addMore").click(function(){
                if($('body').find('.fieldGroup').length < maxGroup){
                    var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click",".remove",function(){
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
@endsection
