@extends('layouts.app')
@section('css-third')
<style>
</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6"><h5 class="m-0">List {{ $pageTitle }}</h5></div>
                                @if (\Request::segment(1) == 'master')
                                <div class="col-6"><button class="btn btn-success float-right" onclick="tambah()">Tambah {{ $pageTitle }}</button></div>
                                @endif
                            </div>
                            
                            
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')@endcomponent
                            @component('copmonents.tables.cluster-system', [
                                'thead' => ['Cluster System', 'Deskripsi', 'Dibuat', 'Diubah'],
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            @component('copmonents.modal.global.show', [
                'field' => [
                    'name',
                    'description',
                ],
            ])

            @endcomponent

            @component('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah Cluster System',
                'route' => route($route['store']),
                'form' => 'copmonents.form.cluster-system',
                'select' => null,
            ])

            @endcomponent

            {{-- Modal Edit --}}
            @component('copmonents.modal.global.edit', [
                'pageTitle' => 'Cluster System',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.cluster-system',
                'select' => null,
            ])

            @endcomponent
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
@section('js-third')
<script>
    function tambah() {
        $('#modal-tambah').modal();
    }
    
    function ubah(params) {
        $('#modal-edit').modal();
        $("form#update").attr('action', "{{ url('master/virtual-machine/cluster-system') }}/"+params.id+"");

        $('input[name=name]').val(params.name);
        $('textarea[name=description]').val(params.description);
    }
</script>
@endsection