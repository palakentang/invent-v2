@extends('layouts.app')
@section('css-third')
<style>
</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6"><h5 class="m-0">List {{ $pageTitle }}</h5></div>
                                @if (\Request::segment(1) == 'master')
                                <div class="col-6"><button class="btn btn-success float-right" onclick="tambah()">Tambah {{ $pageTitle }}</button></div>
                                @endif
                            </div>
                            
                            
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')@endcomponent
                            @component('copmonents.tables.node', [
                                'thead' => ['Node Name', 'Cluster', 'IP Address'],
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            @component('copmonents.modal.global.show', [
                'field' => [
                    'name',
                    'description',
                ],
            ])

            @endcomponent

            @component('copmonents.modal.global.add', [
                'pageTitle' => 'Tambah Node',
                'route' => route($route['store']),
                'form' => 'copmonents.form.node',
                'select' => null,
            ])

            @endcomponent

            {{-- Modal Edit --}}
            @component('copmonents.modal.virtualmachine.node.edit', [
                'pageTitle' => 'Node',
                'data' => $data,
                'route' => null,
                'form' => 'copmonents.form.node',
                'select' => null,
            ])

            @endcomponent
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
@section('js-third')
<script>
    function tambah() {
        $('#modal-tambah').modal();
    }
    
    function ubah(params) {
        $('#modal-edit').modal();
        $("form#edit").attr('action', "{{ url('master/virtual-machine/node') }}/"+params.id+"");

        $('input[name=node_name]').val(params.node_name);
        $('select[name=cluster_id]').val(params.cluster_id);
        $('input[name=machine_name]').val(params.machine_name);

        $.each(JSON.parse(params.ip_address), function (indexInArray, valueOfElement) { 
            console.log(valueOfElement);
            $('#edit-row-address').append(
                '<div class="form-group">'+
                    '<label for="new_ip_address">IP Address</label>'+
                    '<input class="form-control" name="ip_address[]" value="'+valueOfElement+'" type="text">'+
                '</div>'
            );
        });
    }

</script>
@endsection