@extends('layouts.app')
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li @class([
                            'breadcrumb-item',
                        ]) class="breadcrumb-item">
                        <a style="text-decoration: none;color: #6c757d;font-weight: 600;" >{{ ucwords($pageTitle) }}</a>
                        </li>
                    </ol>
                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $nTotalPerangkat }}</h3>

                <p>Total Perangkat</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{ route('barang.index') }}" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ $nTotalPerangkatHabisGaransi }} </h3>

                <p>Perangkat Habis Garansi</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $nTotalVirtualMachine }}</h3>

                <p>Total VM</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>65</h3>

                <p>VM Resource Maksimal</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->		
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Jenis Perangkat</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-100" style="padding: 20px">
                            <div class="position-relative mb-4">
                                <canvas id="sales-chart" height="200"></canvas>
                            </div>
                        </div>
                        <!--
                        <div class="card-footer">
                            <div class="row">
                                @foreach ($merk as $key => $value)
                                <div class="col-sm-3 col-6">
                                    <div class="description-block border-right">
                                        <h5 class="description-header">{{ $key }}</h5>
                                        <span class="description-text">{{ $value }} Item</span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        -->
                        <!-- /.card-body -->
                    </div>
                    @component('copmonents.diagram.bar-chart', [
                        'data' => $jenis_vm,
                        'title' => 'Virtual Machine',
                        'id' => 'virtual-machine-bar'
                    ])
                        
                    @endcomponent
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Komposisi Perangkat</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="chart-responsive">
                                        <canvas id="pieChart" height="175"></canvas>
                                    </div>
                                    <!-- ./chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-5">
                                    <ul class="chart-legend clearfix">
                                        @foreach ($count['jenis']['quantity'] as $item)
                                        <li style="margin: 10px 0;"><i class="far fa-circle {{ $item->_meta }}">&nbsp;{{ ucfirst($item->nama_jenis_barang) }}</i></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    @component('copmonents.diagram.pie-chart', [
                        'title' => 'Cluster System',
                        'data' => App\Models\JenisVirtualMachine::get(),
                        'id' => 'cluster-system-pie'
                    ])
                        
                    @endcomponent
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
@section('js-third')
<script src="{{ asset('v1/plugins/chart.js/Chart.min.js') }}"></script>
@component('copmonents.script.bar-chart', [
'label' => $count['jenis']['name'],
'value' => $count['jenis']['quantity']
])

@endcomponent

@component('copmonents.script.pie-chart', [
'label' => $count['jenis']['name'],
'value' => $count['jenis']['quantity']
])

@endcomponent

@component('copmonents.script.dashboard.virtualmachine.bar-chart', [
'label' => $count['clusterSystem']['name'],
'value' => $count['clusterSystem']['quantity'],
'id' => 'cluster-system-bar'
])

@endcomponent

@component('copmonents.script.dashboard.virtualmachine.pie-chart', [
'label' => $count['clusterSystem']['name'],
'value' => $count['clusterSystem']['quantity'],
'id' => 'cluster-system-pie'
])

@endcomponent

<script>
    function show(response) {
        console.log(response.pengadaan)
        $("#modal-show").modal()

        $('#merk_atau_tipe_show').html(response.jenis_atau_tipe);
        $('#header_nama_barang').html(response.nama_barang);
        $('#jenis_barang_show').html(response.jenis_barang.nama_jenis_barang);
        $('#nama_barang_show').html(response.nama_barang);
        $('#serial_number_show').html(response.serial_number);
        $('#nama_pengadaan_show').html(response.pengadaan.nama_pengadaan);

        response.i_p_addresses.forEach(element => {
            var html = `<li>`+element.ip_address+`</li>`;
            $('#ip_show').append(html)
        });

        $('#daterange_show').html((response.garansi_awal == null ? '' : response.garansi_awal)+' - '+(response.garansi_akhir == null ? '' : response.garansi_akhir));
        // $('#pengadaan_id_show').html(response.pengadaan.nama_pengadaan);

        $('#row_show').html(JSON.parse(response.position).row);
        $('#rack_show').html(JSON.parse(response.position).rack);
        $('#unit_show').html(JSON.parse(response.position).unit);
    }
</script>
@endsection