@extends('layouts.app')
@section('css-third')
<link rel="stylesheet" type="text/css" href="{{ asset('v1/dist/css/daterangepicker/daterangepicker.css') }}" />
<script>
$(function() {
  $("#store").validate({
    rules: {
      nama_barang: {
        required: true,
        minlength: 3
      },
      action: "required"
    },
    messages: {
      nama_barang: {
        required: "Isikan nama barang",
        minlength: "Your data must be at least 3 characters"
      },
      action: "Please provide some data"
    }
  });
});
</script>
<style>
    .fab-container {
        position: fixed;
        bottom: 5px;
        right: 13px;
        z-index: 999;
        cursor: pointer;
    }

    .fab-icon-holder {
    width: 50px;
    height: 50px;
    border-radius: 100%;
    background: #016fb9;
    box-shadow: 0 6px 20px rgba(0, 0, 0, 0.2);
    }

    .fab-icon-holder:hover {
    opacity: 0.8;
    }

    .fab-icon-holder i {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    font-size: 25px;
    color: #ffffff;
    }

    .fab {
    width: 60px;
    height: 60px;
    background: #28a745;
    }

    .fab-options {
    list-style-type: none;
    margin: 0;
    position: absolute;
    bottom: 70px;
    right: 0;
    opacity: 0;
    transition: all 0.3s ease;
    transform: scale(0);
    transform-origin: 85% bottom;
    }

    .fab:hover+.fab-options,
    .fab-options:hover {
    opacity: 1;
    transform: scale(1);
    }

	.modal-backdrop {
	   background-color: blue;
	}

	.modal-title {
		font-size: 16px;
	}

	.modal-header {
		font-size: 3em;
		padding-top: 0px;
		padding-bottom: 0px;
	   /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#b3dced+0,29b8e5+50,bce0ee+100;Blue+Pipe */
		background: #b3dced; /* Old browsers */
		background: -moz-linear-gradient(top, #b3dced 0%, #29b8e5 50%, #bce0ee 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee',GradientType=0 ); /* IE6-9 */ 

	}
	
	.modal-body {
		/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f6f8f9+0,e5ebee+50,d7dee3+51,f5f7f9+100;White+Gloss */
		background: #f6f8f9; /* Old browsers */
		background: -moz-linear-gradient(top,  #f6f8f9 0%, #e5ebee 50%, #d7dee3 51%, #f5f7f9 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f6f8f9', endColorstr='#f5f7f9',GradientType=0 ); /* IE6-9 */		
	   	}

</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @component('layouts.partials.alert')
            @endcomponent
            <div class="row">
                {{-- <div class="col-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">Kelola Perangkat</h5>
                        </div>
                        <div class="card-body" style="max-height: 36rem;overflow-y: auto;">
                            <form action="" method="post" id="AddBarang">
                                @csrf
                                @component('copmonents.form.barang', [
                                    'data' => null,
                                    'select' => $select,
                                ])
                                @endcomponent
                                <button type="submit" class="btn btn-success float-right">Simpan {{ $pageTitle }}</button>
                            </form>
                        </div>
                    </div>
                </div> --}}
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
							    <div class="col-md-12 bg-light text-right">
                                    {{-- <form action="{{ route('master.import.barang') }}" method="post" id="import" class="float-right" enctype="multipart/form-data">
                                        @csrf
                                        {!! Form::file('import', [null]) !!}
                                        {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                                    </form> --}}
                                    @if (\Request::segment(1) == 'master')
                                        <button onclick="tambahBarang()" class="btn btn-success float-right"><i class="fas fa-plus">&nbsp;</i>Tambah Perangkat</button>
                                    @endif
                                </div>
                            </div>
                    {{--    <div class="card-body"> --}}
					        &nbsp;
                            @component('copmonents.tables.barang', [
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent

                            {{-- Modal Barang Detail --}}
                            @component('copmonents.modal.barang.show', [
                                'select' => $select,
                                'data' => null
                            ])

                            @endcomponent

                            {{-- Modal Barang Detail --}}
                            @component('copmonents.modal.barang.add', [
                                'select' => $select
                            ])

                            @endcomponent

                            {{-- Modal Barang Detail --}}
                            @component('copmonents.modal.barang.edit', [
                                'select' => $select,
                                'data' => null
                            ])

                            @endcomponent

                            {{-- Modal IP Mgmt --}}
                            @component('copmonents.modal.ip.mgmt')

                            @endcomponent
                            
                            {{-- Modal IP Mgmt --}}
                            @component('copmonents.modal.barang.add-ip')

                            @endcomponent
{{--                        </div> --}}
                    </div>
                </div>
                <!--<div class="fab-container">-->
                <!--    <div class="fab fab-icon-holder">-->
                <!--        <a href="#" onclick="tambahBarang()" ><i class="fas fa-plus"></i></a>-->
                <!--    </div>-->
                <!--</div>-->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

</div>
<!-- /.content -->
@endsection
@section('js-third')
<script type="text/javascript" src="{{ asset('v1/dist/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('v1/dist/js/daterangepicker.min.js') }}"></script>
<script>
    function tambahBarang() {
        $('#modal-tambah').modal()
      $("input[type=text]").val("");

    }
    
    function tambahIP(params) {
        console.log(params)
        $('#modal-tambah-ip').modal()
        $('input#barang_id').val(params)
      $("input[type=text]").val("");

    }
</script>
@component('copmonents.script.barang')

@endcomponent
@endsection
