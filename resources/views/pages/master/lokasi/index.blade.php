@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem; 
    }
</style>
    
@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])
                    
                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')
                                
                            @endcomponent
                            <form action="{{ route($route['store']) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Lokasi</label>
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'nama']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi</label>
                                    <textarea name="deskripsi" id="deskripsi" cols="2" rows="2" class="form-control"></textarea>
                                </div>
                                <button type="submit" class="btn btn-success float-right">Simpan {{ $pageTitle }}</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.lokasi', [
                                'thead' => ['Lokasi', 'Dibuat', 'Diubah'],
                                'data' => $data,
                                'route' => $route
                            ])
                                
                            @endcomponent                      
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection