@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem;
    }


    .fab-container {
        position: fixed;
        bottom: 5px;
        right: 13px;
        z-index: 999;
        cursor: pointer;
    }

    .fab-icon-holder {
    width: 50px;
    height: 50px;
    border-radius: 100%;
    background: #016fb9;
    box-shadow: 0 6px 20px rgba(0, 0, 0, 0.2);
    }

    .fab-icon-holder:hover {
    opacity: 0.8;
    }

    .fab-icon-holder i {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    font-size: 25px;
    color: #ffffff;
    }

    .fab {
    width: 60px;
    height: 60px;
    background: #28a745;
    }

    .fab-options {
    list-style-type: none;
    margin: 0;
    position: absolute;
    bottom: 70px;
    right: 0;
    opacity: 0;
    transition: all 0.3s ease;
    transform: scale(0);
    transform-origin: 85% bottom;
    }

    .fab:hover+.fab-options,
    .fab-options:hover {
    opacity: 1;
    transform: scale(1);
    }

</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('master.hypervisor.powervm.index') }}">PowerVM</a></li>
                        <li @class([
                            'breadcrumb-item',
                        ]) class="breadcrumb-item">
                            {{-- <a style="text-decoration: none;color: #6c757d;font-weight: 600;" href="'.Request::segment($i).'.index")></a> --}}
                            Power VM Lists
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                            <form action="{{ route('master.import.powervmlist') }}" method="post" id="import" class="float-right" enctype="multipart/form-data">
                                @csrf
                                {!! Form::file('import', [null]) !!}
                                {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                            </form>
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.powervmlists', [
                                'thead' => ['ID','Deskripsi', 'Hostname', 'IP Address', 'Komponen', 'Service'],
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent
                            @component('copmonents.modal.global', [
                                'form' => 'copmonents.form.powervmlists',
                                'route' => route($route['store'], Request::segment(4)),
                                'pageTitle' => $pageTitle,
                                'data' => null
                            ])

                            @endcomponent

                            {{-- Modal PowerVM --}}
                            @component('copmonents.modal.global.show', [
                                'field' => ['power_vm_id', 'deskripsi', 'hostname', 'ip_address', 'vCpu', 'vRam', 'vDisk', 'OS', 'copmonents', 'service']
                            ])

                            @endcomponent

                            @component('copmonents.modal.global.edit', [
                                'form' => 'copmonents.form.powervmlists',
                                'route' => route($route['store'], Request::segment(4)),
                                'pageTitle' => $pageTitle,
                                'data' => $data
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
                <div @class([
                    'fab-container',
                    'd-none' => Request::segment(1) != 'master'
                ])>
                    <div class="fab fab-icon-holder">
                        <a href="#" onclick="tambahBarang()" ><i class="fas fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
<script>
    function tambahBarang() {
        $('#modal-tambah').modal()
    }

    function ubah(params) {
        $('#modal-edit').modal()
        $('#update').attr('action', params.route);
        $('input[name=deskripsi]').val(params.data.deskripsi);
        $('input[name=hostname]').val(params.data.hostname);
        $('input[name=ip_address]').val(params.data.ip_address);
        $('input[name=vCpu]').val(params.data.vCpu);
        $('input[name=vRam]').val(params.data.vRam);
        $('input[name=vDisk]').val(params.data.vDisk);
        $('input[name=OS]').val(params.data.OS);
        $('input[name=copmonents]').val(params.data.copmonents);
        $('input[name=service]').val(params.data.service);
    }
    
    function lihat(params) {
        console.log(params.deskripsi);
        $('#modal-show').modal()
        $('#power_vm_id').html(params.power_v_m.kd_power_vm);
        $('td#deskripsi').html(params.deskripsi);
        $('td#hostname').html(params.hostname);
        $('td#ip_address').html(params.ip_address);
        $('td#vCpu').html(params.vCpu);
        $('td#vRam').html(params.vRam);
        $('td#vDisk').html(params.vDisk);
        $('td#OS').html(params.OS);
        $('td#copmonents').html(params.copmonents);
        $('td#service').html(params.service);

    }
</script>
