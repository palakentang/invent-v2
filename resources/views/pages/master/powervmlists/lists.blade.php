@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem;
    }


    .fab-container {
        position: fixed;
        bottom: 5px;
        right: 13px;
        z-index: 999;
        cursor: pointer;
    }

    .fab-icon-holder {
    width: 50px;
    height: 50px;
    border-radius: 100%;
    background: #016fb9;
    box-shadow: 0 6px 20px rgba(0, 0, 0, 0.2);
    }

    .fab-icon-holder:hover {
    opacity: 0.8;
    }

    .fab-icon-holder i {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    font-size: 25px;
    color: #ffffff;
    }

    .fab {
    width: 60px;
    height: 60px;
    background: #28a745;
    }

    .fab-options {
    list-style-type: none;
    margin: 0;
    position: absolute;
    bottom: 70px;
    right: 0;
    opacity: 0;
    transition: all 0.3s ease;
    transform: scale(0);
    transform-origin: 85% bottom;
    }

    .fab:hover+.fab-options,
    .fab-options:hover {
    opacity: 1;
    transform: scale(1);
    }

</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('master.hypervisor.powervm.index') }}">PowerVM</a></li>
                        <li @class([
                            'breadcrumb-item',
                        ]) class="breadcrumb-item">
                            {{-- <a style="text-decoration: none;color: #6c757d;font-weight: 600;" href="'.Request::segment($i).'.index")></a> --}}
                            Power VM Lists
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach ($data as $item)
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0"><a href="{{ route('master.hypervisor.powervm.lists.index', [$item->id]) }}">{{ $pageTitle }} {{ $item->kd_power_vm }}</a></h5>

                        </div>
                        <div class="card-body">
                            <table id="" class="display table table-striped" style="width:100%">
                                <thead>
                                    <tr style=" text-align: center">
                                        <th>ID</th>
                                        <th>Deskripsi</th>
                                        <th>Hostname</th>
                                        <th>IP Address</th>
                                        <th>Komponen</th>
                                        <th>Service</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1
                                    @endphp
                                    @foreach ($item->Lists as $value)
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $value->deskripsi }}</td>
                                        <td>{{ $value->hostname }}</td>
                                        <td>{{ $value->ip_address }}</td>
                                        <td>{{ $value->copmonents }}</td>
                                        <td>{{ $value->service }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="fab-container">
                    <div class="fab fab-icon-holder">
                        <a href="#" onclick="tambahBarang()" ><i class="fas fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
