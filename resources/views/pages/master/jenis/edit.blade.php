@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem; 
    }
</style>
    
@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])
                    
                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form Edit {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')
                                
                            @endcomponent
                            <form action="{{ route($route, $data->id) }}" method="post">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="nama_jenis_barang">Nama Jenis Barang</label>
                                    {!! Form::text('nama_jenis_barang', $data->nama_jenis_barang, ['class' => 'form-control', 'id' => 'nama_jenis_barang']) !!}
                                </div>
                                <button type="submit" class="btn btn-success float-right">Update {{ $pageTitle }}</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr style=" text-align: center">
                                        <th>Jenis Barang</th>
                                        <th>Dibuat</th>
                                        <th>Diubah</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (app\Models\JenisBarang::get() as $item)
                                    <tr style=" text-align: center">
                                        <td>{{ $item->nama_jenis_barang }}</td>
                                        <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
                                        <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
                                        <td width="40" style="white-space:nowrap; align=right;">
                                            <div class="row">
                                                {{-- <div class="col-4"></div> --}}
                                                <div class="col-6">
													<form action="{{ route('master.jenis.destroy', $item) }}" method="post">
													<a title="edit" class="btn btn-success btn-sm" href="{{ route('master.jenis.edit', $item->id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
													<button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
														@method('delete')
														@csrf
													</form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection