@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem;
    }
</style>

@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])

                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div @class([
                    'col-lg-4',
                    'd-none' => Request::segment(1) != 'master'
                ])>
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form {{ $pageTitle }}</h5>
                            {{-- <form action="{{ route('master.import.powervm') }}" method="post" id="import" class="float-right" enctype="multipart/form-data">
                                @csrf
                                {!! Form::file('import', [null]) !!}
                                {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                            </form> --}}
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')

                            @endcomponent
                            <form action="{{ route($route['store']) }}" method="post">
                                @csrf
                                @component('copmonents.form.powervm', [
                                    'data' => null,
                                    'kode' => $kode
                                ])

                                @endcomponent
                                <button type="submit" class="btn btn-success float-right">Simpan {{ $pageTitle }}</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div @class([
                    'col-lg-8' => Request::segment(1) == 'master',
                    'col-lg-12' => Request::segment(1) != 'master'
                ])>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.powervm', [
                                'thead' => ['ID','Nama VM', 'Hostname', 'IP', 'Dibuat', 'Diubah'],
                                'data' => $data,
                                'route' => $route
                            ])

                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection
