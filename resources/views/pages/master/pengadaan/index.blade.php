@extends('layouts.app')
@section('css-third')
<style>
    .form-check-input {
        margin-top: -0.7rem; 
    }
</style>
    
@endsection
@section('contents')
<!-- Content Header (Page header) -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $pageTitle }}</h1>
                </div><!-- /.col -->
                @component('layouts.partials.breadcrumb', [
                    'pageTitle' => $pageTitle
                ])
                    
                @endcomponent
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-title">
                            <h5 class="card-header">Form {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('layouts.partials.alert')
                                
                            @endcomponent
                            <form action="{{ route($route['store']) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="nama_pengadaan">Nama Pengadaan Barang</label>
                                    {!! Form::text('nama_pengadaan', old('nama_pengadaan'), ['class' => 'form-control', 'id' => 'nama_jenis_barang']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="tgl_pengadaan">Tanggal Pengadaan Barang</label>
                                    {!! Form::date('tgl_pengadaan', old('tgl_pengadaan'), ['class' => 'form-control', 'id' => 'nama_jenis_barang']) !!}
                                </div>
                                <button type="submit" class="btn btn-success float-right">Simpan {{ $pageTitle }}</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="m-0">List {{ $pageTitle }}</h5>
                        </div>
                        <div class="card-body">
                            @component('copmonents.tables.pengadaan', [
                                'thead' => ['Nama Pengadaan', 'Tgl BAST', 'Diubah'],
                                'data' => $data,
                                'route' => $route
                            ])
                                
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
@endsection