{{-- @extends('layouts.app') --}}

{{-- @section('contents') --}}

<div class="p-6 bg-gray-100 mb-3 rounded-md shadow-md text-gray-700 border border-gray-400">
    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <img src="https://simlala.dephub.go.id/simlala/ox_images/ox_kemenhub_img/kemenhub-logo.png" alt="" srcset="" class="float-end">
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">
            <h2 class="align-self-center">Aplikasi Inventaris Infrastruktur Ditlala</h2>
        </div>
    </div>
</div>

<div class="bg-white p-4 border border-gray-200 rounded">
    @livewire('users')
</div>
{{-- @endsection --}}