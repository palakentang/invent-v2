<div class="card">
    <div class="card-header">
        <h3 class="card-title">Kategori {{ $title }} Report</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-100" style="padding: 20px">
        <div class="position-relative mb-4">
            <canvas id="{{ $id }}" height="200"></canvas>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            @foreach ($data as $key => $value)
            <div class="col-sm-4 col-6">
                <div class="description-block border-right">
                    <h5 class="description-header">{{ strtoupper(str_replace('_', ' ', $key)) }}</h5>
                    <span class="description-text">{{ $value }} Item</span>
                </div>
            </div>
            @endforeach
        </div>
        <!-- /.row -->
    </div>
    <!-- /.card-body -->
</div>