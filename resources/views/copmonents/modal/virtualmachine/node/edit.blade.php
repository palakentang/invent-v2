<div class="modal fade" id="modal-edit" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit {{ $pageTitle }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="edit">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="node_name">Nama Node</label>
                        {!! Form::text('node_name', old('node_name'), ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="cluster_id">Cluster</label>
                        <select name="cluster_id" id="cluster_id" class="form-control" required>
                            <option value="">Pilih Cluster</option>
                            @foreach (App\Models\cluster::get() as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        Jika Cluster tidak ditemukan <a href="{{ route('master.virtual-machine.cluster.index') }}">Klik Disini</a>
                    </div>
                    <div class="form-group">
                        <label for="machine_name">Nama Mesin</label>
                        {!! Form::text('machine_name', old('machine_name'), ['class' => 'form-control']) !!}
                    </div>

                    <div id="edit-row-address">
                        
                    </div>
                    <div id="add-row-addresses">
                        
                    </div>
                    

                    <a href="#" onclick="tambahIPAddress()">Tambah IP Address</a>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('edit').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
