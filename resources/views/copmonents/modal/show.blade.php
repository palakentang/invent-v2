<div id="modal-show" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="header_nama_barang"></h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="width:100%" class="table table-striped">
                        <tr>
                            <th>Nama VM</th>
                            <td id="nama_vm"></td>
                        </tr>
                        <tr>
                            <th>Nama Barang</th>
                            <td id="nama_barang_show"></td>
                        </tr>
                        <tr>
                            <th>Nama Pengadaan:</th>
                            <td id="nama_pengadaan_show"></td>
                        </tr>
                        <tr>
                            <th>Serial Number:</th>
                            <td id="serial_number_show"></td>
                        </tr>
                        <tr>
                            <th>Garansi:</th>
                            <td id="daterange_show"></td>
                        </tr>
                        <tr>
                            <th>IP: </th>
                            <td>
                                <ul id="ip_show" style="padding-left: 15px;"></ul>
                            </td>
                        </tr>
                        <tr>
                            <th>Row: </th>
                            <td>
                                <ul id="row_show" style="padding: 0"></ul>
                            </td>
                        </tr>
                        <tr>
                            <th>Rack: </th>
                            <td>
                                <ul id="rack_show" style="padding: 0"></ul>
                            </td>
                        </tr>
                        <tr>
                            <th>Unit: </th>
                            <td>
                                <ul id="unit_show" style="padding: 0"></ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
