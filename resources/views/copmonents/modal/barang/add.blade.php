<div class="modal fade" id="modal-tambah" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Perangkat</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('master.barang.store') }}" method="post" id="store">
                    @csrf
                    @component('copmonents.form.barang', [
                        'data' => null,
                        'select' => $select
                    ])
                        
                    @endcomponent
                </form>
            </div>
            <div class="modal-footer">
			    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="button" onClick="event.preventDefault(); document.getElementById('store').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
