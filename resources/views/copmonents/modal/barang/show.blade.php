<div class="modal fade" id="modal-show" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Perangkat</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="show">
			        @method('PUT')	
                    @csrf
                    @component('copmonents.form.barang-show', [
                        'data' => null,
                        'select' => $select
                    ])
                        
                    @endcomponent
				

                </form>
            </div>
        </div>
    </div>
</div>
