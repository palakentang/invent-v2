<div class="modal fade" id="modal-edit" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit {{$pageTitle}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ $route }}" method="post" id="update">
                    @csrf
                    {{method_field('PUT')}}
                    @component($form, [
                        'data' => $data,
                        'select' => $select
                        // 'edit' => true
                    ])

                    @endcomponent
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="event.preventDefault(); document.getElementById('update').submit()" class="btn btn-success" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
