<div id="modal-show" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="header"></h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="width:100%" class="table table-striped">
                        @foreach ($field as $item)
                            <tr>
                                <th>{{ str_replace('show', '', ucfirst(str_replace('_', ' ', $item))) }}</th>
                                <td id="{{ $item }}"></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
