{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="name">Nama Cluter System</label>
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="description">Description Cluter System</label>
    {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'cols' => 2, 'rows' => 2]) !!}
</div>