<script>

</script>
<style>
#bg-modal-blue {
padding-left:10px;
padding-right:10px;
/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#b3dced+0,29b8e5+50,bce0ee+100;Blue+Pipe */
background: #b3dced; /* Old browsers */
background: -moz-linear-gradient(top, #b3dced 0%, #29b8e5 50%, #bce0ee 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee',GradientType=0 ); /* IE6-9 */ 
}
#bg-modal-green {
padding-left:10px;
padding-right:10px;
/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#d2ff52+0,91e842+100;Neon */
background: #d2ff52; /* Old browsers */
background: -moz-linear-gradient(top,  #d2ff52 0%, #91e842 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #d2ff52 0%,#91e842 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #d2ff52 0%,#91e842 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d2ff52', endColorstr='#91e842',GradientType=0 ); /* IE6-9 */
}
#bg-modal-gray {
padding-left:10px;
padding-right:10px;	
/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f6f8f9+0,e5ebee+50,d7dee3+51,f5f7f9+100;White+Gloss */
background: #f6f8f9; /* Old browsers */
background: -moz-linear-gradient(top,  #f6f8f9 0%, #e5ebee 50%, #d7dee3 51%, #f5f7f9 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #f6f8f9 0%,#e5ebee 50%,#d7dee3 51%,#f5f7f9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f6f8f9', endColorstr='#f5f7f9',GradientType=0 ); /* IE6-9 */
}
</style>
  <div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="nama_barang">Nama Perangkat <sup>*</sup></label>
				{!! Form::text('nama_barang', $data == null ? old('nama_barang') : $data->nama_barang, ['class' => 'form-control', 'id' => 'nama_barang']) !!}
				{!! Form::checkbox('isHypervisor', '1', null, ['id' => 'isHypervisor']) !!}  &nbsp;
				<label for="isHypervisor">Hypervisor</label>
			</div>	   
			
		</div>
		<div class="col">
			<div class="form-group">
				<label for="jenis_barang_id">Jenis Perangkat<sup>*</sup></label>
				{!! Form::select('jenis_barang_id', $select['jenis'], $data == null ? old('jenis_barang_id') : $data->jenis_barang_id, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="col">
			<div class="form-group">
				<label for="merk_id">Merk <sup>*</sup></label>
				{!! Form::select('merk_id', $select['merk'], $data == null ? old('id') : $data->id, ['class' => 'form-control']) !!}
			</div>
		</div>
   </div>	
   <div class="row">
      <div class="col">
		<div class="form-group">
			<label for="serial_number">Tipe</label>
			{!! Form::text('tipe', old('tipe'), ['class' => 'form-control']) !!}
		</div>
    </div>
 
    <div class="col">
		<div class="form-group">
			<label for="serial_number">Serial Number</label>
			{!! Form::text('serial_number', old('serial_number'), ['class' => 'form-control']) !!}
		</div>
    </div>
    <div class="col">
		<div class="form-group">
			<label for="mac_address">Mac Address</label>
			{!! Form::text('mac_address', old('mac_address'), ['class' => 'form-control']) !!}
		</div>
    </div>
	<div class="col">
		<div class="form-group">
			<label for="status_id">Status Perangkat<sup>*</sup></label>
			{!! Form::select('status_id', $select['status'], $data == null ? old('status_id') : $data->status_id, ['class' => 'form-control']) !!}
		</div>
	</div>
  </div> 
  <div class="row">
		<div class="col">
			<div class="form-group">
				<label for="lokasi_id">Lokasi Perangkat <sup>*</sup></label>
				{!! Form::select('lokasis_id', $select['lokasi'], $data == null ? old('id') : $data->id, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="col">		
			<div class="form-group">
				<label for="rack">Posisi Rack</label>
				{!! Form::text('PosisiRack', old('PosisiRack'), ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="col">		
			<div class="form-group">
				<label for="unit">Unit Rack</label>
				{!! Form::text('UnitRack', old('UnitRack'), ['class' => 'form-control']) !!}
			</div>
		</div>
  </div>	
  <div class="row">
		<div class="col">
			<div class="form-group">
				<label for="pengadaans_id">Sumber Pengadaan</label>
				{!! Form::select('pengadaans_id', $select['pengadaan'], old('pengadaans_id'), ['class' => 'form-control']) !!}
			</div>
        </div>
		<div class="col-6">
			<div class="form-group">
				<label>Masa Garansi / Support:</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="far fa-calendar-alt"></i>
						</span>
					</div>
					<input type="text" class="form-control" name="GaransiMulai" value="" />
					{!! Form::hidden('GaransiMulai', old('GaransiMulai'), ['id' => 'GaransiMulai']) !!}
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="far fa-calendar-alt"></i>
						</span>
					</div>
					<input type="text" class="form-control" name="GaransiAkhir" value="" />
					{!! Form::hidden('GaransiAkhir', old('GaransiAkhir'), ['id' => 'GaransiAkhir']) !!}
				</div>	
			</div>
		</div>	
   </div>	

  <div class="row">
	<div class="col">
			<div class="form-group">
				<div id="ipAddress" style="display: block;">
					<label><br>IP Adrress : </label>&nbsp; &nbsp; &nbsp;
					{!! Form::text('ip_address1', old('ip_address1'), ['class' => 'form-control']) !!} 
					{!! Form::checkbox('IPMgmt1', '1', null, ['id' => 'isIPMgmt1']) !!}&nbsp;&nbsp; 
					<label>IP Management</label>
				</div>
			</div>
	</div>
	<div class="col">
			<div class="form-group">
				<div id="ipAddress" style="display: block;">
					<label><br>IP Adrress : </label>&nbsp; &nbsp; &nbsp;
					{!! Form::text('ip_address2', old('ip_address2'), ['class' => 'form-control']) !!} 
					{!! Form::checkbox('IPMgmt2', '1', null, ['id' => 'isIPMgmt2']) !!}&nbsp;&nbsp; 
					<label>IP Management</label>
				</div>
			</div>
	</div>
	<div class="col">
			<div class="form-group">
				<div id="ipAddress" style="display: block;">
					<label><br>IP Adrress : </label>&nbsp; &nbsp; &nbsp;
					{!! Form::text('ip_address3', old('ip_address3'), ['class' => 'form-control']) !!} 
					{!! Form::checkbox('IPMgmt3', '1', null, ['id' => 'isIPMgmt3']) !!}&nbsp;&nbsp; 
					<label>IP Management</label>
				</div>
			</div>
	</div>
  </div>	
  * : wajib isi   