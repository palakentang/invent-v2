{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="node_name">Nama Node</label>
    {!! Form::text('node_name', old('node_name'), ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="cluster_id">Cluster</label>
    <select name="cluster_id" id="cluster_id" class="form-control" required>
        <option value="">Pilih Cluster</option>
        @foreach (App\Models\cluster::get() as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
        @endforeach
    </select>
    Jika Cluster tidak ditemukan <a href="{{ route('master.virtual-machine.cluster.index') }}">Klik Disini</a>
</div>
<div class="form-group">
    <label for="machine_name">Nama Mesin</label>
    {!! Form::text('machine_name', old('machine_name'), ['class' => 'form-control']) !!}
</div>

<div class="form-group" id="ip_address_row">
    <label for="ip_address">IP Address</label>
    {!! Form::text('ip_address', old('ip_address'), ['class' => 'form-control']) !!}
</div>