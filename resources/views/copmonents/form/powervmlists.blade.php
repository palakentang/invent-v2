<div class="row">
    <div class="col-12">
        <label for="deskripsi">Deskripsi</label>
        <input class="form-control" id="deskripsi" name="deskripsi" type="text">
    </div>
</div>
<div class="row">
    <div class="col-12">
        <label for="hostname">Hostname</label>
        <input class="form-control" id="hostname" name="hostname" type="text">
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="ip_address">IP Address</label>
            <input class="form-control" id="ip_address" name="ip_address" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="vCpu">vCPU</label>
            <input class="form-control" id="vCpu" name="vCpu" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="vRam">vRAM</label>
            <input class="form-control" id="vRam" name="vRam" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="vDisk">vDisk</label>
            <input class="form-control" id="vDisk" name="vDisk" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="OS">OS</label>
            <input class="form-control" id="OS" name="OS" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="copmonents">Components</label>
            <input class="form-control" id="copmonents" name="copmonents" type="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label for="service">Service</label>
            <input class="form-control" id="service" name="service" type="text">
        </div>
    </div>
</div>
