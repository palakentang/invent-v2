{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="kd_power_vm">Kode VM</label>
    <input class="form-control" id="kd_power_vm" readonly="" name="kd_power_vm" type="text" value="{{$data == null ? $kode : $data->kd_power_vm}}">
</div>
<div class="form-group">
    <label for="vm_name">Nama VM</label>
    <input class="form-control" id="vm_name" name="vm_name" type="text" value="{{$data == null ? old('kd_power_vm') : $data->vm_name}}">
</div>
<div class="form-group">
    <label for="hostname">Hostname</label>
    <input class="form-control" id="hostname" name="hostname" type="text" value="{{$data == null ? old('hostname') : $data->hostname}}">
</div>
<div class="form-group">
    <label for="vm_ip">IP Address</label>
    <input class="form-control" id="vm_ip" name="vm_ip" type="text" value="{{$data == null ? old('vm_ip') : $data->vm_ip}}">
</div>
