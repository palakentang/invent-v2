{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="name">Nama Jenis Virtual Machine</label>
    <input value="{{$data == null ? old('nama') : $data->nama}}" class="form-control" id="nama" name="nama" type="text">
</div>