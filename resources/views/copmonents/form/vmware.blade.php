{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="hostname">Hostname</label>
    <input value="{{$data == null ? old('hostname') : $data->hostname }}" class="form-control" id="hostname" name="hostname" type="text">
</div>
<div class="form-group">
    <label for="guest_os">Guest OS</label>
    <input value="{{$data == null ? old('guest_os') : $data->guest_os }}" class="form-control" id="guest_os" name="guest_os" type="text">
</div>
<div class="form-group">
    <label for="state">State</label>
    <select class="form-control" name="state">
        <option {{$data == null ? '' : $data->state == 'Power Off' ? 'selected' : '' }} value="Power Off">Power Off</option>
        <option {{$data == null ? '' : $data->state == 'Power On' ? 'selected' : '' }} value="Power On" selected="selected">Power On</option>
    </select>
</div>
<div class="form-group">
    <label for="ip_address">IP Address</label>
    <input value="{{$data == null ? old('ip_address') : $data->IP_address }}" class="form-control" id="ip_address" name="ip_address" type="text">
</div>
<div class="form-group">
    <label for="memory_size">Memory Size</label>
    <input value="{{$data == null ? old('memory_size') : $data->memory_size }}" class="form-control" id="memory_size" name="memory_size" type="text">
</div>
<div class="form-group">
    <label for="cpus">CPUS</label>
    <input value="{{$data == null ? old('cpus') : $data->cpus }}" class="form-control" id="cpus" name="cpus" type="text">
</div>
<div class="form-group">
    <label for="provisioned_space">Provisioned Space</label>
    <input value="{{$data == null ? old('provisioned_space') : $data->provisioned_space }}" class="form-control" id="provisioned_space" name="provisioned_space" type="text">
</div>
<div class="form-group">
    <label for="used_space">Used Space</label>
    <input value="{{$data == null ? old('used_space') : $data->used_space }}" class="form-control" id="used_space" name="used_space" type="text">
</div>
