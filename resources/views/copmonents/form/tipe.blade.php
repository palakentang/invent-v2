<div class="form-group">
    <label for="nama_tipe">Nama Tipe</label>
    {!! Form::text('nama_tipe', $data == null ? old('nama_tipe') : $data->nama, ['class' => 'form-control', 'id' => 'nama_tipe']) !!}
</div>