{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="nama_cluster">Nama Cluster</label>
    {!! Form::text('nama_cluster', old('nama_cluster'), ['class' => 'form-control', 'required']) !!}
</div>
<div class="form-group">
    <label for="cluster_system_id">Cluster System</label>
    <select name="cluster_system_id" id="" class="form-control" required>
        <option value="">Pilih Cluster System</option>
        @forelse (\App\Models\ClusterSystem::get() as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
        @empty
        @endforelse
    </select>
</div>
