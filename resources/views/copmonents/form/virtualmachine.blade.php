{!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
<div class="form-group">
    <label for="cluster">Cluster</label>
    <select name="cluster_id" id="cluster_id" class="form-control">
        @foreach (App\Models\Cluster::get() as $item)
        <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->host }})</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="name">Jenis Virtual Machine</label>
    {!! Form::select('jenis_virtual_machines_id', $select['jenis'], $data != null ? '' : old('jenis_virtual_machines_id'), ['class' => 'form-control', 'required']) !!}
</div>
<div class="form-group">
    <label for="name">Nama Virtual Machine</label>
    <input value="{{$data == null ? old('nama_vm') : ''}}" class="form-control" id="nama_vm" name="nama_vm" type="text">
</div>
<div class="form-group">
    <label for="name">Deskripsi Virtual Machine</label>
    <textarea name="deskripsi" id="deskripsi" cols="30" rows="2" class="form-control">{{ $data == null ? old('deskripsi') : '' }}</textarea>
</div>
<div class="form-group">
    <label for="name">IP Address</label>
    <input value="{{$data == null ? old('ip_address') : ''}}" class="form-control" id="ip_address" name="ip_address" type="text">
</div>
<div class="form-group">
    <label for="name">Core</label>
    <input value="{{$data == null ? old('core') : ''}}" class="form-control" id="core" name="core" type="text">
</div>
<div class="form-group">
    <label for="name">Kapasitas Memori</label>
    <input value="{{$data == null ? old('kapasitas_memori') : ''}}" class="form-control" id="kapasitas_memori" name="kapasitas_memori" type="text">
</div>
<div class="form-group">
    <label for="name">Kapasitas HDD</label>
    <input value="{{$data == null ? old('space_hdd') : ''}}" class="form-control" id="space_hdd" name="space_hdd" type="text">
</div>
<div class="form-group">
    <div class="row">
        <div class="col-2">
            <div class="form-check">
                <input class="form-check-input" name="status_power" type="checkbox" id="on" value="on" {{ $data != null && '' == 'on'  ? 'checked' : '' }}>
                <label class="form-check-label" for="on" >ON</label>
            </div>
        </div>
        <div class="col-6">
            <div class="form-check">
                <input class="form-check-input" name="status_power" type="checkbox" id="off" value="off" {{ $data != null && '' == 'off' ? 'checked' : '' }}>
                <label class="form-check-label" for="off" >OFF</label>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="name">OS</label>
    <input value="{{$data == null ? old('os') : ''}}" class="form-control" id="os" name="os" type="text">
</div>
<div class="form-group">
    <label for="name">Mac Address</label>
    <input value="{{$data == null ? old('mac_address') : ''}}" class="form-control" id="mac_address" name="mac_address" type="text">
</div>
<div class="form-group">
    <label for="name">Used Spaced</label>
    <input value="{{$data == null ? old('used_spaced') : ''}}" class="form-control" id="used_spaced" name="used_spaced" type="text">
</div>
<div class="form-group">
    <label for="vRAM">vRAM</label>
    <input value="{{$data == null ? old('vRAM') : ''}}" class="form-control" id="vRAM" name="vRAM" type="text">
</div>
<div class="form-group">
    <label for="vDISK">vDISK</label>
    <input value="{{$data == null ? old('vDISK') : ''}}" class="form-control" id="vDISK" name="vDISK" type="text">
</div>
