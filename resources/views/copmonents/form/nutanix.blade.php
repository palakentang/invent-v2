    {!! $data == null ? '' : '<input type="hidden" name="_method" value="PUT">'!!}
    <div class="form-group">
        <label for="name">Nama VM</label>
        <input value="{{$data == null ? old('name') : $data->name}}" class="form-control" id="name" name="name" type="text">
    </div>
    <div class="form-group">
        <label for="ip_address">IP Address</label>
        <input value="{{$data == null ? old('ip_address') : $data->ip_address}}" class="form-control" id="ip_address" name="ip_address" type="text">
    </div>
    <div class="form-group">
        <label for="core">Core</label>
        <input value="{{$data == null ? old('core') : $data->core}}" class="form-control" id="core" name="core" type="text">
    </div>
    <div class="form-group">
        <label for="memory_capacity">Kapasitas Memori</label>
        <input value="{{$data == null ? old('memory_capacity') : $data->memory_capacity}}" class="form-control" id="memory_capacity" name="memory_capacity" type="text">
    </div>
    <div class="form-group">
        <label for="provisionedSpace">Full memori</label>
        <input value="{{$data == null ? old('provisioned_space') : $data->provisioned_space}}" class="form-control" id="provisioned_space" name="provisioned_space" type="text">
    </div>
    <div class="form-group">
        <label for="usedSpace">Used Space</label>
        <input value="{{$data == null ? old('used_space') : $data->used_space}}" class="form-control" id="used_space" name="used_space" type="text">
    </div>
