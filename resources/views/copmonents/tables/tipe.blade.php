<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->nama_tipe }}</td>
            <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            <td>
                <div class="row">
                    <div class="col-6">
                        <a class="btn btn-success" href="{{ route($route['edit'], $item->id) }}">Ubah</a>
                    </div>
                    <div class="col-6">
                        <form action="{{ route($route['delete'], $item->id) }}" method="post">
                            <input class="btn btn-danger" type="submit" value="Delete" />
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>