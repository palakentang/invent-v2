<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->nama_merk }}</td>
            <td width="110">{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td width="110">{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            <td width="40" style="white-space:nowrap; align=right;">
                <div class="row">
                    <div class="col-6">
                        <form action="{{ route($route['delete'], $item->id) }}" method="post">
						<a title="edit" class="btn btn-success btn-sm" href="{{ route($route['edit'], $item->id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
						<button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>