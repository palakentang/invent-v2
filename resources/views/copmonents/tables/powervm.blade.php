<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th @class([
                'd-none' => Request::segment(1) != 'master'
            ])>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>
                @if (Request::segment(1) == 'master')
                    <a href="{{route('master.hypervisor.powervm.lists.index', ['power_vm_id' => $item->id])}}">{{ $item->kd_power_vm }}</a>
                @else
                    <a href="{{route('hypervisor.powervm.lists.index', ['power_vm_id' => $item->id])}}">{{ $item->kd_power_vm }}</a>
                @endif
            </td>
            <td>{{ $item->vm_name }}</td>
            <td>{{ $item->hostname }}</td>
            <td>{{ $item->vm_ip }}</td>
            <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            <td @class([
                'd-none' => Request::segment(1) != 'master'
            ])>
                <div class="row">
                    <div class="col-6">
                        <a class="btn btn-success" href="{{ route($route['edit'], $item->id) }}">Ubah</a>
                    </div>
                    <div class="col-6">
                        <form action="{{ route($route['delete'], $item->id) }}" method="post">
                            <input class="btn btn-danger" type="submit" value="Delete" />
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
