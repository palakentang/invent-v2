<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->id }}</td>
            <td>{{ $item->deskripsi }}</td>
            <td>{{ $item->hostname }}</td>
            <td>{{ $item->ip_address }}</td>
            <td>{{ $item->copmonents }}</td>
            <td>{{ $item->service }}</td>
            <td>
                <div class="row">
                    @if (Request::segment(1) == 'master')
                    <div class="col-6">
                        <a class="btn btn-success" href="#" onclick="ubah({'data':{{$item}}, 'route':'{{route($route['update'], [$item->power_vm_id, $item->id])}}'})">Ubah</a>
                    </div>
                    <div class="col-6">
                        <form action="{{ route($route['delete'], [$item->power_vm_id, $item->id]) }}" method="post">
                            <input class="btn btn-danger" type="submit" value="Delete" />
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                    @else
                        <a class="btn btn-success" href="#" onclick="lihat({{$item}})">Lihat</a>
                    @endif
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
