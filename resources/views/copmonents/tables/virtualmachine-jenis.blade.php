<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->nama }}</td>
            <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            <td>
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-success btn-sm btn-block" href="{{ route($route['edit'], $item->id) }}">Ubah</a>
                        <form action="{{ route($route['delete'], $item->id) }}" method="post" id="hapus{{ $item->id }}">
                            @method('delete')
                            @csrf
                        </form>
                        <a style="margin-top: 8px;" class="btn btn-danger btn-sm btn-block" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus{{ $item->id }}').submit()">Hapus</a>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
