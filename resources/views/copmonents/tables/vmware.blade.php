<table id="example" class="display" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            <th @class([
                'd-none' => Request::segment(1) != 'master'
            ])>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->hostname }}</td>
            <td>{{ $item->IP_address }}</td>
            <td>
                Provisioned Space: {{$item->provisioned_space}}<br>
                Used Space: {{$item->used_space}}
            </td>
            <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            <td @class([
                'd-none' => Request::segment(1) != 'master'
            ])>
                <div class="row">
                    <a class="btn btn-success" href="{{ route($route['edit'], $item->id) }}">Ubah</a><br>
                    <form action="{{ route($route['delete'], $item->id) }}" method="post" style="margin-top: 7px">
                        <input class="btn btn-danger" type="submit" value="Hapus" />
                        @method('delete')
                        @csrf
                    </form>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
