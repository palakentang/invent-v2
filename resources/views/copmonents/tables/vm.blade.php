<table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
    <thead>
    {{--    
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            @if (\Request::segment(1) == 'master')

            @endif
        </tr>
    --}}
    <tr style="text-align: center">
			@if (Request::segment(1) == 'master')            
            <th width="30">Aksi</th>
			@else
		    <th width="10">Aksi</th>
            @endif		
            <th width="8">No.</th>
            <th width="150">Cluster</th>
            <th width="90">Host</th>
            <th>Nama VM</th>
            <th>State</th>
            <th width="90">Kondisi Disk</th>
            <th width="80">vRAM</th>			
            <th width="80">vCPU</th>
            <th width="80">vDisk</th>
            <th width="80">Guest OS</th>			
			<th width="50">IP Address</th>
        </tr>    
    </thead>
    <tbody>
    @php
       $no = 0;
    @endphp
        @foreach ($data as $item)
        @php
            $no++;
        @endphp    
            <td style="white-space:nowrap; align=right; vertical-align:top;">
                @if (Request::segment(1) == 'master')
					<a href="#" class="btn btn-success btn-sm" onclick="show({{$item}}, {{$item->Cluster}})"><i class="fa fa-info-circle" aria-hidden="true"></i></a>                            
					<a href="#" class="btn btn-warning btn-sm" onclick="ubah({{$item}})"><i class="fa fa-edit" aria-hidden="true"></i></a>
					<form action="{{ route($route['delete'], $item->id) }}" method="post" id="hapus{{ $item->id }}" style="display: none">
						@method('DELETE')
						@csrf
					</form>
					<a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus{{ $item->id }}').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
				@else
					<a href="#" class="btn btn-success btn-sm" onclick="show({{ $item }})"><i class="fa fa-info-circle" aria-hidden="true"></i></a>					
				@endif			
            </td>
            
            <td style="vertical-align:top;">{{ $no }}</td>
            <td style="vertical-align:top;">{{ isset($item->Cluster) ? $item->Cluster->nama_cluster : '-' }}</td>
            <td style="vertical-align:top;">{{ $item->hostname }}</td>            
            <td style="vertical-align:top;">{{ $item->name }}</td>
            <td style="vertical-align:top;"><span @class([
                'right badge badge-success' => $item->power == 'on',
                'right badge badge-danger' => $item->power == 'off',
            ])>{{ strtoupper($item->power) }}</span></td>
            <td style="vertical-align:top;">{{ $item->used_space }}&nbsp;&nbsp;
                @switch(true)
                    @case($item->PercentageUsedSpaced($item) > 50)
                        <span class="right badge badge-success">{{ $item->UsedSpaced() }} Kosong</span>
                        @break
                    @case($item->PercentageUsedSpaced($item) < 50)
                        <span class="right badge badge-danger">{{ $item->UsedSpaced() }} Kosong</span>
                        @break
                    @default
                        <span class="right badge badge-warning">{{ $item->UsedSpaced() }} Kosong</span>

                @endswitch
                
            </td>
            <td style="vertical-align:top;">{{ $item->vRAM }}</td>
            <td style="vertical-align:top;">{{ $item->vCPU }}</td>
            <td style="vertical-align:top;">{{ $item->vDISK }}</td>
            <td style="vertical-align:top;">{{ isset($item->OSS) ? $item->OSS->nama_os : '-' }}</td>
            <td style="vertical-align:top;">
                <ul>
                    @if($item->ip_address===null)  
					{{ "-" }}
					@else
						<?=str_replace(';', '<br>', $item->ip_address)?>
					@endif
                </ul>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
