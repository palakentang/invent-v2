<table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
    <thead>
        <tr style="text-align: center">
			@if (Request::segment(1) == 'master')            
            <th width="30">Aksi</th>
			@else
		    <th width="10">Aksi</th>
            @endif		
            <th width="8">No.</th>
            <th width="150">Nama Barang</th>
            <th width="90">Jenis</th>
            <th>Merk / Tipe</th>
            <th>Cluster</th>			
            <th width="80">S/N</th>			
            <th width="80">Mac Address</th>
            <th width="80">IP Address</th>			
			<th width="50">Pengadaan</th>
	        <th width="90">Akhir Warranty</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 0;
        @endphp
        @foreach ($data as $item)
        @php
            $no++;
        @endphp
            <tr>
				<td style="white-space:nowrap; align=right; vertical-align:top;">
                @if (Request::segment(1) == 'master')
					<a href="#" class="btn btn-success btn-sm" onclick="show({{$item}})"><i class="fa fa-info-circle" aria-hidden="true"></i></a>                            
					<a href="#" class="btn btn-warning btn-sm" onclick="edit({{$item}})"><i class="fa fa-edit" aria-hidden="true"></i></a>
					<form action="{{ route($route['delete'], $item->id) }}" method="post" id="hapus{{ $item->id }}" style="display: none">
						@method('DELETE')
						@csrf
					</form>
					<a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus{{ $item->id }}').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
				@else
					<a href="#" class="btn btn-success btn-sm" onclick="show({{ $item }})"><i class="fa fa-info-circle" aria-hidden="true"></i></a>					
				@endif			
                </td>			
                <td style="vertical-align:top;">{{ $no }}</td>
                <td style="vertical-align:top;">{{ $item->nama_barang }}</td>
                <td style="vertical-align:top;">{{ $item->JenisBarang->nama_jenis_barang }}</td>
				<td style="vertical-align:top;">{{ $item->MerkBarang->nama_merk }} {{ $item->tipe }} </td>
				<td style="vertical-align:top;"> 
					@if(isset($item->ClusterBarang->nama_cluster))
					   {{ $item->ClusterBarang->nama_cluster }}					
					@else
						{{ "-" }}
					@endif
					</td>
				<td style="vertical-align:top;">
				    @if($item->serial_number===null)  
					{{ "-" }}
					@else
					{{ $item->serial_number }}	
					@endif
				</td>
				<td style="vertical-align:top;">
					@if($item->mac_address===null)  
					{{ "-" }}
					@else
					{{ $item->mac_address }}	
				@endif			
				</td>
				<td style="vertical-align:top;">
				{{--
                        @forelse ($item->IPAddresses as $value)
                            {{ $value->ip_address }} {{ $value->isMgmt == 1 ? '(Mgmt)' : '' }}<br>
                        @empty
                            -
                        @endforelse	
				 --}}
				 @if($item->ip_address===null)  
					{{ "-" }}
					@else
						<?=str_replace(';', '<br>', $item->ip_address)?>
					@endif
				</td>

				<td style="vertical-align:top;">
				    @if(isset($item->Pengadaan->tgl_pengadaan))
					   {{ date("Y", strtotime($item->Pengadaan->tgl_pengadaan)); }}
				    @else 
					    - 
					@endif 			
				</td>
				<td style="vertical-align:top;">
			    @if($item->garansi_akhir===null)  
					{{ "-" }}
				@else
					{{ date("d F Y", strtotime($item->garansi_akhir)) }}	
				@endif
				</td>
            </tr>
        @endforeach
    </tbody>
</table>
