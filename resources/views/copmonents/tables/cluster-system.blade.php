<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            @if (\Request::segment(1) == 'master')

            <th>Aksi</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->name }}</td>
            <td>{{ $item->description }}</td>
            <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
            <td>{{ date('d F Y', strtotime($item->updated_at)) }}</td>
            @if (\Request::segment(1) == 'master')

            <td class="align-right">
                <div class="">
                    <div class="">
                        <a href="#" class="btn btn-success btn-sm" onclick="ubah({{ $item }})"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <form action="{{ route($route['delete'], $item->id) }}" method="post" id="hapus{{ $item->id }}" style="display: none">
                            @method('DELETE')
                            @csrf
                        </form>
                        <a class="btn btn-danger btn-sm" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus{{ $item->id }}').submit()"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
            </td>

            @endif
        </tr>
        @endforeach
    </tbody>
</table>
