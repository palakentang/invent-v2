<table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr style=" text-align: center">
            @foreach ($thead as $item)
                <th>{{ $item }}</th>
            @endforeach
            @if (\Request::segment(1) == 'master')

            <th>Aksi</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <td>{{ $item->nama_vm }}</td>
            <td>{{ $item->Jenis->nama }}</td>
            <td>
                Mac Address: {{ $item->mac_address ? $item->mac_address : '-' }}<br />
                {{-- Mac Address: {{ $data->mac_address }} --}}
            </td>
            <td>{{ $item->ip_address }}</td>
            <td>{{ $item->kapasitas_memori }}</td>
            <td><span class="{{ $item->status_power == 'on' ? 'btn btn-success' : 'btn btn-danger' }}">{{ $item->status_power }}</span></td>
            @if (\Request::segment(1) == 'master')

            <td>
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-success btn-sm btn-block" href="#" onclick="ubah({{ $item }})">Ubah</a>
                        <a class="btn btn-primary btn-sm btn-block" onclick="lihat({{$item}})" href="#">Lihat</a>
                        <form action="{{ route($route['delete'], $item->id) }}" method="post" id="hapus{{ $item->id }}">
                            @method('delete')
                            @csrf
                        </form>
                        <a style="margin-top: 8px;" class="btn btn-danger btn-sm btn-block" href="javascript(0);" onclick="event.preventDefault(); document.getElementById('hapus{{ $item->id }}').submit()">Hapus</a>
                    </div>
                </div>
            </td>

            @endif
        </tr>
        @endforeach
    </tbody>
</table>
