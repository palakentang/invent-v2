        <script>
                $(function() {
                    'use strict'
        //-------------
        // - PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#{{ $id }}').get(0).getContext('2d')
        var pieData = {
            labels: [
                @foreach ($label as $item)
                    '{{ $item }}',
                @endforeach
            ],
            datasets: [
            {
                data: [
                    @foreach ($value as $items)
                        '{{ $items->Clusters->count() }}',
                    @endforeach
                ],
                backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de']
            }
            ]
        }
        var pieOptions = {
            legend: {    
            display: false
            }
        }
        // Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        // eslint-disable-next-line no-unused-vars
        var pieChart = new Chart(pieChartCanvas, {
            type: 'doughnut',
            data: pieData,
            options: pieOptions
        })

        //-----------------
        // - END PIE CHART -
        //-----------------
    })

    </script>
