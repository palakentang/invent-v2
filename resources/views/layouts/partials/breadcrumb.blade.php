
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
        @for ($i = 1; $i <= count(Request::segments()); $i++)
        <li @class([ 'breadcrumb-item', 'active' => Request::segment($i) != 'master']) class="breadcrumb-item">
        {{-- {!! Request::segment(2) == 'hypervisor' ? ucwords(str_replace("-"," ",Request::segment($i))) : '' !!} --}}
        <a style="text-decoration: none;color: #6c757d;font-weight: 600;" href="#")>{{ucfirst(Request::segment($i))}}</a>
        </li>
        @endfor
    </ol>
</div><!-- /.col -->
