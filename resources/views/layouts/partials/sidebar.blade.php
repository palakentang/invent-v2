<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link">
        <img src="{{ asset('v1/dist/img/logo_id.png') }}" alt="DITLALA Logo"
            style="opacity: .8; width: 100%">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('v1/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">DITLALA</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('master.dashboard') }}" @class([ 'nav-link' , 'active'=> Request::segment(1) ==
                        null
                        ])>
                        <i class="fa fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
				
                {{-- Perangkat --}}
                <li class="nav-item">
                    <a href="{{ route('master.barang.index') }}" @class([ 'nav-link' , 'active'=> Request::segment(1) ==
                        'barang'
                        ])>
                        <i class="fas fa-server"></i>
                        <p>Perangkat</p>
                    </a>
                </li>
				
                {{-- Virtual Machine --}}
                <li class="nav-item">
					<a href="{{ route('master.virtual-machine.vm.index') }}" @class([ 'nav-link' , 'active'=>Request::segment(1) == 'virtual-machine'
						])>
						<i class="fas fa-cubes"></i>
						<p>Virtual Machine</p>
					</a>
                </li>
				
                <li class="nav-item">
                    <a href="#" @class([ 'nav-link' , 'active'=> Request::segment(1) != 'dashboard' && Request::segment(1) != 'barang' && Request::segment(1) != 'tabel-pendukung'])>
                        <i class="fas fa-table"></i>
                        <p>
                            Tabel Pendukung
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
					
                        {{-- Jenis Barang --}}
                        <li class="nav-item">
                            <a href="{{ route('master.jenis.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'jenis'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Jenis Barang</p>
                            </a>
                        </li>

                        {{-- Pengadaan --}}
                        <li class="nav-item">
                            <a href="{{ route('master.pengadaan.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'pengadaan'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Pengadaan</p>
                            </a>
                        </li>

                        {{-- Lokasi --}}
                        <li class="nav-item">
                            <a href="{{ route('master.lokasi.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'lokasi'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Lokasi</p>
                            </a>
                        </li>                        

                        {{-- Merk --}}
                        <li class="nav-item">
                            <a href="{{ route('master.merk.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'merk'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Merk</p>
                            </a>
                        </li>

                        {{-- Status Perangkat --}}
                        <li class="nav-item">
                            <a href="{{ route('master.statusbarang.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'statusbarang'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Status Perangkat</p>
                            </a>
                        </li>

                        {{-- Node 
                        <li class="nav-item">
                            <a href="{{ route('master.virtual-machine.node.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'node'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Node</p>
                            </a>
                        </li>
                        --}}

                        {{-- Cluster --}}
                        <li class="nav-item">
                            <a href="{{ route('master.virtual-machine.cluster.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'cluster'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Cluster</p>
                            </a>
                        </li>

                        {{-- CLuster System --}}
                        <li class="nav-item">
                            <a href="{{ route('master.virtual-machine.cluster-system.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'cluster-system'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master Cluster System</p>
                            </a>
                        </li>				
                        
                        {{-- OS --}}
                        <li class="nav-item">
                            <a href="{{ route('master.os.index') }}" @class([ 'nav-link' , 'active'=>
                                Request::segment(2) == 'os'
                                ])>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Master OS</p>
                            </a>
                        </li>				
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>