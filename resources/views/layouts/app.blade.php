<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DITLALA - Inventaris Perangkat Keras</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('v1/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('v1/dist/css/adminlte.min.css') }}">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}
    @yield('css-third')
    
	<style>
		body {font-size:13px;}
	</style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        @include('layouts.partials.nav')

        @if (Request::segment(1) == 'master')
            @include('layouts.partials.sidebar')
        @else
            @include('layouts.partials.sidebar-guest')
        @endif

        <!-- Content Wrapper. Contains page content -->
            @yield('contents')
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        
    </div>
    <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 <a href="https://dephub.go.id/">Direktorat Lalu Lintas dan Angkutan Laut - Kementerian Republik Indonesia</a>.</strong> All rights
        reserved.
    </footer>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('v1/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('v1/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('v1/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('v1/dist/js/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#example').DataTable({
				iDisplayLength: 10,
                pageLength: 10,
                scrollX: true
            });
        } );
    </script>
    @yield('js-third')
</body>

</html>